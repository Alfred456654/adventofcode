#!/usr/bin/python

import re
from itertools import *

test = {
        '<>': (0, 0),
        '<random characters>': (0, 17),
        '<<<<>': (0, 3),
        '<{!>}>': (0, 2),
        '<!!>': (0, 0),
        '<!!!>>': (0, 0),
        '<{o"i!a,<{i<a>': (0, 10),
        '{}': (1, 0),
        '{{{}}}': (6, 0),
        '{{},{}}': (5, 0),
        '{{{},{},{{}}}}': (16, 0),
        '{<{},{},{{}}>}': (1, 10),
        '{<a>,<a>,<a>,<a>}': (1, 4),
        '{{<a>},{<a>},{<a>},{<a>}}': (9, 4),
        '{{<!>},{<!>},{<!>},{<a>}}': (3, 13),
        '{{<ab>},{<ab>},{<ab>},{<ab>}}': (9, 8),
        '{{<!!>},{<!!>},{<!!>},{<!!>}}': (9, 0),
        '{{<a!>},{<a!>},{<a!>},{<ab>}}': (3, 17)
        }

RE_GARBAGE = re.compile(r'<[^>]*[^!]>')

def calc_weight(in_str):
    result = 0
    weight = 1
    for c in chain(in_str):
        if c == '{':
            result += weight
            weight += 1
        if c == '}':
            weight -= 1
    return result

def calc(in_str):
    transform = re.sub('!.', '', in_str)
    size_before = len(transform)
    n_garbage = 0
    while RE_GARBAGE.search(transform):
        transform = re.sub('<[^>]*[^!]>', '', transform, count = 1)
        n_garbage += 1
    while '<>' in transform:
        transform = re.sub('<>', '', transform, count = 1)
        n_garbage += 1
    size_after = min(size_before, len(transform) + 2 * n_garbage)
    return (calc_weight(transform), (size_before - size_after))

if __name__ == '__main__':
    for i in test:
        c = calc(i)
        # print('%s | %s | %s' % (test[i], c, i))
        assert c[0] == test[i][0]
        assert c[1] == test[i][1]
    with open('input', 'r') as f:
        in_str = f.read().strip()
        print('answers: %d, %d' % calc(in_str))
