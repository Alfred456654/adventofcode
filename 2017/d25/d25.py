#!/usr/bin/python

def test():
    steps = 6
    tape = {}
    cursor = 0
    state = 0
    for _ in range(steps):
        if cursor not in tape:
            tape[cursor] = 0
        if state == 0:
            tape[cursor] = 1 - tape[cursor]
            cursor += 2 * tape[cursor] - 1
            state = 1
        elif state == 1:
            tape[cursor] = 1
            cursor -= 2 * tape[cursor] - 1
            state = 0
    return sum(tape.values())

def calc():
    steps = 12683008
    tape = {}
    cursor = 0
    state = 0
    for _ in range(steps):
        if cursor not in tape:
            tape[cursor] = 0
        t = tape[cursor]
        if state == 0:
            tape[cursor] = 1 - t
            cursor -= 2 * t - 1
            state = 1
        elif state == 1:
            tape[cursor] = 1 - t
            cursor += 2 * t - 1
            state += 1 + 2 * t
        elif state == 2:
            tape[cursor] = 1 - t
            cursor -= 2 * t - 1
            state += 2 - t
        elif state == 3:
            tape[cursor] = 1
            cursor -= 1
            state = 0
        elif state == 4:
            tape[cursor] = 0
            cursor += 1
            state = 5 * t
        elif state == 5:
            tape[cursor] = 1
            cursor += 1
            state = 4 - 4 * t
    return sum(tape.values())

if __name__ == '__main__':
    assert test() == 3
    print('Answer 1: %d' % calc())
