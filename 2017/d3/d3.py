#!/usr/bin/python

import math
import numpy as np
import operator

input = 361527
test = {15: 2, 30: 5, 44: 5, 81: 8, 59: 6, 1: 0, 69: 4}
test2 = {120: 122, 6: 10, 800: 806, 150: 304}

def calc(n):
    x = int(math.ceil(math.sqrt(n))/2)
    p = int(math.pow(2*x+1, 2))
    m = min(list(map(abs, np.array([p-x, p-3*x, p-5*x, p-7*x]) - n)))
    return x + m

def calc2(n):
    maze = {(0,0): 1}
    moves = [(0,1), (1,0), (0,-1), (-1,0)]
    nmoves = 1
    x, y = 0, 0
    it = 0
    while True:
        for _ in range(nmoves):
            (x,y) = tuple(map(operator.add, (x,y), moves[it % 4]))
            maze[(y,x)] = sum([maze[(i,j)] for i in range(y-1, y+2) for j in range(x-1, x+2) if (i,j) in maze])
            if maze[(y,x)] > n:
                return maze[(y,x)]
        it += 1
        if it % 2 == 0:
            nmoves += 1

if __name__ == '__main__':
    for k in test.keys():
        # print('%5d | %5d | %d' % (k, test[k], calc(k)))
        assert test[k] == calc(k)
    for k in test2.keys():
        # print('%5d | %5d | %d' % (k, test2[k], calc2(k)))
        assert test2[k] == calc2(k)
    print('answer 1: %d' % calc(input))
    print('answer 2: %d' % calc2(input))
