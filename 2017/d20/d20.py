#!/usr/bin/python

from itertools import chain
import re


def a_add(a, b):
    return [x + y for x, y in zip(a, b)]


def a_mul(a, m):
    return [m * x for x in a]


def n1(x):
    return sum(map(abs, x))


def get_puzzle(filename):
    with open(filename, 'r') as f:
        return [eval('{' + re.sub("([apv])", "'\g<1>'", x.replace('=', ':').replace('<', '[').replace('>', ']')) + '}')
                for x in map(str.strip, f.readlines())]


def min_n_it(part):
    v = part['v']
    n = n1(v)
    v_min = n + 1
    it = 0
    while n < v_min:
        v_min = n
        it += 1
        v = a_add(v, part['a'])
        n = n1(v)
    return it


def calc(_p):
    aa = min([n1(x['a']) for x in _p])
    min_a = [_p.index(x) for x in _p if n1(x['a']) == aa]
    max_n_it = max([min_n_it(_p[x]) for x in min_a])
    final_vs = {x: n1(a_add(_p[x]['v'], a_mul(_p[x]['a'], max_n_it))) for x in min_a}
    v_min = min(final_vs.values())
    return [x for x in final_vs if final_vs[x] == v_min][0]


def running(pos, vel, acc):
    n_pos = {x: n1(pos[x]) for x in pos}
    n_vel = {x: n1(vel[x]) for x in vel}
    n_acc = {x: n1(acc[x]) for x in acc}
    n_pos = sorted(n_pos, key=n_pos.get)
    n_vel = sorted(n_vel, key=n_vel.get)
    n_acc = sorted(n_acc, key=n_acc.get)
    if n_pos == n_vel:
        return n_vel != n_acc
    return True


def calc2(particles):
    pos = {x: particles[x]['p'] for x in range(len(particles))}
    vel = {x: particles[x]['v'] for x in range(len(particles))}
    acc = {x: particles[x]['a'] for x in range(len(particles))}
    it = 0
    while running(pos, vel, acc):
        print(len(pos))
        positions = {}
        for i in pos.keys():
            vel[i] = a_add(vel[i], acc[i])
            pos[i] = a_add(vel[i], pos[i])
            lol = str(pos[i])
            if lol in positions:
                positions[lol].append(i)
            else:
                positions[lol] = [i]
        to_del = list(set(chain(*[x for x in positions.values() if len(x) > 1])))
        for i in to_del:
            pos.pop(i)
            vel.pop(i)
            acc.pop(i)
    return len(pos)


if __name__ == '__main__':
    p = get_puzzle('test')
    print(f'Answer 1: {calc(p):d}')
    print(f'Answer 2: {calc2(p):d}')
