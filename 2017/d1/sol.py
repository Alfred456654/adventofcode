with open("input.txt", 'r') as f:
    in_str = f.read().strip()
    print(sum([int(in_str[i]) for i in range(len(in_str)) if in_str[i-1] == in_str[i]]))
