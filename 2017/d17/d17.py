#!/usr/bin/python

test_i = 3
test_o = 638
input = 359

def calc(n):
    a = [0]
    t = 0
    for i in range(1, 2018):
        t = (t + n) % i + 1
        a.insert(t, i)
    return a[a.index(2017) + 1]

def calc2(n):
    a = 0
    t = 0
    for i in range(1, 50000001):
        t = (t + n) % i + 1
        if t == 1:
            a = i
    return a

if __name__ == '__main__':
    assert calc(test_i) == test_o
    print(calc(input))
    print(calc2(input))
