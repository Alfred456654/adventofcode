#!/usr/bin/python

from itertools import chain

MOVES = {'-': [(0,-1), (0,1)], '|': [(-1,0), (1,0)], '+': [(-1,0), (1,0), (0,-1), (0,1)]}
ALPHABET = list(map(chr, range(65, 91)))

def get_move(maze, visited, y, x):
    c = maze[y][x]
    for m in MOVES[c]:
        [i, j] = [sum(t) for t in zip(*[m, (y, x)])]
        if i in range(len(maze)) and j in range(len(maze[i])):
            c2 = maze[i][j]
            if c == '+' and (i, j) not in visited:
                if m[0] == 0 and (c2 in ['-', '+'] or c2 in ALPHABET):
                    return (i, j)
                elif m[0] != 0 and (c2 in ['|', '+'] or c2 in ALPHABET):
                    return (i, j)
            else:
                if (c2 in [c, '+'] or c2 in ALPHABET) and (i,j) not in visited:
                    return (i, j)
                else:
                    [k, l] = [sum(t) for t in zip(*[m, (i, j)])]
                    if (k, l) not in visited and k in range(len(maze)) and l in range(len(maze[i])):
                        c3 = maze[k][l]
                        if c3 in[c, '+'] or c3 in ALPHABET:
                            visited[(i, j)] = maze[i][j]
                            return (k, l)
    return (-1, -1)

def browse(filename):
    maze = get_maze(filename)
    (y, x) = (0, maze[0].index('|'))
    visited = {}
    letters = []
    steps = 1
    while True:
        visited[(y, x)] = maze[y][x]
        (i, j) = get_move(maze, visited, y, x)
        if (i, j) == (-1, -1):
            break
        if maze[i][j] in ALPHABET:
            letters.append(maze[i][j])
            maze[i][j] = maze[y][x]
        steps += abs(y-i) + abs(x-j)
        (y, x) = (i, j)
    return (''.join(letters), steps)

def get_maze(filename):
    with open(filename, 'r') as t:
        return [list(chain(x)) for x in t.read().split('\n')[:-1]]

if __name__ == '__main__':
    assert browse('test') == ('ABCDEF', 38)
    print('Answer 1: %s\nAnswer 2: %d' % browse('input'))
