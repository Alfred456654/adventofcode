#!/usr/bin/python

from itertools import chain
from math import sqrt

MOVES = {0: (-1, 0), 1: (0, -1), 2: (1, 0), 3: (0, 1)}
MOVE_INCR = {1: {0: 1, 1: -1}, 2: {0: 1, 1: 0, 2: -1, 3: 2}}

def get_puzzle(filename, n):
    with open(filename, 'r') as t:
        p = [list(map(int, chain(x))) for x in t.read().strip().replace('.', '0').replace('#', str(n)).split('\n')]
        return {(i,j):p[i][j] for i in range(len(p)) for j in range(len(p))}

def calc(filename, n, q):
    cpt = 0
    p = get_puzzle(filename, q)
    d = sqrt(len(p))
    i, j = int(d/2), int(d/2)
    dir_idx = 0
    for _ in range(n):
        if (i,j) not in p:
            p[(i,j)] = 0
        dir_idx += MOVE_INCR[q][p[(i,j)]]
        dir_idx %= len(MOVES)
        p[(i,j)] += 1
        p[(i,j)] %= 2*q
        if p[(i,j)] == q:
            cpt += 1
        i += MOVES[dir_idx][0]
        j += MOVES[dir_idx][1]
    return cpt

if __name__ == '__main__':
    assert calc('test', 10000, 1) == 5587
    print('Answer 1: %d' % calc('input', 10000, 1))
    assert calc('test', 100, 2) == 26
    print('Answer 2: %d' % calc('input', 10000000, 2))
