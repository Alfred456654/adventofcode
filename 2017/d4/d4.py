#!/usr/bin/python

test = {'aa bb cc dd ee': True, 'aa bb cc dd aa': False, 'aa bb cc dd aaa': True}
test2 = {'abcde fghij': True, 'abcde xyz ecdab': False, 'a ab abc abd abf abj': True,
        'iiii oiii ooii oooi oooo': True, 'oiii ioii iioi iiio': False}

def valid2(in_str):
    words = [ ''.join(sorted(x)) for x in in_str.split(' ')]
    set_w = set(words)
    return len(words) == len(set_w)

def valid(in_str):
    words = in_str.split(' ')
    set_w = set(words)
    return len(words) == len(set_w)

if __name__ == '__main__':
    for t in test:
        # print('%20s | %8s | %s' % (t, test[t], valid(t)))
        assert valid(t) == test[t]
    for t in test2:
        # print('%20s | %8s | %s' % (t, test2[t], valid2(t)))
        assert valid2(t) == test2[t]
    with open('input', 'r') as f:
        in_strs = f.read().strip().split('\n')
        a1 = sum([valid(x) for x in in_strs])
        print('answer 1: %d' % a1)
        a2 = sum([valid2(x) for x in in_strs])
        print('answer 2: %d' % a2)
