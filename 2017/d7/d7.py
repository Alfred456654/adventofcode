#!/usr/bin/python

def check_l(node, local_weights, children, total_weights, kin, to_visit, visited):
    visited.append(node)
    if node in kin.keys():
        parent = kin[node]
        neighbours = children[parent]
        test = {x: total_weights[x] for x in neighbours}
        valueset = set(test.values())
        if len(valueset) > 1:
            test2 = {list(test.values()).count(x):x for x in valueset}
            wrong_weight = test2[1]
            wrong_node = [x for x in test.keys() if test[x] == wrong_weight][0]
            good_node = [x for x in test.keys() if test[x] != wrong_weight][0]
            return local_weights[wrong_node] - total_weights[wrong_node] + total_weights[good_node]
        for n in neighbours:
            if n not in to_visit and n not in visited:
                to_visit.insert(0, n)
        if parent not in to_visit and parent not in visited:
            to_visit.append(parent)
        return check_l(to_visit[1], local_weights, children, total_weights, kin, to_visit[2:], visited)

def calc_total_weight(node, local_weights, children, total_weights):
    if node in total_weights.keys():
        return total_weights[node]
    elif node not in children.keys():
        return local_weights[node]
    else:
        return local_weights[node] + sum([calc_total_weight(x, local_weights, children, total_weights) for x in children[node]])

def calc(filename):
    with open(filename, 'r') as f:
        instrs = map(str.strip, f.readlines())
        instrs = [x.replace(')','').replace('(','').replace(',','').split() for x in instrs]
        local_weights = {}
        total_weights = {}
        kin = {}
        children = {}
        for i in instrs:
            local_weights[i[0]] = int(i[1])
            if len(i) > 2:
                for child in i[3:]:
                    kin[child] = i[0]
                children[i[0]] = i[3:]
        for i in instrs:
            total_weights[i[0]] = calc_total_weight(i[0], local_weights, children, total_weights)
        root = [x for x in kin.values() if x not in kin.keys()][0]
        leaves = [x for x in kin.keys() if x not in kin.values()]
        result = check_l(leaves[0], local_weights, children, total_weights, kin, leaves[1:], [])
        return (root, result)

if __name__ == '__main__':
    assert calc('test') == ('tknk', 60)
    print(calc('input'))
