#!/usr/bin/python

from itertools import islice, cycle, chain

test_k = 'flqrgnkx'
test_v = 8108
test_v2 = 1242
in_str = 'jzgqcdpd'

def xor_hash(l):
    h = 0
    for i in l:
        h ^= i
    return h

def twist(in_list, lengths, cur_pos, skip_size):
    L = len(in_list)
    for l in lengths:
        seq_a = list(islice(cycle(in_list), cur_pos, cur_pos + l))[::-1]
        seq_b = list(islice(cycle(in_list), cur_pos + l, cur_pos + L))
        in_list = list(islice(cycle(seq_a + seq_b), -cur_pos % L, L + (-cur_pos % L)))
        cur_pos += l + skip_size
        cur_pos %= L
        skip_size += 1
        skip_size %= L
    return (in_list, cur_pos, skip_size)

def knot_hash(in_str):
    lengths = list(map(ord, in_str))
    lengths += [17, 31, 73, 47, 23]
    in_list = list(range(256))
    cur_pos = 0
    skip_size = 0
    for _ in range(64):
        (in_list, cur_pos, skip_size) = twist(in_list, lengths, cur_pos, skip_size)
    in_list = [in_list[i:i+16] for i in range(0, len(in_list), 16)]
    in_list = list(map(xor_hash, in_list))
    return ''.join('{0:08b}'.format(x) for x in in_list)

def calc(in_str):
    return sum([knot_hash('%s-%d' % (in_str, x)).count('1') for x in range(128)])

def calc2(in_str):
    grid = [list(map(int, chain(knot_hash('%s-%d' % (in_str, x))))) for x in range(128)]
    return len(set(cluster(grid).values()))

def neighbours(grid, i, j, result):
    row = [(k, j) for k in range(max(0, i-1), min(len(grid), i+2)) if k != i and grid[k][j] > 0 and (k,j) in result]
    col = [(i, l) for l in range(max(0, j-1), min(len(grid), j+2)) if l != j and grid[i][l] > 0 and (i,l) in result]
    return row + col

def cluster(grid):
    result = {}
    cluster_number = 0
    for i in range(len(grid)):
        for j in range(len(grid[i])):
            if grid[i][j] > 0:
                n = neighbours(grid, i, j, result)
                clusters = [ result[x] for x in n ]
                if len(clusters) == 0:
                    cluster_number += 1
                    result[(i,j)] = cluster_number
                elif len(clusters) == 1:
                    result[(i,j)] = clusters[0]
                else:
                    result[(i,j)] = min(clusters)
                    for z in result:
                        if result[z] in clusters:
                            result[z] = min(clusters)
    return result

if __name__ == '__main__':
    #assert calc(test_k) == test_v
    print('Answer 1: %d' % calc(in_str))
    #assert calc2(test_k) == test_v2
    print('Answer 2: %d' % calc2(in_str))
