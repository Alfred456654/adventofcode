#!/usr/bin/python

I_A = 516
I_B = 190
T_A = 65
T_B = 8921

def calc(xa, xb):
    res = 0
    for _ in range(40000000):
        xa = 16807 * xa % 2147483647
        xb = 48271 * xb % 2147483647
        if xa & 65535 == xb & 65535:
            res += 1
    return res

def calc2(xa, xb):
    res = 0
    for _ in range(5000000):
        xa = 16807 * xa % 2147483647
        while xa & 3 != 0:
            xa = 16807 * xa % 2147483647
        xb = 48271 * xb % 2147483647
        while xb & 7 != 0:
            xb = 48271 * xb % 2147483647
        if xa & 65535 == xb & 65535:
            res += 1
    return res

if __name__ == '__main__':
    # assert calc(T_A, T_B) == 588
    print('Answer 1: %d' % calc(I_A, I_B))
    # assert calc2(T_A, T_B) == 309
    print('Answer 2: %d' % calc2(I_A, I_B))
