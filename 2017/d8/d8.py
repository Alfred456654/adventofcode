#!/usr/bin/python

def calc(filename):
    with open(filename, 'r') as f:
        global_max = 0
        instrs = map(str.split, map(str.strip, f.readlines()))
        regs = {}
        for i in instrs:
            if i[0] not in regs.keys():
                regs[i[0]] = 0
            if i[4] not in regs.keys():
                regs[i[4]] = 0
            if eval("%s %s %s" % (regs[i[4]], i[5], i[6])):
                regs[i[0]] += [int(i[2]), -int(i[2])][i[1] == 'dec']
                global_max = max(global_max, regs[i[0]])
        return (max(regs.values()), global_max)

if __name__ == '__main__':
    print(calc('test'))
    assert calc('test') == (1, 10)
    print(calc('input'))
