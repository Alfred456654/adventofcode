module adventofcode

go 1.23.2

require (
	github.com/fogleman/gg v1.3.0
	github.com/stretchr/testify v1.10.0
	gitlab.com/Alfred456654/aoc-utils v0.5.1
	golang.org/x/exp v0.0.0-20241204233417-43b7b7cde48d
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/image v0.23.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
