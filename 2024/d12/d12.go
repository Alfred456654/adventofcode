package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

var (
	up    = aoc.Tuple{X: 0, Y: -1}
	left  = aoc.Tuple{X: -1, Y: 0}
	down  = aoc.Tuple{X: 0, Y: 1}
	right = aoc.Tuple{X: 1, Y: 0}

	fourSides = []aoc.Tuple{up, left, down, right}
)

type clusterDef struct {
	letter rune
	locs   *aoc.TupleSet
}

func computeSides(grid *aoc.Grid, loc aoc.Tuple, letter rune) int {
	var res int
	for i := range fourSides {
		s1 := fourSides[i]
		s2 := fourSides[(i+1)%4]
		n1 := loc.Add(s1)
		n2 := loc.Add(s2)
		corner := loc.Add(s1).Add(s2)
		r1, i1 := grid.GetAt(n1)
		r2, i2 := grid.GetAt(n2)
		rc, _ := grid.GetAt(corner)
		if (!i1 && !i2) || (r1 == letter && r2 == letter && rc != letter) || (r1 != letter && r2 != letter) {
			res++
			continue
		}
	}
	return res
}

func discoverCluster(cluster *clusterDef, loc aoc.Tuple, grid *aoc.Grid, toDo *aoc.TupleSet) (int, int) {
	around4 := grid.GetNeighbours(loc.X, loc.Y, func(x int, y int, r rune) bool {
		return (x-loc.X)*(y-loc.Y) == 0 && r == cluster.letter
	})
	perimeter := 4 - len(around4)
	sides := computeSides(grid, loc, cluster.letter)
	for neighb := range around4 {
		if !cluster.locs.Contains(aoc.Tuple{X: neighb.X, Y: neighb.Y}) {
			cluster.locs.Add(neighb)
			toDo.Remove(neighb)
			nPerim, nSides := discoverCluster(cluster, neighb, grid, toDo)
			perimeter += nPerim
			sides += nSides
		}
	}
	return perimeter, sides
}

func calc(filename string) (int, int) {
	var res, res2 int
	grid := aoc.NewGridFromFile(filename, false)
	toDo := aoc.NewTupleSet()
	for y := range grid.Height {
		for x := range grid.Width {
			toDo.Add(aoc.Tuple{X: x, Y: y})
		}
	}
	for toDo.Size() > 0 {
		var loc aoc.Tuple
		for loc = range toDo.Iterate() {
			break
		}
		toDo.Remove(loc)
		letter, _ := grid.GetAt(loc)
		clusterLocs := aoc.NewTupleSet()
		clusterLocs.Add(loc)
		cluster := &clusterDef{letter: letter, locs: clusterLocs}
		perim, sides := discoverCluster(cluster, loc, grid, toDo)
		surface := area(cluster)
		res += perim * surface
		res2 += sides * surface
	}
	return res, res2
}

func area(cluster *clusterDef) int {
	return cluster.locs.Size()
}

func main() {
	t11, t12 := calc("test1.txt")
	aoc.CheckEqualsMsg(140, t11, "t11")
	aoc.CheckEqualsMsg(80, t12, "t12")
	t21, t22 := calc("test2.txt")
	aoc.CheckEqualsMsg(772, t21, "t21")
	aoc.CheckEqualsMsg(436, t22, "t22")
	t31, t32 := calc("test3.txt")
	aoc.CheckEqualsMsg(1930, t31, "t31")
	aoc.CheckEqualsMsg(1206, t32, "t32")
	_, t42 := calc("test4.txt")
	aoc.CheckEqualsMsg(236, t42, "t42")
	_, t52 := calc("test5.txt")
	aoc.CheckEqualsMsg(368, t52, "t52")
	fmt.Println(calc("input.txt"))
}
