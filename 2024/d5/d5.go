package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

var beforeCache map[int]*aoc.Set[int]
var afterCache map[int]*aoc.Set[int]

func readInput(filename string) [][]int {
	blocks := aoc.ReadByBlocks(filename)
	ruleStrs := blocks[0]
	pageStrs := blocks[1]

	for _, rule := range ruleStrs {
		chunks := strings.Split(rule, "|")
		left, _ := strconv.Atoi(chunks[0])
		right, _ := strconv.Atoi(chunks[1])
		_, exists := beforeCache[left]
		if !exists {
			beforeCache[left] = aoc.NewSet[int]()
		}
		beforeCache[left].Add(right)
		_, exists = afterCache[right]
		if !exists {
			afterCache[right] = aoc.NewSet[int]()
		}
		afterCache[right].Add(left)
	}

	pages := make([][]int, len(pageStrs))
	for i, page := range pageStrs {
		pageNbrs := strings.Split(page, ",")
		pages[i] = make([]int, len(pageNbrs))
		for j, pageNbr := range pageNbrs {
			pages[i][j], _ = strconv.Atoi(pageNbr)
		}
	}

	fillCaches()
	return pages
}

func fillCaches() {
	var nbOps int
	for val, afters := range beforeCache {
		for after := range afters.Iterate() {
			if !afterCache[after].Contains(val) {
				nbOps++
				afterCache[after].Add(val)
			}
		}
	}

	for val, befores := range afterCache {
		for before := range befores.Iterate() {
			if !beforeCache[before].Contains(val) {
				nbOps++
				beforeCache[before].Add(val)
			}
		}
	}

	if nbOps > 0 {
		fillCaches()
	}
}

func calc(filename string) (int, int) {
	beforeCache = make(map[int]*aoc.Set[int])
	afterCache = make(map[int]*aoc.Set[int])
	pages := readInput(filename)

	var result, result2 int

	for i, page := range pages {
		ordered := true
		allNumbers := aoc.NewSet[int]()
		allNumbers.AddAll(page)

		for j := 0; j < len(page)-1; j++ {
			p1 := pages[i][j]
			p2 := pages[i][j+1]
			_, exists := beforeCache[p1]
			if exists {
				ordered = ordered && beforeCache[p1].Contains(p2)
			}
			_, exists = afterCache[p2]
			if exists {
				ordered = ordered && afterCache[p2].Contains(p1)
			}
		}

		middle := len(page) / 2
		if ordered {
			result += pages[i][middle]
		} else {
			for _, p := range page {
				after, ok := beforeCache[p]
				if ok && after.Intersection(allNumbers).Size() == middle {
					result2 += p
				}
			}
		}

	}
	return result, result2
}

func main() {
	t1, t2 := calc("test.txt")
	aoc.CheckEquals(143, t1)
	aoc.CheckEquals(123, t2)
	fmt.Println(calc("input.txt"))
}
