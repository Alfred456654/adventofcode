package main

import (
	"fmt"
	"github.com/fogleman/gg"
	"gitlab.com/Alfred456654/aoc-utils"
	"image/color"
	"regexp"
	"strconv"
)

var (
	robotLine = regexp.MustCompile(`p=(-?\d+),(-?\d+) v=(-?\d+),(-?\d+)`)
)

type robotDef struct {
	px, py, vx, vy int
}

func readInput(filename string) []robotDef {
	lines := aoc.ReadInputLines(filename)
	robots := make([]robotDef, len(lines))
	for i, line := range lines {
		matches := robotLine.FindStringSubmatch(line)
		px, _ := strconv.Atoi(matches[1])
		py, _ := strconv.Atoi(matches[2])
		vx, _ := strconv.Atoi(matches[3])
		vy, _ := strconv.Atoi(matches[4])
		robots[i] = robotDef{
			px: px,
			py: py,
			vx: vx,
			vy: vy,
		}
	}
	return robots
}

func move(robots []robotDef, nbCols int, nbLines int, timesteps int) map[aoc.Tuple]int {
	pos := make(map[aoc.Tuple]int)
	for _, r := range robots {
		endx := (r.px + timesteps*r.vx) % nbCols
		endy := (r.py + timesteps*r.vy) % nbLines
		if endx < 0 {
			endx += nbCols
		}
		if endy < 0 {
			endy += nbLines
		}
		pos[aoc.Tuple{X: endx, Y: endy}]++
	}
	return pos
}

func computeQuadrants(pos map[aoc.Tuple]int, nbCols int, nbLines int) []int {
	quadrants := make([]int, 4)
	for loc, nb := range pos {
		if loc.X == nbCols/2 || loc.Y == nbLines/2 {
			continue
		}
		quadrant := 0
		if loc.X >= (nbCols+1)/2 {
			quadrant = 2
		}
		if loc.Y >= (nbLines+1)/2 {
			quadrant++
		}
		quadrants[quadrant] += nb
	}
	return quadrants
}

func calc(filename string, nbLines, nbCols, timesteps int) int {
	robots := readInput(filename)
	pos := move(robots, nbCols, nbLines, timesteps)
	quadrants := computeQuadrants(pos, nbCols, nbLines)
	return aoc.Prod(quadrants)
}

func drawPic(pos map[aoc.Tuple]int, nbLines, nbCols, idx int) {
	ctx := gg.NewContext(101, 103)
	for y := range nbLines {
		for x := range nbCols {
			n := pos[aoc.Tuple{X: x, Y: y}]
			if n > 0 {
				ctx.SetColor(color.White)
			} else {
				ctx.SetColor(color.Black)
			}
			ctx.SetPixel(x, y)
		}
	}
	err := ctx.SavePNG(fmt.Sprintf("p%d.png", idx))
	if err != nil {
		panic(err)
	}
}

func calc2(filename string, nbLines, nbCols int) {
	robots := readInput(filename)
	pos := move(robots, nbCols, nbLines, 7344)
	drawPic(pos, nbLines, nbCols, 7344)
}

func main() {
	aoc.CheckEquals(12, calc("test.txt", 7, 11, 100))
	fmt.Println(calc("input.txt", 103, 101, 100))
	calc2("input.txt", 103, 101)
}
