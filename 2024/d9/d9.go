package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

func readInput(filename string) []int {
	inStr := aoc.ReadInputLines(filename)[0]
	blocks := make([]int, 0)
	for i, r := range inStr {
		val := int(r) - 48
		for range val {
			if i%2 == 0 {
				blocks = append(blocks, i/2)
			} else {
				blocks = append(blocks, -1)
			}
		}
	}
	return blocks
}

func readInput2(filename string) ([]int, map[int]aoc.Tuple, []aoc.Tuple, int) {
	inStr := aoc.ReadInputLines(filename)[0]
	blocks := make([]int, 0)
	files := make(map[int]aoc.Tuple)
	freeSpace := make([]aoc.Tuple, 0)
	var curIdx, curLoc int
	for i, r := range inStr {
		val := int(r) - 48
		if i%2 == 0 {
			for range val {
				blocks = append(blocks, i/2)
			}
			files[curIdx] = aoc.Tuple{X: curLoc, Y: val}
			curIdx++
		} else {
			for range val {
				blocks = append(blocks, -1)
			}
			freeSpace = append(freeSpace, aoc.Tuple{X: curLoc, Y: val})
		}
		curLoc += val
	}

	return blocks, files, freeSpace, curIdx - 1
}

func computeHash(blocks []int) int {
	hash := 0
	for idx, val := range blocks {
		if val != -1 {
			hash += idx * val
		}
	}
	return hash
}

func calc(filename string) int {
	blocks := readInput(filename)
	lastFileIdx := len(blocks) - 1
	for idx := 0; idx < lastFileIdx; idx++ {
		val := blocks[idx]
		if val == -1 {
			blocks[idx], blocks[lastFileIdx] = blocks[lastFileIdx], blocks[idx]
			for blocks[lastFileIdx] == -1 {
				lastFileIdx--
			}
		}
	}
	return computeHash(blocks)
}

func calc2(filename string) int {
	blocks, files, freeSpace, lastFileIdx := readInput2(filename)
	for idx := lastFileIdx; idx >= 0; idx-- {
		file := files[idx]
		for spaceIdx, space := range freeSpace {
			if space.X >= file.X {
				break
			}
			if space.Y >= file.Y {
				freeSpace[spaceIdx].Y -= file.Y
				freeSpace[spaceIdx].X += file.Y
				for i := range file.Y {
					blocks[i+space.X] = idx
					blocks[i+file.X] = -1
				}
				break
			}
		}
	}
	return computeHash(blocks)
}

func main() {
	aoc.CheckEquals(1928, calc("test.txt"))
	fmt.Println(calc("input.txt"))
	aoc.CheckEquals(2858, calc2("test.txt"))
	fmt.Println(calc2("input.txt"))
}
