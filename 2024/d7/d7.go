package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"math/big"
	"strconv"
	"strings"
)

type ops byte

const (
	opsPlus = iota
	opsMul
	opsCat
)

var (
	permCache2 = make(map[int][][]ops)
	permCache3 = make(map[int][][]ops)
)

func generatePerm2(length int) [][]ops {
	cache, exists := permCache2[length]
	if exists {
		return cache
	}
	res := make([][]ops, aoc.Pow(2, length))
	for i := range res {
		res[i] = make([]ops, length)
		for j := range res[i] {
			if (i)&(1<<j) > 0 {
				res[i][j] = opsMul
			}
		}
	}
	permCache2[length] = res
	return res
}

func generatePerm3(length int) [][]ops {
	cache, exists := permCache3[length]
	if exists {
		return cache
	}
	res := make([][]ops, aoc.Pow(3, length))
	for i := range res {
		ternStr := fmt.Sprintf("%03s", big.NewInt(int64(i)).Text(3))
		res[i] = make([]ops, length)
		for j, c := range ternStr {
			if len(ternStr)-j-1 < len(res[i]) {
				res[i][len(ternStr)-j-1] = ops(c - 48)
			}
		}
	}
	return res
}

func calc(filename string, order int) int {
	var res int
	equations := aoc.ReadInputLines(filename)
	for _, line := range equations {
		result, _ := strconv.Atoi(strings.Split(line, ":")[0])
		chunks := strings.Split(line, " ")[1:]
		numbers := make([]int, len(chunks))
		for i, chunk := range chunks {
			numbers[i], _ = strconv.Atoi(chunk)
		}
		genPerm := generatePerm2
		if order == 3 {
			genPerm = generatePerm3
		}
		for _, p := range genPerm(len(chunks) - 1) {
			if doOperation(numbers, p, result) {
				res += result
				break
			}
		}
	}
	return res
}

func doOperation(numbers []int, operators []ops, result int) bool {
	res := numbers[0]
	for i, op := range operators {
		res = doOp(res, numbers[i+1], op)
	}
	return res == result
}

func doOp(a, b int, op ops) int {
	switch op {
	case opsPlus:
		return a + b
	case opsMul:
		return a * b
	case opsCat:
		val, _ := strconv.Atoi(fmt.Sprintf("%d%d", a, b))
		return val
	}
	panic("haha")
}

func main() {
	aoc.CheckEquals(3749, calc("test.txt", 2))
	fmt.Println(calc("input.txt", 2))
	aoc.CheckEquals(11387, calc("test.txt", 3))
	fmt.Println(calc("input.txt", 3))
}
