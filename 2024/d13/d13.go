package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"math"
	"regexp"
	"strconv"
)

var (
	pa = regexp.MustCompile(`Button A: X\+(\d+), Y\+(\d+)`)
	pb = regexp.MustCompile(`Button B: X\+(\d+), Y\+(\d+)`)
	pp = regexp.MustCompile(`Prize: X=(\d+), Y=(\d+)`)
)

type machine struct {
	buttonA aoc.Tuple
	buttonB aoc.Tuple
	prize   aoc.Tuple
}

func (m machine) saignedesyeux() (bool, int, int) {
	px := float64(m.prize.X)
	py := float64(m.prize.Y)
	ax := float64(m.buttonA.X)
	ay := float64(m.buttonA.Y)
	bx := float64(m.buttonB.X)
	by := float64(m.buttonB.Y)
	alpha := ay / ax
	beta := by / bx
	gamma := py / px
	if math.Min(alpha, beta) > gamma || gamma > math.Max(alpha, beta) {
		return false, 0, 0
	}
	/*
		We project P(rize) onto the (button)A line alongside the (button)B line, we get M
		solve this system:
		P - x * B € A
		py - beta * x = alpha * (px - x)
		x = (py - alpha * px) / (beta - alpha)
	*/
	x := (py - alpha*px) / (beta - alpha)
	delta := math.Abs(x - float64(int(math.Round(x))))
	const tolerance = 1e-2
	if delta > tolerance {
		return false, 0, 0
	}
	mx := px - x
	my := py - x*beta
	x2 := mx / ax
	delta = math.Abs(x2 - float64(int(math.Round(x2))))
	if delta > tolerance {
		return false, 0, 0
	}
	y2 := my / ay
	delta = math.Abs(y2 - float64(int(math.Round(y2))))
	if delta > tolerance {
		return false, 0, 0
	}
	delta = math.Abs(x2 - y2)
	if delta > tolerance {
		return false, 0, 0
	}
	nbA := int(math.Round(x2))
	nbB := int(math.Round(x / bx))
	return true, nbA, nbB
}

func calc(filename string, part2 bool) int {
	blocks := aoc.ReadByBlocks(filename)
	var result int
	for _, block := range blocks {
		a := pa.FindStringSubmatch(block[0])
		b := pb.FindStringSubmatch(block[1])
		p := pp.FindStringSubmatch(block[2])
		ax, _ := strconv.Atoi(a[1])
		ay, _ := strconv.Atoi(a[2])
		bx, _ := strconv.Atoi(b[1])
		by, _ := strconv.Atoi(b[2])
		px, _ := strconv.Atoi(p[1])
		py, _ := strconv.Atoi(p[2])
		m := machine{
			buttonA: aoc.Tuple{X: ax, Y: ay},
			buttonB: aoc.Tuple{X: bx, Y: by},
			prize:   aoc.Tuple{X: px, Y: py},
		}
		if part2 {
			m.prize = aoc.Tuple{X: 10000000000000 + px, Y: 10000000000000 + py}
		}
		canReach, nbA, nbB := m.saignedesyeux()
		if canReach {
			result += 3*nbA + nbB
			continue
		}
	}
	return result
}

func main() {
	aoc.CheckEquals(480, calc("test.txt", false))
	fmt.Println(calc("input.txt", false))
	fmt.Println(calc("input.txt", true))
}
