package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"sync"
)

var (
	mutex        = &sync.Mutex{}
	orientations = []*aoc.Tuple{
		{X: 0, Y: -1},
		{X: 1, Y: 0},
		{X: 0, Y: 1},
		{X: -1, Y: 0},
	}
)

type guardDef struct {
	initialLoc  aoc.Tuple
	curLoc      *aoc.Tuple
	visited     map[aoc.Tuple]map[int]struct{}
	orientation int
}

func readInput(filename string) (*aoc.Grid, *guardDef) {
	mutex.Lock()
	defer mutex.Unlock()
	grid := aoc.NewGridFromFile(filename, false)
	guardLocs := grid.FindAll(func(_ int, _ int, r rune) bool {
		return r == '^'
	})
	guardLoc := guardLocs[0]
	guard := newGuard(guardLoc)

	return grid, guard
}

func (g *guardDef) visit(loc aoc.Tuple, orientation int) {
	_, ex := g.visited[loc]
	if !ex {
		g.visited[loc] = make(map[int]struct{})
	}
	g.visited[loc][orientation] = struct{}{}
}

func newGuard(guardLoc aoc.Tuple) *guardDef {
	guard := &guardDef{
		initialLoc:  guardLoc,
		curLoc:      &guardLoc,
		visited:     make(map[aoc.Tuple]map[int]struct{}),
		orientation: 0,
	}
	guard.visit(guardLoc, 0)
	return guard
}

func (g *guardDef) step(grid *aoc.Grid) (bool, bool) {
	ahead, coordsAhead, inside := g.getCellInFront(grid)
	if !inside {
		return true, false
	}
	if ahead == '#' {
		g.visit(*g.curLoc, g.orientation)
		g.orientation++
		if g.orientation > 3 {
			g.orientation = 0
		}
		return false, false
	}
	var looped bool
	_, ok := g.visited[coordsAhead]
	if ok {
		_, looped = g.visited[coordsAhead][g.orientation]
	}
	g.visit(coordsAhead, g.orientation)
	g.curLoc = &coordsAhead
	return false, looped
}

func (g *guardDef) getCellInFront(grid *aoc.Grid) (rune, aoc.Tuple, bool) {
	newX := g.curLoc.X + orientations[g.orientation].X
	newY := g.curLoc.Y + orientations[g.orientation].Y
	ahead, inside := grid.Get(newX, newY)
	return ahead, aoc.Tuple{X: newX, Y: newY}, inside
}

func calc(filename string) int {
	grid, guard := readInput(filename)
	var outside bool
	for !outside {
		outside, _ = guard.step(grid)
	}
	return len(guard.visited)
}

func calc2(filename string) int {
	res2 := 0
	grid, baseGuard := readInput(filename)
	for y := range grid.Height {
		for x := range grid.Width {
			cell, _ := grid.Get(x, y)
			if cell == '.' {
				res2 += tryMaze(grid, baseGuard.initialLoc, x, y)
			}
		}
	}
	return res2
}

func tryMaze(grid *aoc.Grid, initialLoc aoc.Tuple, x int, y int) int {
	guard := newGuard(initialLoc)
	grid.Set(x, y, '#')
	for {
		outside, looped := guard.step(grid)
		if looped {
			grid.Set(x, y, '.')
			return 1
		}
		if outside {
			grid.Set(x, y, '.')
			return 0
		}
	}
}

func main() {
	aoc.CheckEquals(41, calc("test.txt"))
	fmt.Println(calc("input.txt"))
	aoc.CheckEquals(6, calc2("test.txt"))
	fmt.Println(calc2("input.txt"))
}
