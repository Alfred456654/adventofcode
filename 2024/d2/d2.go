package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"slices"
)

func calc(filename string) (int, int) {
	grid := aoc.ReadIntInputLines(filename)
	res := 0
	violations := make(map[int]int)
	for i, line := range grid {
		v := aoc.Max(isSafe(line, 1), isSafe(line, -1))
		if v == 1000 {
			res++
		} else {
			violations[i] = v
		}
	}
	res2 := 0
	for lineNb, value := range violations {
		for i := aoc.Max(0, value-1); i <= aoc.Min(len(grid[lineNb])-1, value+1); i++ {
			newLine := slices.Clone(grid[lineNb])
			newLine = append(newLine[:i], newLine[i+1:]...)
			v := aoc.Max(isSafe(newLine, 1), isSafe(newLine, -1))
			if v == 1000 {
				res2++
				break
			}
		}
	}
	return res, res + res2
}

func isSafe(line []int, order int) int {
	for i := range len(line) - 1 {
		p := line[i]
		n := line[i+1]
		d := order * (n - p)
		if d < 1 || d > 3 {
			return i
		}
	}
	return 1000
}

func main() {
	t1, t2 := calc("test.txt")
	aoc.CheckEquals(2, t1)
	aoc.CheckEquals(4, t2)
	fmt.Println(calc("input.txt"))
}
