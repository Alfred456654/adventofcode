package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"math"
)

func blink(stones map[int]int) map[int]int {
	res := make(map[int]int)
	for stoneVal, amount := range stones {
		nDigits := 0
		if stoneVal > 0 {
			nDigits = 1 + int(math.Log10(float64(stoneVal)))
		}
		switch {
		case stoneVal == 0:
			res[1] += amount
		case nDigits%2 == 0:
			split := aoc.Pow(10, nDigits/2)
			res[(stoneVal / split)] += amount
			res[(stoneVal % split)] += amount
		default:
			res[stoneVal*2024] += amount
		}
	}
	return res
}

func mapSum(stones map[int]int) int {
	var res int
	for _, amount := range stones {
		res += amount
	}
	return res
}

func calc(filename string, nbOps int) int {
	stones := make(map[int]int)
	for _, stone := range aoc.ReadIntInputLines(filename)[0] {
		stones[stone]++
	}
	for range nbOps {
		stones = blink(stones)
	}
	return mapSum(stones)
}

func main() {
	aoc.CheckEquals(7, calc("test.txt", 1))
	aoc.CheckEquals(22, calc("test2.txt", 6))
	aoc.CheckEquals(55312, calc("test2.txt", 25))
	fmt.Println(calc("input.txt", 25))
	fmt.Println(calc("input.txt", 75))
}
