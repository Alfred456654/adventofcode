package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"sort"
	"strconv"
	"strings"
)

func calc(filename string) (int, int) {
	lines := aoc.ReadInputLines(filename)
	t1, t2 := make([]int, len(lines)), make([]int, len(lines))
	m1, m2 := make(map[int]int), make(map[int]int)
	for i, l := range lines {
		chunks := strings.Split(l, "   ")
		t1[i], _ = strconv.Atoi(chunks[0])
		t2[i], _ = strconv.Atoi(chunks[1])
		m1[t1[i]] += 1
		m2[t2[i]] += 1
	}
	sort.Ints(t1)
	sort.Ints(t2)
	d := 0
	s := 0
	for i, l1 := range t1 {
		d += aoc.Abs(l1 - t2[i])
		s += l1 * m2[l1]
	}
	return d, s
}

func main() {
	t1, t2 := calc("test.txt")
	aoc.CheckEquals(11, t1)
	aoc.CheckEquals(31, t2)
	fmt.Println(calc("input.txt"))
}
