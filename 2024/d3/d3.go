package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"regexp"
	"strconv"
)

var (
	MulPattern = regexp.MustCompile(`mul\((\d+),(\d+)\)`)
	DoOrDont   = regexp.MustCompile(`(do\(\)|don't\(\))`)
)

func calc2(filename string) (int, int) {
	operations := aoc.ReadInputLines(filename)
	var operation string
	for _, line := range operations {
		operation += line
	}
	markers := DoOrDont.FindAllString(operation, -1)
	chunks := DoOrDont.Split(operation, -1)
	resDo := doMultiplications(chunks[0])
	resDont := 0
	for i, marker := range markers {
		if marker == "do()" {
			resDo += doMultiplications(chunks[i+1])
		} else {
			resDont += doMultiplications(chunks[i+1])
		}
	}
	return resDont + resDo, resDo
}

func doMultiplications(operation string) int {
	muls := MulPattern.FindAllStringSubmatch(operation, -1)
	res := 0
	for _, mul := range muls {
		n1, _ := strconv.Atoi(mul[1])
		n2, _ := strconv.Atoi(mul[2])
		res += n1 * n2
	}
	return res
}

func main() {
	t1, _ := calc2("test.txt")
	aoc.CheckEquals(161, t1)
	_, t2 := calc2("test2.txt")
	aoc.CheckEquals(48, t2)
	fmt.Println(calc2("input.txt"))
}
