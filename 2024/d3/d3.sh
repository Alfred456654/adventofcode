#!/bin/bash
GO=1
RES=0
while read -r line; do
    if [ "$line" = "don't()" ]; then
        GO=0
    elif [ "$line" = "do()" ]; then
        GO=1
    elif [ "$GO" -eq 1 ]; then
        echo "$line"
        N0=$(echo "$line" | cut -d ',' -f 1 | cut -d '(' -f 2)
        N1=$(echo "$line" | cut -d ',' -f 2 | cut -d ')' -f 1)
        RES=$((RES + N0 * N1))
    fi
done < <(grep -E -oe 'mul\([0-9]*,[0-9]*\)' -e "do\(\)" -e "don't\(\)" input.txt)
echo "$RES"
