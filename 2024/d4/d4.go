package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

func calc(filename string) int {
	res := 0
	grid := aoc.NewGridFromFile(filename, false)
	xLocs := grid.FindAll(func(_, _ int, letter rune) bool {
		return letter == 'X'
	})
	for _, xLoc := range xLocs {
		mLocs := grid.GetNeighbours(xLoc.X, xLoc.Y, func(_, _ int, neighb rune) bool {
			return neighb == 'M'
		})
		for mLoc := range mLocs {
			dx := mLoc.X - xLoc.X
			dy := mLoc.Y - xLoc.Y
			aLoc, ok1 := grid.Get(mLoc.X+dx, mLoc.Y+dy)
			sLoc, ok2 := grid.Get(mLoc.X+2*dx, mLoc.Y+2*dy)
			if ok1 && ok2 && aLoc == 'A' && sLoc == 'S' {
				res++
			}
		}
	}
	return res
}

func calc2(filename string) int {
	res := 0
	grid := aoc.NewGridFromFile(filename, false)
	aLocs := grid.FindAll(func(_, _ int, letter rune) bool {
		return letter == 'A'
	})
	for _, aLoc := range aLocs {
		mLocs := grid.GetNeighbours(aLoc.X, aLoc.Y, func(_, _ int, neighb rune) bool {
			return neighb == 'M'
		})
		for mLoc := range mLocs {
			dx := mLoc.X - aLoc.X
			dy := mLoc.Y - aLoc.Y
			if dx*dy != 0 {
				opposite, ok1 := grid.Get(aLoc.X-dx, aLoc.Y-dy)
				diag1, ok2 := grid.Get(aLoc.X-dx, aLoc.Y+dy)
				diag2, ok3 := grid.Get(aLoc.X+dx, aLoc.Y-dy)
				if diag1 > diag2 {
					diag2, diag1 = diag1, diag2
				}
				if ok1 && ok2 && ok3 && opposite == 'S' && diag1 == 'M' && diag2 == 'S' {
					res++
				}
			}
		}
	}
	return res / 2
}

func main() {
	aoc.CheckEquals(4, calc("testA.txt"))
	aoc.CheckEquals(18, calc("testB.txt"))
	aoc.CheckEquals(1, calc2("test2A.txt"))
	aoc.CheckEquals(9, calc2("test2B.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
