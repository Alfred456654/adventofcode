package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

func readInput(filename string) (*aoc.Grid, map[byte][]aoc.Tuple) {
	grid := aoc.NewGridFromFile(filename, false)
	antennaGroups := make(map[byte][]aoc.Tuple)
	byteRanges := [][]byte{
		{48, 58},  // 0-9
		{65, 91},  // A-Z
		{97, 123}, // a-z
	}
	var b byte
	for _, br := range byteRanges {
		for b = br[0]; b < br[1]; b++ {
			locs := grid.FindAll(func(_ int, _ int, r rune) bool {
				return byte(r) == b
			})
			if len(locs) > 0 {
				antennaGroups[b] = locs
			}
		}
	}
	return grid, antennaGroups
}

func computeAntinodes(locations []aoc.Tuple, antinodes map[aoc.Tuple]struct{}, grid *aoc.Grid) {
	for i, ant1 := range locations {
		for j, ant2 := range locations {
			if i != j {
				delta := ant1.Sub(ant2)
				antinodeLoc := ant1.Add(delta)
				_, ok := grid.GetAt(antinodeLoc)
				if ok {
					antinodes[antinodeLoc] = struct{}{}
				}
			}
		}
	}
}

func computeAntinodes2(locations []aoc.Tuple, antinodes map[aoc.Tuple]struct{}, grid *aoc.Grid) {
	for i, ant1 := range locations {
		for j, ant2 := range locations {
			if i != j {
				delta := ant1.Sub(ant2)
				stillInside := true
				antinodeLoc := ant1
				for stillInside {
					antinodes[antinodeLoc] = struct{}{}
					antinodeLoc = antinodeLoc.Add(delta)
					_, stillInside = grid.GetAt(antinodeLoc)
				}
			}
		}
	}
}

func calc(filename string) int {
	grid, antennas := readInput(filename)
	antinodes := make(map[aoc.Tuple]struct{})
	for _, locations := range antennas {
		computeAntinodes(locations, antinodes, grid)
	}
	return len(antinodes)
}

func calc2(filename string) int {
	grid, antennaGroups := readInput(filename)
	antinodes := make(map[aoc.Tuple]struct{})
	for _, locations := range antennaGroups {
		computeAntinodes2(locations, antinodes, grid)
	}
	return len(antinodes)
}

func main() {
	aoc.CheckEquals(14, calc("test.txt"))
	fmt.Println(calc("input.txt"))
	aoc.CheckEquals(34, calc2("test.txt"))
	fmt.Println(calc2("input.txt"))
}
