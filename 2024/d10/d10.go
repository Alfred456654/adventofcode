package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

func trailScore(orig aoc.Tuple, maze *aoc.Grid, endsSet *aoc.TupleSet) int {
	curVal, _ := maze.GetAt(orig)
	plusOne := maze.GetNeighbours(orig.X, orig.Y, func(x int, y int, r rune) bool {
		if r != curVal+1 || (orig.X-x)*(orig.Y-y) != 0 {
			return false
		}
		if r == '9' {
			tuple := aoc.Tuple{X: x, Y: y}
			if endsSet.Contains(tuple) {
				endsSet.Remove(tuple)
				return true
			}
			return false
		}
		return true
	})
	if curVal == '8' {
		return len(plusOne)
	}
	var res int
	for next := range plusOne {
		res += trailScore(next, maze, endsSet)
	}
	return res
}

func trailRating(orig aoc.Tuple, maze *aoc.Grid) int {
	curVal, _ := maze.GetAt(orig)
	plusOne := maze.GetNeighbours(orig.X, orig.Y, func(x int, y int, r rune) bool {
		return r == curVal+1 && (orig.X-x)*(orig.Y-y) == 0
	})
	var res int
	for next := range plusOne {
		if curVal == '8' {
			return len(plusOne)
		}
		res += trailRating(next, maze)
	}
	return res
}

func calc(filename string, part2 bool) int {
	maze := aoc.NewGridFromFile(filename, false)
	origins := maze.FindAll(func(_ int, _ int, r rune) bool {
		return r == '0'
	})
	ends := maze.FindAll(func(_ int, _ int, r rune) bool {
		return r == '9'
	})
	var res int
	for _, orig := range origins {
		if part2 {
			res += trailRating(orig, maze)
		} else {
			endsMap := aoc.NewTupleSet()
			endsMap.AddAll(ends)
			res += trailScore(orig, maze, endsMap)
		}
	}
	return res
}

func main() {
	for test, exp := range map[string]int{
		"test1.txt": 1,
		"test2.txt": 2,
		"test3.txt": 4,
		"test4.txt": 3,
		"test5.txt": 36,
	} {
		aoc.CheckEqualsMsg(exp, calc(test, false), test)
	}
	fmt.Println(calc("input.txt", false))
	for test, exp := range map[string]int{
		"test6.txt": 3,
		"test7.txt": 13,
		"test8.txt": 227,
		"test9.txt": 81,
	} {
		aoc.CheckEqualsMsg(exp, calc(test, true), test)
	}
	fmt.Println(calc("input.txt", true))
}
