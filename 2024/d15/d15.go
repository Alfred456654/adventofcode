package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strings"
)

var (
	left   = aoc.Tuple{X: -1, Y: 0}
	up     = aoc.Tuple{X: 0, Y: -1}
	right  = aoc.Tuple{X: 1, Y: 0}
	down   = aoc.Tuple{X: 0, Y: 1}
	arrows = map[rune]aoc.Tuple{
		'<': left,
		'^': up,
		'>': right,
		'v': down,
	}
	other = map[rune]aoc.Tuple{
		'[': right,
		']': left,
	}
)

func showMaze(maze *aoc.Grid) {
	for y := range maze.Height {
		for x := range maze.Width {
			r, _ := maze.Get(x, y)
			fmt.Printf("%c", r)
		}
		fmt.Println()
	}
	fmt.Println()
}

func readMovements(movLines []string) []aoc.Tuple {
	movStr := strings.Builder{}
	for _, l := range movLines {
		movStr.WriteString(l)
	}
	movements := make([]aoc.Tuple, movStr.Len())
	for i, mov := range movStr.String() {
		movements[i] = arrows[mov]
	}
	return movements
}

func readInput(filename string, part2 bool) ([]aoc.Tuple, *aoc.Grid) {
	blocks := aoc.ReadByBlocks(filename)
	var maze *aoc.Grid
	if part2 {
		maze = makePart2Maze(blocks[0])
	} else {
		maze = makePart1Maze(blocks[0])
	}
	movements := readMovements(blocks[1])
	return movements, maze
}

func makePart1Maze(mazeDef []string) *aoc.Grid {
	maze := aoc.NewGrid(len(mazeDef[0]), len(mazeDef), false)
	for y := range mazeDef {
		for x := range mazeDef[0] {
			maze.Set(x, y, rune(mazeDef[y][x]))
		}
	}
	return maze
}

func makePart2Maze(mazeDef []string) *aoc.Grid {
	maze := aoc.NewGrid(2*len(mazeDef[0]), len(mazeDef), false)
	for y := range mazeDef {
		for x := range mazeDef[0] {
			switch mazeDef[y][x] {
			case '#':
				maze.Set(2*x, y, '#')
				maze.Set(2*x+1, y, '#')
			case '.':
				maze.Set(2*x, y, '.')
				maze.Set(2*x+1, y, '.')
			case 'O':
				maze.Set(2*x, y, '[')
				maze.Set(2*x+1, y, ']')
			case '@':
				maze.Set(2*x, y, '@')
				maze.Set(2*x+1, y, '.')
			}
		}
	}
	return maze
}

func canMove(maze *aoc.Grid, mov, loc aoc.Tuple) bool {
	neighbLoc := loc.Add(mov)
	neighbRune, inside := maze.GetAt(neighbLoc)
	if !inside || neighbRune == '#' {
		return false
	}
	if neighbRune == '.' {
		return true
	}
	if neighbRune == '[' || neighbRune == ']' {
		box2 := neighbLoc.Add(other[neighbRune])
		half1CanMove := canMove(maze, mov, neighbLoc)
		half2CanMove := true
		if !box2.Equals(loc) {
			half2CanMove = canMove(maze, mov, box2)
		}
		return half1CanMove && half2CanMove
	}
	return canMove(maze, mov, neighbLoc)
}

func attemptMove(maze *aoc.Grid, mov, loc aoc.Tuple) (aoc.Tuple, bool) {
	neighbLoc := loc.Add(mov)
	neighbRune, inside := maze.GetAt(neighbLoc)
	if !inside || neighbRune == '#' {
		return loc, false
	}
	thisRune, _ := maze.GetAt(loc)
	if neighbRune == '.' {
		maze.SetAt(neighbLoc, thisRune)
		maze.SetAt(loc, '.')
		return neighbLoc, true
	}
	if (neighbRune == '[' || neighbRune == ']') && (mov.Equals(up) || mov.Equals(down)) {
		box2 := neighbLoc.Add(other[neighbRune])
		if !box2.Equals(loc) {
			if canMove(maze, mov, neighbLoc) && canMove(maze, mov, box2) {
				_, _ = attemptMove(maze, mov, box2)
				_, _ = attemptMove(maze, mov, neighbLoc)
				maze.SetAt(neighbLoc, thisRune)
				maze.SetAt(loc, '.')
				return neighbLoc, true
			} else {
				return loc, false
			}
		}
		return neighbLoc, true
	}
	if _, ok := attemptMove(maze, mov, neighbLoc); ok {
		maze.SetAt(neighbLoc, thisRune)
		maze.SetAt(loc, '.')
		return neighbLoc, true
	}
	return loc, false
}

func gps(maze *aoc.Grid, boxRune rune) int {
	boxes := maze.FindAll(func(_ int, _ int, r rune) bool {
		return r == boxRune
	})
	var res int
	for _, box := range boxes {
		res += box.X + 100*box.Y
	}
	return res
}

func calc(filename string, part2 bool) int {
	movements, maze := readInput(filename, part2)
	loc := maze.FindAll(func(_ int, _ int, r rune) bool {
		return r == '@'
	})[0]
	for _, mov := range movements {
		loc, _ = attemptMove(maze, mov, loc)
	}
	boxRune := 'O'
	if part2 {
		boxRune = '['
	}
	if filename == "test.txt" || filename == "test3.txt" {
		showMaze(maze)
	}
	return gps(maze, boxRune)
}

func main() {
	_ = calc("test.txt", false)
	aoc.CheckEquals(10092, calc("test2.txt", false))
	aoc.CheckEquals(9021, calc("test2.txt", true))
	_ = calc("test3.txt", true)
	fmt.Println(calc("input.txt", false))
	fmt.Println(calc("input.txt", true))
}
