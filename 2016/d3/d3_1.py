import re
three_numbers = re.compile('[^0-9]*([0-9]*)[^0-9]*([0-9]*)[^0-9]*([0-9]*)[^0-9]*')
f = open('input', 'r')
nb_possible = 0
for i in f.readlines():
    nbrs_match = three_numbers.match(i.strip())
    nbrs = []
    for n in nbrs_match.groups():
        nbrs.append(int(n))
    nbrs.sort()
    if nbrs[0] + nbrs[1] > nbrs[2]:
        nb_possible = nb_possible + 1

print(nb_possible)
