offset = 1362
departure = (1, 1)
arrival = (31, 39)
def formula(x, y):
    return bin(x*x + 3*x + 2*x*y + y + y*y + offset)[2:].count('1') % 2
def get_neighbours(visited, node):
    x = node[0]
    y = node[1]
    answer = [ ]
    for i in range(x - 1, x + 2, 2):
        if i > 0 and formula(i, y) == 0 and not (i, y) in visited:
            answer.append((i, y))
    for j in range(y - 1, y + 2, 2):
        if j > 0 and formula(x, j) == 0 and not (x, j) in visited:
            answer.append((x, j))
    return answer
if __name__ == '__main__':
    queue = [ departure ]
    visited = [ ]
    tree = { }
    while queue:
        node = queue.pop(0)
        visited.append(node)
        if node == arrival:
            fallback = str(node)
            cpt = 0
            while fallback != str(departure):
                fallback = tree[fallback]
                cpt += 1
            print(cpt)
            break
        for adj in get_neighbours(visited, node):
            tree.update({str(adj): str(node)})
            queue.append(adj)
