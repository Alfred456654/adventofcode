values_to_find = [17, 61]
class Bot:
    def __init__(self, b_id):
        self.b_id = b_id
        self.chips = [ ]
    def __str__(self):
        return 'Bot %d' % self.b_id
    def add_initial_chip(self, chip_value):
        self.chips.append(chip_value)
    def receiver_low(self, receiver):
        self.receiver_low = receiver
    def receiver_high(self, receiver):
        self.receiver_high = receiver
    def receive_chip(self, chip_value):
        self.chips.append(chip_value)
        if len(self.chips) == 2:
            if sorted(self.chips) == values_to_find:
                print('%s responsible for comparing %d and %d' % (self, values_to_find[0], values_to_find[1]))
            self.give_low_chip()
            self.give_high_chip()
    def give_low_chip(self):
        if len(self.chips) < 1:
            return 0
        else:
            chip_value = min(self.chips)
            self.chips.remove(chip_value)
            self.receiver_low.receive_chip(chip_value)
    def give_high_chip(self):
        if len(self.chips) < 1:
            return 0
        else:
            chip_value = min(self.chips)
            self.chips.remove(chip_value)
            self.receiver_high.receive_chip(chip_value)
class Output:
    def __init__(self, o_id):
        self.o_id = o_id
        self.chips = []
    def __str__(self):
        return 'Output %d' % o_id
    def receive_chip(self, chip_value):
        self.chips.append(chip_value)
if __name__ == '__main__':
    bots_dict = { }
    output_dict = { }
    with open('input', 'r') as f:
        value_instrs = []
        bot_instrs = []
        for i in f.readlines():
            j = i.strip().split(' ')
            if j[0] == 'value':
                value_instrs.append(j)
            else:
                bot_instrs.append(j)
        for i in value_instrs:
            chip_value = int(i[1])
            b_id = int(i[5])
            if b_id in bots_dict:
                bots_dict[b_id].add_initial_chip(chip_value)
            else:
                new_bot = Bot(b_id)
                new_bot.add_initial_chip(chip_value)
                bots_dict.update({b_id: new_bot})
        for i in bot_instrs:
            giver_id = int(i[1])
            low_id = int(i[6])
            high_id = int(i[11])
            if giver_id not in bots_dict:
                bots_dict.update({giver_id: Bot(giver_id)})
            giver = bots_dict[giver_id]
            if i[5] == 'bot':
                if not low_id in bots_dict:
                    bots_dict.update({low_id: Bot(low_id)})
                low_receiver = bots_dict[low_id]
            else:
                if not low_id in output_dict:
                    output_dict.update({low_id: Output(low_id)})
                low_receiver = output_dict[low_id]
            if i[10] == 'bot':
                if not high_id in bots_dict:
                    bots_dict.update({high_id: Bot(high_id)})
                high_receiver = bots_dict[high_id]
            else:
                if not high_id in output_dict:
                    output_dict.update({high_id: Output(high_id)})
                high_receiver = output_dict[high_id]
            giver.receiver_low = low_receiver
            giver.receiver_high = high_receiver
        for b_id in bots_dict:
            bot = bots_dict[b_id]
            if len(bot.chips) == 2:
                break
        bot.give_low_chip()
        bot.give_high_chip()
        answer_2 = 1
        for i in range(3):
            for j in output_dict[i].chips:
                answer_2 *= j
        print('Product of 3 first outputs: %d' % answer_2)
