#!/usr/bin/env python3
from statistics import mode
answer = ''
with open('input', 'r') as f:
    a = list(map(str.strip, f.readlines()))
    b = [''] * 8
    for i in range(8):
        for j in a:
            b[i] += j[i]
    for k in b:
        answer += mode(k)
print(answer)
