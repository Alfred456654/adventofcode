#!/usr/bin/env python3
from collections import Counter
answer = ''
with open('input', 'r') as f:
    a = list(map(str.strip, f.readlines()))
    b = [''] * 8
    for i in range(8):
        for j in a:
            b[i] += j[i]
    for k in b:
        answer += Counter(k).most_common()[:-2:-1][0][0]
print(answer)
