import re
import collections
room_pattern = re.compile('([a-z-]+)([0-9]+)\[([a-z]{5})\]')
f = open('input', 'r')
rooms = f.readlines()
answer = 0
for room in rooms:
    match = room_pattern.match(room.strip())
    letters = match.group(1).replace('-', '')
    room_id = int(match.group(2))
    checksum = match.group(3)
    letters_freq = collections.Counter(letters)
    five_most = letters_freq.most_common(5)
    min_cnt = five_most[len(five_most) - 1][1]
    count_this = True
    for l in checksum:
        count_this = count_this and letters_freq[l] >= min_cnt
    for l in five_most:
        if letters_freq[l[0]] > min_cnt:
            count_this = count_this and l[0] in checksum
    if count_this:
        answer = answer + room_id
print(answer)
