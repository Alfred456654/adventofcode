from hashlib import md5
input = 'ugkcyxxp'
i = 0
answer = ''
while len(answer) < 8:
    i += 1
    h = md5()
    h.update(input + str(i))
    if h.hexdigest().startswith('00000'):
        answer += h.hexdigest()[5]
print(answer)
