import re
from hashlib import md5
salt = r'ngcjuoqr'
three = re.compile(r"(.)\1\1")
five = re.compile(r"(.)\1\1\1\1")
if __name__ == '__main__':
    mem3 = { }
    mem5 = { }
    idx = 0
    finished = False
    answers = [ ]
    while not finished:
        idx += 1
        h = md5()
        h.update((salt + str(idx)).encode('us-ascii'))
        s = h.hexdigest()
        for k in range(2016):
            h = md5()
            h.update(s.encode('us-ascii'))
            s = h.hexdigest()
        m3 = three.search(s)
        if m3:
            mem3.update({idx: m3.group(1)})
        m5 = five.search(s)
        if m5:
            mem5.update({idx: m5.group(1)})
        for i in list(mem3):
            if idx - i > 1000:
                mem3.pop(i)
            else:
                let_3 = mem3[i][0]
                for j in mem5:
                    let_5 = mem5[j][0]
                    if j - i <= 1000 and j > i and let_3 == let_5:
                        answers.append(i)
                        mem3.pop(i)
        finished = len(answers) >= 64 and idx >= 1000 + sorted(answers)[63]
    print(sorted(answers)[63])
