f = open('input', 'r')
l = f.readlines()[0].split(', ')
x,y = 0,0
turn_dir = {'R': -1, 'L': 1}
incr_x = {0: 0, 1: -1, 2: 0, 3: 1}
incr_y = {0: 1, 1: 0, 2: -1, 3: 0}
cur_dir = 0
for i in l:
    cur_dir = (cur_dir + turn_dir[i[0]]) % 4
    nb_blocks = int(i[1:].strip())
    x = x + incr_x[cur_dir] * nb_blocks
    y = y + incr_y[cur_dir] * nb_blocks
print(abs(x) + abs(y))
