#!/usr/bin/env python3

KEYPAD = [[3 * line + col + 1 for col in range(3)] for line in range(3)]
DIRECTION_LETTERS = {'U': (-1, 0), 'L': (0, -1), 'R': (0, 1), 'D': (1, 0)}


def read_input(filename="input2"):
    directions = []
    with open(filename, 'r') as input_file:
        for stripped_line in [read_line.strip() for read_line in input_file.readlines()]:
            directions.append([DIRECTION_LETTERS[letter] for letter in stripped_line])
    return directions


def follow_path(col, line, movements):
    for movement in movements:
        line = min(2, max(0, line + movement[0]))
        col = min(2, max(0, col + movement[1]))
    return KEYPAD[line][col]


if __name__ == '__main__':
    pos_x, pos_y = 1, 1
    read_dirs = read_input()
    answer = ''
    for path in read_dirs:
        answer += str(follow_path(pos_x, pos_y, path))
    print(answer)
