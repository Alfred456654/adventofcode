#!/bin/zsh
#     1
#   2 3 4
# 5 6 7 8 9
#   A B C
#     D
zmodload zsh/mathfunc

X=-2
Y=0

function dist() {
    echo $(( abs($1) + abs($2) ))
}

function number() {
    case $2 in
        -2)
            echo 1
            ;;
        -1)
            echo $((3+$1))
            ;;
        0)
            echo $((7+$1))
            ;;
        1)
            echo "\x$(printf %x $((66+$1)))"
            ;;
        2)
            echo D
            ;;
    esac
}

cat input | while read line; do
    for i in $(seq 1 ${#line[@]}); do
        case ${line[$i]} in
            U)
                Y=$((Y-1))
                if [ $(dist $X $Y) -gt 2 ]; then
                    Y=$((Y+1))
                fi
                ;;
            D)
                Y=$((Y+1))
                if [ $(dist $X $Y) -gt 2 ]; then
                    Y=$((Y-1))
                fi
                ;;
            L)
                X=$((X-1))
                if [ $(dist $X $Y) -gt 2 ]; then
                    X=$((X+1))
                fi
                ;;
            R)
                X=$((X+1))
                if [ $(dist $X $Y) -gt 2 ]; then
                    X=$((X-1))
                fi
                ;;
        esac
    done
    echo "$(number $X $Y)"
done
