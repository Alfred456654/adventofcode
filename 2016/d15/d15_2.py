class Disc:
    def __init__(self, nb_pos, init_pos):
        self.nb_pos = nb_pos
        self.pos = init_pos
    def pos_time(self, t):
        return (self.pos + t) % self.nb_pos
def get_all_positions(discs, t):
    answer = []
    for i in range(len(discs)):
        d = discs[i + 1]
        answer.append(d.pos_time(t))
    return answer
goal = [ 12, 15, 16, 3, 0, 0, 4 ]
instrs = list(map(str.strip, open('input2', 'r').readlines()))
discs = { }
for i in instrs:
    breakdown = i.split(' ')
    d_id = int(breakdown[1].replace('#', ''))
    nb_pos = int(breakdown[3])
    init_pos = int(breakdown[11].replace('.', ''))
    discs.update({d_id: Disc(nb_pos, init_pos)})
status = []
t = 0
while status != goal:
    status = get_all_positions(discs, t)
    t += 1
t -= 1
print(t)
