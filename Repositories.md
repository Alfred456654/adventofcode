# Active

|      Whom      |                                                Where                                                 |      What      |
|:--------------:|:----------------------------------------------------------------------------------------------------:|:--------------:|
|     Alfred     |               [GitLab/Alfred456654](https://www.gitlab.com/Alfred456654/adventofcode)                |       Go       |
|     Arnaud     |                      [GitHub/aoc2024](https://github.com/ArnaudPannatier/aoc24)                      |    Python 3    |
|    Benjamin    |                     [GitHub/baudren](https://github.com/baudren/advent-of-code)                      |    Python 3    |
|    Charles     |                    [GitLab/Picheric](https://gitlab.com/picheryc/advent_of_code)                     |    Python 3    |
|      Dave      | [Google Drive](https://drive.google.com/drive/folders/19sxMbA4YRVO-HXM0HCJy2QGgEjDuHT5S?usp=sharing) |     EXCEL      |
|    Florian     |                      [GitLab/FloMibu](https://gitlab.com/FloMibu/adventofcode)                       | Python 2 (lol) |
|     Ikspe      |                       [GitLab/ikspe](https://gitlab.com/ikspe/advent-of-code)                        |      Rust      |
|     Jasmin     |                  [GitLab/j.fragnaud1](https://gitlab.com/j.fragnaud1/adventofcode)                   |      Rust      |
|       JB       |                        [GitHub/fromenjn](https://github.com/fromenjn/aoc2024)                        |       Go       |
|     Pierre     |                       [GitLab/pelyot](https://gitlab.com/pelyot/adventofcode)                        |      Rust      |
| RobinOffZeWood |                 [GitHub/robinoffzewood](https://github.com/robinoffzewood/aoc-2022)                  |     Python     |
|    sebdotv     |                        [GitHub/sebdotv](https://github.com/sebdotv/aoc-rust)                         |      Rust      |
|     Serans     |                          [GitHub/Serans](https://github.com/serans/aoc2022)                          |      Rust      |

# Inactive

|      Whom      |                                   Where                                   |      What       |
|:--------------:|:-------------------------------------------------------------------------:|:---------------:|
|    Charles     |     [GitLab/picheryc](https://gitlab.com/picheryc/adventofcode2020/)      |    Python 3     |
|   Guillaume    | [GitHub/GuillaumeSmaha](https://github.com/GuillaumeSmaha/advent-of-code) |  Go & Python 3  |
|   R0nd0ud0u    |       [GitHub.com/r0nd0ud0u](https://github.com/r0nd0ud0u/aoc2020)        |       C++       |
|    PtiLuky     |             [GitHub/PtiLuky](https://github.com/PtiLuky/aoc)              |      Rust       |
|    William     |    [GitHub/willi-am-public](https://github.com/willi-am-publi/aoc2020)    |    Python 3     |
|    Syllabus    |       [Repl.it/Syllabus](https://replit.com/@syllabus/AoC2021?v=1)        |      Rust       |
|      Dave      |         [GitLab/dchiron88](https://gitlab.com/dchiron88/aoc2021)          |    Python 3     |
|   DavidXire    |     [GitHub/DavidXire](https://github.com/davidXire/AdventofCode2021)     |    Python 3     |
|    Eagleseb    |      [GitHub/Eagleseb](http://github.com/Eagleseb/AdventOfCode2021)       |     Python      |
| RobinOffZeWood |    [GitHub/robinoffzewood](https://github.com/robinoffzewood/aoc-2021)    |      Rust       |
|     Marlox     |         [GitHub/marlox-ouda](https://github.com/marlox-ouda/aoc)          | Python 3, Go, C |
