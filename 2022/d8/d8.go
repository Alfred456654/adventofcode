package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

func transpose(forest [][]int) [][]int {
	columns := make([][]int, len(forest[0]))
	for i, row := range forest {
		for j := range row {
			if columns[j] == nil {
				columns[j] = make([]int, len(forest))
			}
			columns[j][i] = row[j]
		}
	}
	return columns
}

func isVisible(row, column []int, i, j int) int {
	if i == 0 || j == 0 || i == len(row)-1 || j == len(column)-1 {
		return 1
	}
	height := row[j]

	left := row[:j]
	maxLeft := aoc.ListMax(left)
	if height > maxLeft {
		return 1
	}

	right := row[j+1:]
	maxRight := aoc.ListMax(right)
	if height > maxRight {
		return 1
	}

	top := column[:i]
	maxTop := aoc.ListMax(top)
	if height > maxTop {
		return 1
	}

	down := column[i+1:]
	maxDown := aoc.ListMax(down)
	if height > maxDown {
		return 1
	}

	return 0
}

func score(array []int, index, direction int) int {
	height := array[index]
	result := 0
	for i := index + direction; i >= 0 && i < len(array); i += direction {
		result++
		if array[i] >= height {
			break
		}
	}
	return result
}

func scenicScore(row, column []int, i, j int) int {
	scores := make([]int, 4)
	scores[0] = score(row, j, -1)
	scores[1] = score(row, j, 1)
	scores[2] = score(column, i, -1)
	scores[3] = score(column, i, 1)
	result := 1
	for _, s := range scores {
		result *= s
	}
	return result
}

func calc(filename string) (int, int) {
	forest := aoc.ReadIntRectangle(filename)
	columns := transpose(forest)
	visible := 0
	bestScore := 0
	for i, row := range forest {
		for j := range row {
			column := columns[j]
			visible += isVisible(row, column, i, j)
			bestScore = aoc.Max(bestScore, scenicScore(row, column, i, j))
		}
	}
	return visible, bestScore
}

func main() {
	t1, t2 := calc("test.txt")
	aoc.CheckEquals(21, t1)
	aoc.CheckEquals(8, t2)
	fmt.Println(calc("input.txt"))
}
