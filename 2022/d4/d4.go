package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

type sectionRange struct {
	low  int
	high int
}

func parseLine(line string) (*sectionRange, *sectionRange) {
	elves := strings.Split(line, ",")
	result := make([]*sectionRange, 2)
	for idx, elf := range elves {
		chunks := strings.Split(elf, "-")
		low, _ := strconv.ParseInt(chunks[0], 10, 64)
		high, _ := strconv.ParseInt(chunks[1], 10, 64)
		result[idx] = &sectionRange{
			low:  int(low),
			high: int(high),
		}
	}
	return result[0], result[1]
}

func (p *sectionRange) contains(p2 *sectionRange) bool {
	return p.low <= p2.low && p.high >= p2.high
}

func (p *sectionRange) overlaps(p2 *sectionRange) bool {
	return (p.low <= p2.low && p2.low <= p.high) || (p2.low <= p.low && p.low <= p2.high)
}

func calc(filename string) (int, int) {
	result := 0
	result2 := 0
	for _, line := range aoc.ReadInputLines(filename) {
		elf1, elf2 := parseLine(line)
		if elf1.contains(elf2) || elf2.contains(elf1) {
			result++
		}
		if elf1.overlaps(elf2) {
			result2++
		}
	}
	return result, result2
}

func main() {
	t1, t2 := calc("test.txt")
	aoc.CheckEquals(2, t1)
	aoc.CheckEquals(4, t2)
	fmt.Println(calc("input.txt"))
}
