package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/Alfred456654/aoc-utils"
)

var (
	moves = []aoc.Tuple{
		{X: 0, Y: 1},
		{X: -1, Y: 1},
		{X: 1, Y: 1},
	}
)

func parseTuple(tupleStr string) aoc.Tuple {
	chunks := strings.Split(tupleStr, ",")
	x, _ := strconv.Atoi(chunks[0])
	y, _ := strconv.Atoi(chunks[1])
	return aoc.Tuple{X: x, Y: y}
}

func segment(start, end aoc.Tuple) []aoc.Tuple {
	result := make([]aoc.Tuple, 0)
	if start.X == end.X {
		for y := aoc.Min(start.Y, end.Y); y <= aoc.Max(start.Y, end.Y); y++ {
			result = append(result, aoc.Tuple{X: start.X, Y: y})
		}
	} else {
		for x := aoc.Min(start.X, end.X); x <= aoc.Max(start.X, end.X); x++ {
			result = append(result, aoc.Tuple{X: x, Y: start.Y})
		}
	}
	return result
}

func readInput(filename string) (map[aoc.Tuple]struct{}, int) {
	result := make(map[aoc.Tuple]struct{})
	maxY := 0
	lines := aoc.ReadInputLines(filename)
	for _, line := range lines {
		chunks := strings.Split(line, " -> ")
		for i := 0; i < len(chunks)-1; i++ {
			start := parseTuple(chunks[i])
			end := parseTuple(chunks[i+1])
			maxY = aoc.Max(maxY, start.Y)
			maxY = aoc.Max(maxY, end.Y)
			for _, pos := range segment(start, end) {
				result[pos] = struct{}{}
			}
		}
	}
	return result, maxY
}

func dropSand(caves map[aoc.Tuple]struct{}, maxY int) bool {
	sand := aoc.Tuple{X: 500, Y: 0}
	blocked := false
	for !blocked {
		blocked = true
		for _, move := range moves {
			nextPos := sand.Add(move)
			_, occupied := caves[nextPos]
			if !occupied {
				if nextPos.Y >= maxY {
					return true
				}
				blocked = false
				sand = nextPos
				break
			}
		}
	}
	caves[sand] = struct{}{}
	return false
}

func calc(filename string) int {
	finished := false
	caves, maxY := readInput(filename)
	result := 0
	for !finished {
		finished = dropSand(caves, maxY)
		if !finished {
			result++
		}
	}
	return result
}

func dropSand2(caves map[aoc.Tuple]struct{}, maxY int) bool {
	sand := aoc.Tuple{X: 500, Y: 0}
	blocked := false
	for !blocked {
		blocked = true
		for _, move := range moves {
			nextPos := sand.Add(move)
			_, occupied := caves[nextPos]
			if nextPos.Y >= maxY+2 {
				occupied = true
			}
			if !occupied {
				blocked = false
				sand = nextPos
				break
			}
		}
	}
	caves[sand] = struct{}{}
	if sand.Y == 0 && sand.X == 500 {
		return true
	}
	return false
}

func calc2(filename string) int {
	finished := false
	caves, maxY := readInput(filename)
	result := 0
	for !finished {
		finished = dropSand2(caves, maxY)
		result++
	}
	return result
}

func main() {
	aoc.CheckEquals(24, calc("test.txt"))
	aoc.CheckEquals(93, calc2("test.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
