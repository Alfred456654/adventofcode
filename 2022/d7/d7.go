package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

type node struct {
	parent   *node
	name     string
	children []*node
	size     int
}

func newDir(subDirName string, parent *node) *node {
	return &node{
		parent:   parent,
		name:     subDirName,
		children: make([]*node, 0),
	}
}

func newFile(fileName string, size int, parent *node) *node {
	return &node{
		parent: parent,
		name:   fileName,
		size:   size,
	}
}

func (n *node) getSubDir(subDirName string) *node {
	for _, subDir := range n.children {
		if subDir.name == subDirName {
			return subDir
		}
	}
	subDir := newDir(subDirName, n)
	n.children = append(n.children, subDir)
	return subDir
}

func (n *node) computeSize() int {
	if n.size != 0 {
		return n.size
	}
	s := 0
	for _, child := range n.children {
		s += child.computeSize()
	}
	n.size = s
	return s
}

func readInput(filename string) (int, []*node) {
	var fsRoot, curDir *node
	allDirs := make([]*node, 0)
	for _, line := range aoc.ReadInputLines(filename) {
		chunks := strings.Split(line, " ")
		if chunks[0] == "$" {
			if chunks[1] == "cd" {
				switch chunks[2] {
				case "/":
					if fsRoot == nil {
						fsRoot = newDir("/", nil)
						allDirs = append(allDirs, fsRoot)
					}
					curDir = fsRoot
				case "..":
					curDir = curDir.parent
				default:
					curDir = curDir.getSubDir(chunks[2])
				}
			}
		} else {
			if chunks[0] == "dir" {
				newDirectory := newDir(chunks[1], curDir)
				allDirs = append(allDirs, newDirectory)
				curDir.children = append(curDir.children, newDirectory)
			} else {
				size, _ := strconv.Atoi(chunks[0])
				curDir.children = append(curDir.children, newFile(chunks[1], size, curDir))
			}
		}
	}
	return fsRoot.computeSize(), allDirs
}

func calc(filename string) (int, int) {
	rootSize, allDirs := readInput(filename)
	allSizes := make([]int, len(allDirs))
	r1 := 0
	needToRemove := rootSize - 40000000
	r2 := rootSize
	for idx, dir := range allDirs {
		size := dir.size
		if size >= needToRemove && r2 > size {
			r2 = size
		}
		allSizes[idx] = size
		if size < 100000 {
			r1 += size
		}
	}
	return r1, r2
}

func main() {
	t1, t2 := calc("test.txt")
	aoc.CheckEquals(95437, t1)
	aoc.CheckEquals(24933642, t2)
	fmt.Println(calc("input.txt"))
}
