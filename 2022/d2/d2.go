package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strings"
)

func readInput(filename string) [][]string {
	stratLines := aoc.ReadInputLines(filename)
	result := make([][]string, len(stratLines))
	for idx, line := range stratLines {
		chunks := strings.Split(line, " ")
		result[idx] = []string{chunks[0], chunks[1]}
	}
	return result
}

func score(sa, sb string) int {
	a := int(sa[0]) - 64
	b := int(sb[0]) - 87

	outcome := 6
	if a == b {
		outcome = 3
	} else if b+1 == a || (a == 1 && b == 3) {
		outcome = 0
	}
	return b + outcome
}

func score2(sa, sb string) int {
	a := int(sa[0]) - 64
	outcome := int(sb[0]) - 89
	b := a + outcome
	if b < 1 {
		b += 3
	}
	if b > 3 {
		b -= 3
	}
	outcome = (outcome + 1) * 3
	return b + outcome
}

func calc(filename string) int {
	result := 0
	for _, g := range readInput(filename) {
		result += score(g[0], g[1])
	}
	return result
}

func calc2(filename string) int {
	result := 0
	for _, g := range readInput(filename) {
		result += score2(g[0], g[1])
	}
	return result
}

func main() {
	aoc.CheckEquals(15, calc("test.txt"))
	aoc.CheckEquals(12, calc2("test.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
