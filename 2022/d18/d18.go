package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

type particle int

const (
	_ particle = iota
	lava
	water
)

var (
	sixDirs = []cube{
		{x: -1, y: 0, z: 0},
		{x: 1, y: 0, z: 0},
		{x: 0, y: -1, z: 0},
		{x: 0, y: 1, z: 0},
		{x: 0, y: 0, z: -1},
		{x: 0, y: 0, z: 1},
	}
)

type boundaries struct {
	x0 int
	y0 int
	z0 int
	x1 int
	y1 int
	z1 int
}

type cube struct {
	x int
	y int
	z int
}

func newBoundaries(cubes map[cube]particle) *boundaries {
	first := true
	var b *boundaries
	for c := range cubes {
		if first {
			first = false
			b = &boundaries{x0: c.x, y0: c.y, z0: c.z, x1: c.x, y1: c.y, z1: c.z}
		} else {
			b.x0 = aoc.Min(b.x0, c.x)
			b.y0 = aoc.Min(b.y0, c.y)
			b.z0 = aoc.Min(b.z0, c.z)
			b.x1 = aoc.Max(b.x1, c.x)
			b.y1 = aoc.Max(b.y1, c.y)
			b.z1 = aoc.Max(b.z1, c.z)
		}
	}
	b.x0--
	b.y0--
	b.z0--
	b.x1++
	b.y1++
	b.z1++
	return b
}

func (c cube) in(b *boundaries) bool {
	return b.x0 <= c.x && c.x <= b.x1 && b.y0 <= c.y && c.y <= b.y1 && b.z0 <= c.z && c.z <= b.z1
}

func (c cube) add(c2 cube) cube {
	return cube{x: c.x + c2.x, y: c.y + c2.y, z: c.z + c2.z}
}

func (c cube) sub(c2 cube) cube {
	return cube{x: c.x - c2.x, y: c.y - c2.y, z: c.z - c2.z}
}

func (c cube) abs() cube {
	return cube{x: aoc.Abs(c.x), y: aoc.Abs(c.y), z: aoc.Abs(c.z)}
}

func (c cube) norm1() int {
	abs := c.abs()
	return abs.x + abs.y + abs.z
}

func (c cube) touches(c2 cube) bool {
	return c.sub(c2).norm1() == 1
}

func readInput(filename string) []cube {
	lines := aoc.ReadInputLines(filename)
	scan := make([]cube, len(lines))
	for l, line := range lines {
		chunks := strings.Split(line, ",")
		x, _ := strconv.Atoi(chunks[0])
		y, _ := strconv.Atoi(chunks[1])
		z, _ := strconv.Atoi(chunks[2])
		scan[l] = cube{x: x, y: y, z: z}
	}
	return scan
}

func readInputAsMap(filename string) map[cube]particle {
	lines := aoc.ReadInputLines(filename)
	scan := make(map[cube]particle)
	for _, line := range lines {
		chunks := strings.Split(line, ",")
		x, _ := strconv.Atoi(chunks[0])
		y, _ := strconv.Atoi(chunks[1])
		z, _ := strconv.Atoi(chunks[2])
		scan[cube{x: x, y: y, z: z}] = lava
	}
	return scan
}

func calc(filename string) int {
	cubes := readInput(filename)
	result := 0
	for i, c := range cubes {
		result += 6
		for _, c2 := range cubes[i+1:] {
			if c.touches(c2) {
				result -= 2
			}
		}
	}
	return result
}

func contains(listOfCubes []cube, c cube) bool {
	for _, c2 := range listOfCubes {
		if c == c2 {
			return true
		}
	}
	return false
}

func findNeighbours(c cube, stream map[cube]particle, curToVisit []cube, bounds *boundaries) ([]cube, int) {
	toVisit := make([]cube, 0)
	score := 0
	for _, dir := range sixDirs {
		neigh := c.add(dir)
		if !neigh.in(bounds) {
			continue
		}
		partType, visited := stream[neigh]
		if partType == lava {
			score++
		}
		if !visited && !contains(curToVisit, neigh) {
			toVisit = append(toVisit, neigh)
		}
	}
	return toVisit, score
}

func calc2(filename string) int {
	cubes := readInputAsMap(filename)
	bounds := newBoundaries(cubes)
	c0 := cube{x: bounds.x0, y: bounds.y0, z: bounds.z0}
	cubes[c0] = water
	toVisit, result := findNeighbours(c0, cubes, []cube{}, bounds)
	for len(toVisit) > 0 {
		curCube := toVisit[0]
		toVisit = toVisit[1:]
		cubes[curCube] = water
		newToVisit, score := findNeighbours(curCube, cubes, toVisit, bounds)
		toVisit = append(toVisit, newToVisit...)
		result += score
	}

	return result
}

func main() {
	aoc.CheckEquals(64, calc("test.txt"))
	aoc.CheckEquals(58, calc2("test.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
