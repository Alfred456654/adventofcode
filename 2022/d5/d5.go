package main

import (
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var movesRe = regexp.MustCompile(`^move (\d+) from (\d+) to (\d+)$`)

func doMove(stacks [][]string, nb, from, to int, isPart2 bool) [][]string {
	if isPart2 {
		popped := make([]string, nb)
		copy(popped, stacks[from][:nb])
		stacks[from] = stacks[from][nb:]
		stacks[to] = append(popped, stacks[to]...)
	} else {
		for n := 0; n < nb; n++ {
			popped := stacks[from][0]
			stacks[from] = stacks[from][1:]
			stacks[to] = append([]string{popped}, stacks[to]...)
		}
	}
	return stacks
}

func parseCrates(nbStacks int, line string, stacks [][]string) [][]string {
	for idx := 0; idx < nbStacks; idx++ {
		crateName := string(line[4*idx+1])
		if crateName != " " {
			if stacks[idx] == nil {
				stacks[idx] = make([]string, 0)
			}
			stacks[idx] = append(stacks[idx], crateName)
		}
	}
	return stacks
}

func parseMove(line string, stacks [][]string, isPart2 bool) [][]string {
	matches := movesRe.FindAllStringSubmatch(line, -1)
	for _, m1 := range matches {
		nb, _ := strconv.Atoi(m1[1])
		from, _ := strconv.Atoi(m1[2])
		to, _ := strconv.Atoi(m1[3])
		from--
		to--
		stacks = doMove(stacks, nb, from, to, isPart2)
	}
	return stacks
}

func calc(filename string, isPart2 bool) string {
	fileBytes, _ := os.ReadFile(filename)
	fileAsString := string(fileBytes)
	lines := strings.Split(fileAsString, "\n")
	section := 0
	var stacks [][]string
	nbStacks := 0
	for _, line := range lines {
		if line == "" {
			section = 1
			continue
		}
		switch section {
		case 0:
			if nbStacks == 0 {
				nbStacks = (len(line) + 1) / 4
				stacks = make([][]string, nbStacks)
			}
			stacks = parseCrates(nbStacks, line, stacks)
		case 1:
			stacks = parseMove(line, stacks, isPart2)
		}
	}
	result := strings.Builder{}
	for _, stack := range stacks {
		result.WriteString(stack[0])
	}
	return result.String()
}

func main() {
	if t1 := calc("test.txt", false); t1 != "CMZ" {
		panic(fmt.Errorf("expected 'CMZ', got '%s'", t1))
	}
	if t2 := calc("test.txt", true); t2 != "MCD" {
		panic(fmt.Errorf("expected 'MCD', got '%s'", t2))
	}
	fmt.Println(calc("input.txt", false))
	fmt.Println(calc("input.txt", true))
}
