package main

import (
	"fmt"

	"gitlab.com/Alfred456654/aoc-utils"
)

var (
	start    = int32("S"[0])
	end      = int32("E"[0])
	lowest   = int32("a"[0])
	fourDirs = []aoc.Tuple{
		{X: 0, Y: 1},
		{X: 0, Y: -1},
		{X: -1, Y: 0},
		{X: 1, Y: 0},
	}
)

type currentState struct {
	loc   aoc.Tuple
	depth int
}

func readInput(filename string) (map[aoc.Tuple]int, aoc.Tuple, aoc.Tuple) {
	lines := aoc.ReadInputLines(filename)
	terrain := make(map[aoc.Tuple]int)
	var locStart, locEnd aoc.Tuple
	for i, line := range lines {
		for j, height := range line {
			switch height {
			case start:
				locStart = aoc.Tuple{X: j, Y: i}
				terrain[locStart] = 0
			case end:
				locEnd = aoc.Tuple{X: j, Y: i}
				terrain[locEnd] = 25
			default:
				terrain[aoc.Tuple{X: j, Y: i}] = int(height - lowest)
			}
		}
	}
	return terrain, locStart, locEnd
}

func findNeighbours(terrain map[aoc.Tuple]int, curLoc currentState, visited map[aoc.Tuple]struct{}, mapToVisit map[aoc.Tuple]struct{}) []currentState {
	result := make([]currentState, 0)
	for _, d := range fourDirs {
		nextLoc := curLoc.loc.Add(d)
		if nextHeight, exists := terrain[nextLoc]; exists {
			if nextHeight-terrain[curLoc.loc] <= 1 {
				if _, already := visited[nextLoc]; !already {
					if _, planned := mapToVisit[nextLoc]; !planned {
						result = append(result, currentState{
							loc:   nextLoc,
							depth: curLoc.depth + 1,
						})
						mapToVisit[nextLoc] = struct{}{}
					}
				}
			}
		}
	}
	return result
}

func findPath(locStart aoc.Tuple, terrain map[aoc.Tuple]int, locEnd aoc.Tuple) int {
	curLoc := currentState{
		loc:   locStart,
		depth: 0,
	}
	visited := map[aoc.Tuple]struct{}{locStart: {}}
	mapToVisit := make(map[aoc.Tuple]struct{})
	toVisit := findNeighbours(terrain, curLoc, visited, mapToVisit)
	for curLoc.loc != locEnd {
		if len(toVisit) == 0 {
			return len(terrain)
		}
		curLoc = toVisit[0]
		visited[curLoc.loc] = struct{}{}
		toVisit = toVisit[1:]
		toVisit = append(toVisit, findNeighbours(terrain, curLoc, visited, mapToVisit)...)
	}
	return curLoc.depth
}

func calc(filename string) (int, int) {
	terrain, locStart, locEnd := readInput(filename)
	r1 := findPath(locStart, terrain, locEnd)
	r2 := r1
	for loc, height := range terrain {
		if height == 0 {
			r2 = aoc.Min(r2, findPath(loc, terrain, locEnd))
		}
	}
	return r1, r2
}

func main() {
	t1, t2 := calc("test.txt")
	aoc.CheckEquals(31, t1)
	aoc.CheckEquals(29, t2)
	fmt.Println(calc("input.txt"))
}
