package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/Alfred456654/aoc-utils"
)

var (
	shifts = map[string]aoc.Tuple{
		"U": {X: 0, Y: -1},
		"D": {X: 0, Y: 1},
		"R": {X: 1, Y: 0},
		"L": {X: -1, Y: 0},
	}
)

func doMove(move string, knots []aoc.Tuple, visited map[aoc.Tuple]struct{}) ([]aoc.Tuple, map[aoc.Tuple]struct{}) {
	chunks := strings.Split(move, " ")
	nb, _ := strconv.Atoi(chunks[1])
	nbKnots := len(knots)
	for n := 0; n < nb; n++ {
		knots[0] = knots[0].Add(shifts[chunks[0]])
		for i := 1; i < nbKnots; i++ {
			knots[i] = follow(knots[i-1], knots[i])
		}
		visited[knots[nbKnots-1]] = struct{}{}
	}
	return knots, visited
}

func follow(head, tail aoc.Tuple) aoc.Tuple {
	vec := head.Sub(tail)
	shift := aoc.Tuple{}
	if aoc.Abs(vec.X*vec.Y) > 1 {
		shift.X = vec.X / aoc.Abs(vec.X)
		shift.Y = vec.Y / aoc.Abs(vec.Y)
	} else {
		if aoc.Abs(vec.X) > 1 {
			shift.X = vec.X / aoc.Abs(vec.X)
		}
		if aoc.Abs(vec.Y) > 1 {
			shift.Y = vec.Y / aoc.Abs(vec.Y)
		}
	}
	tail = tail.Add(shift)
	return tail
}

func calc(filename string, nbKnots int) int {
	moves := aoc.ReadInputLines(filename)
	knots := make([]aoc.Tuple, nbKnots)
	visited := make(map[aoc.Tuple]struct{})
	visited[knots[nbKnots-1]] = struct{}{}
	for _, m := range moves {
		knots, visited = doMove(m, knots, visited)
	}
	return len(visited)
}

func main() {
	aoc.CheckEquals(13, calc("test.txt", 2))
	aoc.CheckEquals(1, calc("test.txt", 10))
	aoc.CheckEquals(36, calc("test2.txt", 10))
	fmt.Println(calc("input.txt", 2))
	fmt.Println(calc("input.txt", 10))
}
