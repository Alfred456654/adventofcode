#!/bin/bash

set -x
CAP=100
cat input | while read line; do
    ING=$(echo "$line" | cut -d\: -f1)
    REM=$(echo "$line" | cut -d\  -f2-)
    DICT=$(echo "$REM" | sed -e 's/\([a-z]*\) \([0-9\-]\)*,\? \?/[\\"\1\\"]=\2, /g')
    eval "$ING=( $DICT )"
    echo $Sugar
done

#for i in `seq 0 100`; do
#    if [ $i -ne 100 ]; then
#        for j in `seq 0 100`; do
#            for k in `seq 0 100`; do
#                echo -n ""
#            done
#        done
#    else
#        echo -n ""
#    fi
#done
