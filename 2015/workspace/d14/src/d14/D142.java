package d14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This one doesn't actually work for real. Sometimes the final score is 1 less
 * than expected.
 */
public class D142 {

	private static final Pattern P = Pattern.compile("^([^ ]+)[^0-9]*(\\d+)[^0-9]*(\\d+)[^0-9]*(\\d+).*");

	private static final Integer RACE_DURATION = 2503;

	private static List<Reindeer> racers = new ArrayList<Reindeer>();

	public static void main(String[] args) {
		BufferedReader b = null;
		try {
			File f = new File("input");
			b = new BufferedReader(new FileReader(f));
			String line = null;
			while ((line = b.readLine()) != null) {
				Matcher m = P.matcher(line);
				if (m.matches()) {
					String name = m.group(1);
					Integer speed = Integer.valueOf(m.group(2));
					Integer sprintDuration = Integer.valueOf(m.group(3));
					Integer restDuration = Integer.valueOf(m.group(4));
					racers.add(new Reindeer(name, speed, sprintDuration, restDuration));
				} else {
					System.err.println("ERROR: " + line);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				b.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		Map<Reindeer, Integer> distances = new HashMap<Reindeer, Integer>();
		Map<Reindeer, Integer> scores = new HashMap<Reindeer, Integer>();

		for (Reindeer r : racers) {
			distances.put(r, distanceAfterSec(r, RACE_DURATION));
			scores.put(r, 0);
		}

		for (int t = 1; t <= RACE_DURATION; t++) {
			System.out.println("After " + t + " seconds:");
			Map<Reindeer, Integer> situation = new HashMap<Reindeer, Integer>();
			for (Reindeer r : racers) {
				Integer dist = distanceAfterSec(r, t);
				situation.put(r, dist);
				System.out.println("    " + r + " is at " + dist + " (score: " + scores.get(r) + ")");
			}

			Integer leadingDistance = max(situation.values());
			System.out.println("  ## After " + t + " seconds the leading distance is " + leadingDistance);
			for (Reindeer r : racers) {
				Integer dist = situation.get(r);
				if (dist == leadingDistance) {
					Integer previousScore = scores.get(r);
					System.out.println("    " + r + " scored:");
					System.out.println("        previous score: " + previousScore);
					scores.put(r, 1 + previousScore);
					System.out.println("        new score:      " + scores.get(r));
				}
			}
		}

		System.out.println(max(scores.values()));
	}

	private static Integer distanceAfterSec(Reindeer r, Integer seconds) {
		Integer cycleDuration = r.getRestDuration() + r.getSprintDuration();
		Integer nbCycles = seconds / cycleDuration;
		Integer distance = nbCycles * r.getSpeed() * r.getSprintDuration();

		Integer remainingTime = seconds - nbCycles * cycleDuration;
		distance += Math.min(remainingTime, r.getSprintDuration()) * r.getSpeed();
		return distance;
	}

	private static Integer max(Collection<Integer> distances) {
		Integer max = 0;
		for (Integer d : distances) {
			if (d > max)
				max = d;
		}
		return max;
	}
}
