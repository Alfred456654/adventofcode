package d9;

public class Trajet {
	private String from;
	private String to;
	private Integer length;

	public Trajet(String from, String to, int length) {
		this.from = from;
		this.to = to;
		this.length = length;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}
	
	@Override
	public String toString() {
		return "Etape de " + from + " vers " + to + " (" + length + ")";
	}
}
