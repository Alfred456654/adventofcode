// does not work
package d12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class D12Bis {

	public static void main(String[] args) {
		try {
			ScriptEngineManager mgr = new ScriptEngineManager();
			ScriptEngine engine = mgr.getEngineByName("JavaScript");
			String input = "";
			File f = new File("input");
			BufferedReader b = new BufferedReader(new FileReader(f));
			String line = null;
			while ((line = b.readLine()) != null) {
				input += line;
			}
			b.close();

			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			JsonParser jp = new JsonParser();
			JsonElement je = jp.parse(input);
			String prettyJsonString = gson.toJson(je);

			// String[] array = prettyJsonString.split("[\\[{]");
			String[] array = input.replaceAll("\\{", "#").split("#");

			List<String> filtered = filterRed(array);
			String result = join(filtered);
			JsonElement je2 = jp.parse(result);
			System.out.println(gson.toJson(je2));
			result = result.replaceAll("[^-0-9]+", "+");
			result = result.replaceAll("^\\+", "");
			result = result.replaceAll("\\+$", "");
			result = result.replaceAll("\\+-", "-");
			System.out.println(engine.eval(result));
		} catch (IOException | ScriptException e) {
			e.printStackTrace();
		}
	}

	private static List<String> filterRed(String[] array) {
		List<String> result = new ArrayList<String>();
		int red = 0;
		boolean deleteItem = false;
		for (String s : array) {
			if (s.contains(":\"red\"")) {
				if (!deleteItem) {
					red = count(s, '{');
					System.err.println("BEGIN (" + red + ")");
				}
				deleteItem = true;
			}
			if (deleteItem) {
				red += count(s, '{');
				red -= count(s, '}');
				if (red <= 0) {
					red = 0;
					deleteItem = false;
					System.err.println("END (" + red + ")");
				}
			}
			if (!deleteItem) {
				System.out.println(" ## " + s + " ## ");
				result.add(s);
			} else {
				System.err.println(" ## " + s + " ## ");
			}
		}
		return result;
	}

	private static String join(List<String> array) {
		String result = "";
		for (String s : array) {
			result += s;
		}
		return result;
	}

	private static int count(String s, char C) {
		int result = 0;
		for (char c : s.toCharArray()) {
			if (c == C)
				result++;
		}
		return result;
	}
}
