// does not work
package d12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class D12Quad {

	private static final Pattern WHITESPACE_BEGIN = Pattern.compile("^(\\s*).*$");

	public static void main(String[] args) {
		BufferedReader b = null;
		try {
			File f = new File("json.json");
			b = new BufferedReader(new FileReader(f));
			String line = null;
			boolean count = true;
			int total = 0;
			int lastGoodIndent = 0;
			int lineNb = 0;
			while ((line = b.readLine()) != null) {
				lineNb++;
				int nbIndent = getNbIndent(line);
				if (line.contains("red")) {
					count = false;
				}
				if (count) {
					System.out.print(lineNb + ": OK; nbIndent = " + nbIndent + "; total: " + total);
					total += countLine(line);
					lastGoodIndent = nbIndent;
					System.out.println(" --> " + total);
				} else {
					if (nbIndent <= lastGoodIndent) {
						count = true;
					}
					System.out.println("");
				}
			}
			System.out.println(total);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				b.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private static int countLine(String line) {
		String addition = line.replaceAll("[^0-9\\-]+", "+");
		String[] numbers = addition.split("\\+");
		int result = 0;
		for (String n : numbers) {
			if (n.length() > 0)
				result += Integer.parseInt(n);
		}
		return result;
	}

	private static int getNbIndent(String line) {
		Matcher m = WHITESPACE_BEGIN.matcher(line);
		if (m.matches()) {
			return m.group(1).length() / 4;
		}
		return 0;
	}

}
