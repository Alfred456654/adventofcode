// does not work
package d12;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class D12Ter {

	public static void main(String[] args) {
		BufferedReader b = null;
		try {
			String input = "";
			File f = new File("input");
			b = new BufferedReader(new FileReader(f));
			String line = null;
			while ((line = b.readLine()) != null) {
				input += line;
			}
			input = input.replaceAll(" ", "").replaceAll("\\{[^}{]*:\"red\"[^}{]*\\}", "");
			System.out.println(input);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				b.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
