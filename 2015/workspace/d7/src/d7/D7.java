package d7;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class D7 {

	private static Map<String, Integer> vals = new HashMap<String, Integer>();
	private static List<String> unknown = new LinkedList<String>();

	private static final Pattern NB = Pattern.compile("[0-9]+");
	private static final Pattern E2 = Pattern.compile("([a-z0-9]+) -> ([a-z]+)");
	private static final Pattern E3 = Pattern.compile("NOT ([a-z]+) -> ([a-z]+)");
	private static final Pattern E4 = Pattern.compile("([0-9a-z]+) (AND|OR|LSHIFT|RSHIFT) ([0-9a-z]+) -> ([a-z]+)");

	private static final List<Pattern> E = new LinkedList<Pattern>() {
		private static final long serialVersionUID = -1330426870676539246L;

		{
			add(E2);
			add(E3);
			add(E4);
		}
	};

	public static void main(String[] args) {
		while (unknown.size() != 0 || vals.size() == 0) {
			try {
				//File f = new File("input");
				File f = new File("input2");
				BufferedReader b = new BufferedReader(new FileReader(f));
				String line = null;
				while ((line = b.readLine()) != null) {
					Pattern matching = null;
					Matcher m = null;
					for (Pattern p : E) {
						m = p.matcher(line);
						if (m.matches()) {
							matching = p;
						}
					}
					if (matching == null) {
						System.err.println(line);
					} else {
						m = matching.matcher(line);
						if (E2.equals(matching) || E3.equals(matching)) {
							m.matches();
							String left = m.group(1);
							String right = m.group(2);

							if (!vals.containsKey(right) && !unknown.contains(right)) {
								unknown.add(right);
							}

							if (NB.matcher(left).matches()) {
								if (!vals.containsKey(right)) {
									int newVal = Integer.parseInt(left);
									if (E3.equals(matching))
										newVal = ~newVal;
									vals.put(right, newVal);
									if (unknown.contains(right)) {
										unknown.remove(right);
									}
									System.out.println(right + " = " + newVal);
								}
							} else {
								if (!vals.containsKey(left) && !unknown.contains(left)) {
									unknown.add(left);
								}
								if (vals.containsKey(left) && !vals.containsKey(right)) {
									int newVal = vals.get(left);
									if (E3.equals(matching))
										newVal = ~newVal;
									vals.put(right, newVal);
									if (unknown.contains(right)) {
										unknown.remove(right);
									}
									System.out.println(right + " = " + newVal);
								}
							}
						} else {
							m.matches();
							String l1 = m.group(1);
							String l2 = m.group(3);
							String op = m.group(2);
							String right = m.group(4);

							if (!vals.containsKey(right) && !unknown.contains(right)) {
								unknown.add(right);
							}

							Integer v1 = null;
							if (NB.matcher(l1).matches()) {
								v1 = Integer.parseInt(l1);
							} else {
								if (!vals.containsKey(l1) && !unknown.contains(l1)) {
									unknown.add(l1);
								}
							}
							if (vals.containsKey(l1)) {
								int v1test = vals.get(l1);
								if (v1 != null && v1 != v1test) {
									System.err.println("Had stored " + v1test + " for " + l1 + " but just read " + v1
											+ " on line " + line);
								}
								v1 = v1test;
							}

							Integer v2 = null;
							if (NB.matcher(l2).matches()) {
								v2 = Integer.parseInt(l2);
							} else {
								if (!vals.containsKey(l2) && !unknown.contains(l2)) {
									unknown.add(l2);
								}
							}
							if (vals.containsKey(l2)) {
								int v2test = vals.get(l2);
								if (v2 != null && v2 != v2test) {
									System.err.println("Had stored " + v2test + " for " + l2 + " but just read " + v2
											+ " on line " + line);
								}
								v2 = v2test;
							}

							if (v1 != null && v2 != null && !vals.containsKey(right)) {
								int newVal = -1;
								switch (op) {
								case "AND":
									newVal = v1 & v2;
									break;
								case "OR":
									newVal = v1 | v2;
									break;
								case "LSHIFT":
									newVal = v1 << v2;
									break;
								case "RSHIFT":
									newVal = v1 >> v2;
									break;
								default:
									break;
								}
								vals.put(right, newVal);
								System.out.println(right + " = " + newVal);
								if (unknown.contains(right)) {
									unknown.remove(right);
								}
							}
						}
					}
				}
				b.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
