package d16;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class D16_2 {

	private static Map<Integer, Map<String, Integer>> aunts = new HashMap<Integer, Map<String, Integer>>();

	private static Map<String, Integer> gift = new HashMap<String, Integer>();

	private static final Pattern P = Pattern.compile("^Sue (\\d+): (\\w+): (\\d+), (\\w+): (\\d+), (\\w+): (\\d+)$");

	private static final Pattern P2 = Pattern.compile("^(\\w+): (\\d+)$");

	public static void main(String[] args) {
		BufferedReader b = null;
		try {

			// input
			File f = new File("input");
			b = new BufferedReader(new FileReader(f));
			String line = null;
			while ((line = b.readLine()) != null) {
				Matcher m = P.matcher(line);
				if (m.matches()) {
					Integer sueNb = Integer.valueOf(m.group(1));
					Map<String, Integer> sueHas = new HashMap<String, Integer>();
					for (int i = 1; i <= 3; i++) {
						String item = m.group(2 * i);
						Integer amount = Integer.valueOf(m.group(2 * i + 1));
						sueHas.put(item, amount);
					}
					aunts.put(sueNb, sueHas);
				} else {
					System.err.println("ERROR: '" + line + "' does not match the expected pattern");
				}
			}
			b.close();

			// sue
			f = new File("sue");
			b = new BufferedReader(new FileReader(f));
			line = null;
			while ((line = b.readLine()) != null) {
				Matcher m = P2.matcher(line);
				if (m.matches()) {
					String item = m.group(1);
					Integer amount = Integer.valueOf(m.group(2));
					gift.put(item, amount);
				} else {
					System.err.println("ERROR: '" + line + "' does not match the expected pattern");
				}
			}
			b.close();

			// search
			for (Integer sue : aunts.keySet()) {
				boolean ok = true;
				Map<String, Integer> sueHas = aunts.get(sue);
				for (String item : sueHas.keySet()) {
					Integer curAmount = sueHas.get(item);
					Integer expectedAmount = gift.get(item);
					if (itemDoesNotMatch(item, curAmount, expectedAmount)) {
						ok = false;
					}
				}
				if (ok) {
					System.out.println(sue);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * In particular, the cats and trees readings indicates that there are
	 * greater than that many, while the pomeranians and goldfish readings
	 * indicate that there are fewer than that many (due to the modial
	 * interaction of magnetoreluctance).
	 */
	private static boolean itemDoesNotMatch(String item, Integer curAmount, Integer expectedAmount) {
		if ("cats".equals(item) || "trees".equals(item)) {
			return curAmount <= expectedAmount;
		} else if ("pomeranians".equals(item) || "goldfish".equals(item)) {
			return curAmount >= expectedAmount;
		} else {
			return curAmount != expectedAmount;
		}
	}
}
