package d13;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class D132 {

	private static final Pattern P = Pattern
			.compile("^([A-Z][a-z]+) would (gain|lose) ([0-9]+) happiness units by sitting next to ([A-Z][a-z]+).$");

	private static Map<String, Map<String, Integer>> relationships = new HashMap<String, Map<String, Integer>>();

	public static void main(String[] args) {
		BufferedReader b = null;
		try {
			File f = new File("input");
			b = new BufferedReader(new FileReader(f));
			String line = null;
			while ((line = b.readLine()) != null) {
				Matcher m = P.matcher(line);
				if (m.matches()) {
					for (int i = 1; i <= m.groupCount(); i++) {
						String p1 = m.group(1);
						String p2 = m.group(4);
						Integer happDelta = Integer.valueOf(m.group(3));
						if ("lose".equals(m.group(2))) {
							happDelta = -happDelta;
						}
						Map<String, Integer> p1srel = relationships.get(p1);
						if (p1srel == null) {
							p1srel = new HashMap<String, Integer>();
						}
						p1srel.put(p2, happDelta);
						relationships.put(p1, p1srel);
					}
				} else {
					System.err.println(line + " doesn't match");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				b.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		List<String> people = new ArrayList<String>();
		for (String p : relationships.keySet()) {
			people.add(p);
		}

		// Add myself
		Map<String, Integer> myRels = new HashMap<String, Integer>();
		for (String p : people) {
			relationships.get(p).put("Alfred", 0);
			myRels.put(p, 0);
		}
		relationships.put("Alfred", myRels);
		people.add("Alfred");

		String firstPerson = people.remove(0);
		Sitting s = new Sitting(firstPerson, 0, people);
		Integer maxHappiness = maxHappy(firstPerson, s);
		System.out.println(maxHappiness);
	}

	private static Integer maxHappy(String firstPerson, Sitting s) {
		if (s.getRemaining().size() == 0) {
			return s.getHappiness() + happinessFor(s, firstPerson);
		} else {
			List<Integer> happies = new ArrayList<Integer>();
			for (String p : s.getRemaining()) {
				List<String> remainNext = new ArrayList<String>();
				remainNext.addAll(s.getRemaining());
				remainNext.remove(p);
				Sitting s2 = new Sitting(p, s.getHappiness() + happinessFor(s, p), remainNext);
				happies.add(maxHappy(firstPerson, s2));
			}
			return max(happies);
		}
	}

	private static void debug() {
		for (String p1 : relationships.keySet()) {
			System.out.println(p1);
			for (String p2 : relationships.get(p1).keySet()) {
				System.out.println("    " + String.format("%-10s", p2) + relationships.get(p1).get(p2));
			}
		}
	}

	private static Integer happinessFor(Sitting s, String name) {
		return relationships.get(s.getName()).get(name) + relationships.get(name).get(s.getName());
	}

	private static Integer max(List<Integer> happies) {
		Integer max = happies.get(0);
		for (Integer h : happies) {
			if (h > max)
				max = h;
		}
		return max;
	}
}
