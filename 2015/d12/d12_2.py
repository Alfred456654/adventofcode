import json

with open('json.json') as data_file:
    data = json.load(data_file)

def free_of_red(h):
    for n in h:
        if (u'' == n.__class__() or '' == n.__class__()):
            if n.count('red') > 0:
                return False
            item = h[n]
            if (u'' == item.__class__() or '' == item.__class__()):
                if item.count('red') > 0:
                    return False
    return True

def recurse(node, depth):
    result = 0
    for n in node:
        item = n
        if {} == node.__class__():
            item = node[n]
        if [] == item.__class__():
            result = result + recurse(item, depth + 1)
        elif {} == item.__class__():
            if free_of_red(item):
                result = result + recurse(item, depth + 1)
        elif (u'' == item.__class__() or '' == item.__class__()):
            pass
        elif 0 == item.__class__():
            result = result + item
        else:
            print "%d [%s]" % (depth, item.__class__())
    return result

print recurse(data, 0)
