import re
import Image

grid = [[0 for _ in xrange(1000)] for _ in xrange(1000)]

f = open("input", "r")
s = "lololol"
re_instr = re.compile(r'(turn on|turn off|toggle) ([0-9]+),([0-9]+) through ([0-9]+),([0-9]+)')
while (s != ''):
    s = f.readline()
    m = re_instr.match(s)
    if(m):
        instr = m.group(1)
        x1 = int(m.group(2))
        y1 = int(m.group(3))
        x2 = int(m.group(4))
        y2 = int(m.group(5))
        for i in range(min(x1, x2), max(x1, x2) + 1):
            for j in range(min(y1, y2), max(y1, y2) + 1):
                if (instr == "turn on"):
                    grid[i][j] = 1
                elif (instr == "turn off"):
                    grid[i][j] = 0
                elif (instr == "toggle"):
                    grid[i][j] = 1 - grid[i][j]
                else:
                    print("ERROR: unknown instruction: %s" % instr)

total = 0
for i in range(1000):
    total = total + grid[i].count(1)

print(total)

img = Image.new('RGB', (1000,1000), "black")
pixels = img.load()

for i in range(0, 1000):
    for j in range(0, 1000):
        if(grid[i][j] == 1):
            pixels[i, j] = (255, 255, 255)
img.show()
