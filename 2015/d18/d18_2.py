in_file = open('input')
first_it = in_file.readlines()
size_x = len(first_it)
size_y = len(first_it[0]) - 1

idx = 0
for l in first_it:
    first_it[idx] = list(l)
    idx = idx + 1

first_it[0][0] = "#"
first_it[size_x - 1][0] = "#"
first_it[0][size_y - 1] = "#"
first_it[size_x - 1][size_y - 1] = "#"

def getcur(it, k, l):
    if(k < 0 or k >= size_x or l < 0 or l >= size_y):
        return False
    else:
        return "#" == it[k][l]

def neighb(it, i, j):
    surround = 0
    for k in range(i - 1, i + 2):
        for l in range(j - 1, j + 2):
            if getcur(it, k, l) and (i != k or j != l):
                surround = surround + 1
    return surround

def print_it(it):
    print ""
    nb_lit = 0
    for r in it:
        for c in r:
            if c == "#":
                nb_lit = nb_lit + 1
            if c != '\n':
                print c,
        print ""
    print "%d lit" % nb_lit


def recurse(cur_it, n):
    next_it = [["." for x in range(size_x)] for y in range(size_y)]

    for i in range(size_x):
        for j in range(size_y):
            nb_neighb = neighb(cur_it, i, j)

            if cur_it[i][j] == ".":
                if nb_neighb == 3:
                    next_it[i][j] = "#"
                else:
                    next_it[i][j] = "."

            elif cur_it[i][j] == "#":
                if nb_neighb == 2 or nb_neighb == 3:
                    next_it[i][j] = "#"
                else:
                    next_it[i][j] = "."

            else:
                print "Error at %d,%d: %s" % (i, j, cur_it[i, j])

    next_it[0][0] = "#"
    next_it[size_x - 1][0] = "#"
    next_it[0][size_y - 1] = "#"
    next_it[size_x - 1][size_y - 1] = "#"
    print_it(next_it)
    if n > 1:
        recurse(next_it, n - 1)

print_it(first_it)
recurse(first_it, 100)
