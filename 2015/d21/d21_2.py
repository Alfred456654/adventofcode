from math import ceil

my_hp = 100

enn_hp = 104
enn_dmg = 8
enn_arm = 1

def nb_stands(hp, dmg, arm):
    if dmg > arm:
        return int(ceil(float(hp) / float(dmg - arm)))
    else:
        return hp

def read_list(filename):
    in_file = open(filename)
    out_list = []
    cur_item = None
    while cur_item != ['']:
        cur_item = in_file.readline().rstrip().split(';')
        if cur_item != ['']:
            out_list.append({'name': cur_item[0], 'cost': int(cur_item[1]), 'damage': int(cur_item[2]), 'armor': int(cur_item[3])})
    return out_list

weapons = read_list('weapons')
armors = read_list('armors')
armors.append({'name': "no armor", 'cost': 0, 'damage': 0, 'armor': 0})
rings = read_list('rings')
rings.append({'name': "no ring", 'cost': 0, 'damage': 0, 'armor': 0})

max_cost = 0
max_stuff = ""

for w in weapons:
    for a in armors:
        for r1 in rings:
            for r2 in rings:
                my_span = nb_stands(my_hp, enn_dmg, a['armor'] + r1['armor'] + r2['armor'])
                enn_span = nb_stands(enn_hp, w['damage'] + r1['damage'] + r2['damage'], enn_arm)
                cost = w['cost'] + a['cost'] + r1['cost'] + r2['cost']
                if my_span < enn_span and max_cost < cost:
                        max_stuff = "%s, %s, %s, %s" % (w['name'], a['name'], r1['name'], r2['name'])
                        max_cost = cost
print "%d: %s" % (max_cost, max_stuff)
