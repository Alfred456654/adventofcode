package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"regexp"
	"strconv"
	"strings"
)

var parenthesisBlock = regexp.MustCompile(`\(([^()]*)\)`)
var addition = regexp.MustCompile(`([0-9]+) \+ ([0-9]+)`)

func calcBlock(block string) int {
	explodeBlock := strings.Split(block, " ")
	result := 0
	currentOp := "+"
	for _, item := range explodeBlock {
		if item == "+" || item == "*" {
			currentOp = item
		} else {
			nb, _ := strconv.Atoi(item)
			switch currentOp {
			case "+":
				result += nb
			case "*":
				result *= nb
			}
		}
	}
	return result
}

func calcBlock2(block string) int {
	groups := addition.FindStringSubmatch(block)
	for len(groups) > 0 {
		a, _ := strconv.Atoi(groups[1])
		b, _ := strconv.Atoi(groups[2])
		additionResult := a + b
		block = strings.Replace(block, groups[0], fmt.Sprintf("%d", additionResult), 1)
		groups = addition.FindStringSubmatch(block)
	}
	result := calcBlock(block)
	return result
}

func calcLine(line string, isPart2 bool) int {
	groups := parenthesisBlock.FindSubmatchIndex([]byte(line))
	for len(groups) > 0 {
		middlePart := 0
		if isPart2 {
			middlePart = calcBlock2(line[groups[2]:groups[3]])
		} else {
			middlePart = calcBlock(line[groups[2]:groups[3]])
		}
		line = fmt.Sprintf("%s%d%s", line[:groups[0]], middlePart, line[groups[1]:])
		groups = parenthesisBlock.FindSubmatchIndex([]byte(line))
	}
	if isPart2 {
		return calcBlock2(line)
	}
	return calcBlock(line)
}

func calc(filename string) (int, int) {
	result := 0
	result2 := 0
	for _, line := range aoc.ReadInputLines(filename) {
		result += calcLine(line, false)
		result2 += calcLine(line, true)
	}
	return result, result2
}

func main() {
	t1, t2 := calc("test.txt")
	aoc.CheckEqualsMsg(71+51+26+437+12240+13632, t1, "test1")
	aoc.CheckEqualsMsg(231+51+46+1445+669060+23340, t2, "test2")
	r1, r2 := calc("input.txt")
	fmt.Printf("result 1: %d\n", r1)
	fmt.Printf("result 2: %d\n", r2)
}
