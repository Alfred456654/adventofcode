package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

const exp1 = 165
const exp2 = 208
const allOnes = (1 << 36) - 1

func calc(filename string) int {
	allLines := aoc.ReadInputLines(filename)
	memory := make(map[int]int)
	mask := ""
	for _, lineStr := range allLines {
		switch lineStr[:3] {
		case "mas":
			mask = strings.Trim(strings.Split(lineStr, "=")[1], " ")
		case "mem":
			addr, _ := strconv.Atoi(strings.Split(strings.Split(lineStr, "[")[1], "]")[0])
			val, _ := strconv.Atoi(strings.Trim(strings.Split(lineStr, "=")[1], " "))
			for i, m := range mask {
				switch m {
				case 48: // "0" -> set bit to 0
					val &= allOnes ^ (1 << (35 - i))
				case 49: // "1" -> set bit to 1
					val |= allOnes & (1 << (35 - i))
				}
			}
			memory[addr] = val
		}
	}
	sum := 0
	for _, v := range memory {
		sum += v
	}
	return sum
}

func modifMem(memoryPtr *map[int]int, memAddr int, valAtAddr int, mask string) {
	if strings.Contains(mask, "X") {
		modifMem(memoryPtr, memAddr, valAtAddr, strings.Replace(mask, "X", "2", 1)) // 2 is "set to 0"
		modifMem(memoryPtr, memAddr, valAtAddr, strings.Replace(mask, "X", "1", 1)) // 1 is still "set to 1"
	} else {
		for i, m := range mask {
			switch m {
			case 49: // "1" -> set bit to 1
				memAddr |= allOnes & (1 << (35 - i))
			case 50: // "2" -> set bit to 0
				memAddr &= allOnes ^ (1 << (35 - i))
			}
		}
		(*memoryPtr)[memAddr] = valAtAddr
	}
}

func calc2(filename string) int {
	allLines := aoc.ReadInputLines(filename)
	memory := make(map[int]int)
	memoryPtr := &memory
	mask := ""
	for _, line := range allLines {
		switch line[:3] {
		case "mas":
			mask = strings.Trim(strings.Split(line, "=")[1], " ")
		case "mem":
			memAddr, _ := strconv.Atoi(strings.Split(strings.Split(line, "[")[1], "]")[0])
			valAtAddr, _ := strconv.Atoi(strings.Trim(strings.Split(line, "=")[1], " "))
			modifMem(memoryPtr, memAddr, valAtAddr, mask)
		}
	}
	result := 0
	for _, v := range memory {
		result += v
	}
	return result
}

func main() {
	aoc.CheckEqualsMsg(exp1, calc("test.txt"), "test1")
	aoc.CheckEqualsMsg(exp2, calc2("test2.txt"), "test2")
	r1 := calc("input.txt")
	r2 := calc2("input.txt")
	fmt.Printf("Result1: %d\n", r1)
	fmt.Printf("Result2: %d\n", r2)
}
