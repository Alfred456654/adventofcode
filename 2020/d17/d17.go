package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

const (
	exp1 = 112
	exp2 = 848
)

type coord struct {
	x int
	y int
	z int
	w int
}

func readInput(filename string) (map[coord]int, int, int) {
	result := make(map[coord]int)
	layer := aoc.ReadInputLines(filename)
	maxX := len(layer[0]) - 1
	maxY := len(layer) - 1
	for y, line := range layer {
		for x, active := range line {
			// ascii code for # is 35
			if active == 35 {
				result[coord{x, y, 0, 0}] = 1
			} else {
				result[coord{x, y, 0, 0}] = 0
			}
		}
	}
	return result, maxX, maxY
}

func activeAround(grid map[coord]int, xyz coord) int {
	cellActive := grid[xyz] == 1
	activeNeighbours := 0
	for x := xyz.x - 1; x < xyz.x+2; x++ {
		for y := xyz.y - 1; y < xyz.y+2; y++ {
			for z := xyz.z - 1; z < xyz.z+2; z++ {
				if grid[coord{x, y, z, 0}] == 1 {
					activeNeighbours++
				}
			}
		}
	}
	if cellActive {
		activeNeighbours--
	}
	return activeNeighbours
}

func activeAround2(grid map[coord]int, xyzw coord) interface{} {
	cellActive := grid[xyzw] == 1
	activeNeighbours := 0
	for x := xyzw.x - 1; x < xyzw.x+2; x++ {
		for y := xyzw.y - 1; y < xyzw.y+2; y++ {
			for z := xyzw.z - 1; z < xyzw.z+2; z++ {
				for w := xyzw.w - 1; w < xyzw.w+2; w++ {
					if grid[coord{x, y, z, w}] == 1 {
						activeNeighbours++
					}
				}
			}
		}
	}
	if cellActive {
		activeNeighbours--
	}
	return activeNeighbours
}

func transform(grid map[coord]int, minX int, maxX int, minY int, maxY int, minZ int, maxZ int) (map[coord]int,
	int, int, int, int, int, int) {
	newMinX, newMaxX, newMinY, newMaxY, newMinZ, newMaxZ := 0, 0, 0, 0, 0, 0
	newGrid := make(map[coord]int)
	for x := minX - 1; x <= maxX+1; x++ {
		for y := minY - 1; y <= maxY+1; y++ {
			for z := minZ - 1; z <= maxZ+1; z++ {
				currentCell := coord{x, y, z, 0}
				activeNeighbours := activeAround(grid, currentCell)
				if grid[currentCell] == 1 {
					if activeNeighbours != 2 && activeNeighbours != 3 {
						newGrid[currentCell] = 0
					} else {
						newGrid[currentCell] = 1
						newMinZ = aoc.Min(z, newMinZ)
						newMaxZ = aoc.Max(z, newMaxZ)
						newMinX = aoc.Min(x, newMinX)
						newMaxX = aoc.Max(x, newMaxX)
						newMinY = aoc.Min(y, newMinY)
						newMaxY = aoc.Max(y, newMaxY)
					}
				} else if grid[currentCell] == 0 {
					if activeNeighbours == 3 {
						newGrid[currentCell] = 1
						newMinZ = aoc.Min(z, newMinZ)
						newMaxZ = aoc.Max(z, newMaxZ)
						newMinX = aoc.Min(x, newMinX)
						newMaxX = aoc.Max(x, newMaxX)
						newMinY = aoc.Min(y, newMinY)
						newMaxY = aoc.Max(y, newMaxY)
					} else {
						newGrid[currentCell] = 0
					}
				}
			}
		}
	}
	return newGrid, newMinX, newMaxX, newMinY, newMaxY, newMinZ, newMaxZ
}

func transform2(grid map[coord]int, minX int, maxX int, minY int, maxY int, minZ int, maxZ int, minW int, maxW int) (map[coord]int, int, int, int, int, int, int, int, int) {
	newMinX, newMaxX, newMinY, newMaxY, newMinZ, newMaxZ, newMinW, newMaxW := 0, 0, 0, 0, 0, 0, 0, 0
	newGrid := make(map[coord]int)
	for x := minX - 1; x <= maxX+1; x++ {
		for y := minY - 1; y <= maxY+1; y++ {
			for z := minZ - 1; z <= maxZ+1; z++ {
				for w := minW - 1; w <= maxW+1; w++ {
					currentCell := coord{x, y, z, w}
					activeNeighbours := activeAround2(grid, currentCell)
					if grid[currentCell] == 1 {
						if activeNeighbours != 2 && activeNeighbours != 3 {
							newGrid[currentCell] = 0
						} else {
							newGrid[currentCell] = 1
							newMinZ = aoc.Min(z, newMinZ)
							newMaxZ = aoc.Max(z, newMaxZ)
							newMinX = aoc.Min(x, newMinX)
							newMaxX = aoc.Max(x, newMaxX)
							newMinY = aoc.Min(y, newMinY)
							newMaxY = aoc.Max(y, newMaxY)
							newMinW = aoc.Min(w, newMinW)
							newMaxW = aoc.Max(w, newMaxW)
						}
					} else if grid[currentCell] == 0 {
						if activeNeighbours == 3 {
							newGrid[currentCell] = 1
							newMinZ = aoc.Min(z, newMinZ)
							newMaxZ = aoc.Max(z, newMaxZ)
							newMinX = aoc.Min(x, newMinX)
							newMaxX = aoc.Max(x, newMaxX)
							newMinY = aoc.Min(y, newMinY)
							newMaxY = aoc.Max(y, newMaxY)
							newMinW = aoc.Min(w, newMinW)
							newMaxW = aoc.Max(w, newMaxW)
						} else {
							newGrid[currentCell] = 0
						}
					}
				}
			}
		}
	}
	return newGrid, newMinX, newMaxX, newMinY, newMaxY, newMinZ, newMaxZ, newMinW, newMaxW

}

func countGrid(grid map[coord]int) int {
	result := 0
	for _, v := range grid {
		result += v
	}
	return result
}

func calc(filename string) int {
	grid, maxX, maxY := readInput(filename)
	minX, minY, minZ, maxZ := 0, 0, 0, 0
	for it := 0; it < 6; it++ {
		grid, minX, maxX, minY, maxY, minZ, maxZ = transform(grid, minX, maxX, minY, maxY, minZ, maxZ)
	}
	return countGrid(grid)
}

func calc2(filename string) int {
	grid, maxX, maxY := readInput(filename)
	minX, minY, minZ, maxZ, minW, maxW := 0, 0, 0, 0, 0, 0
	for it := 0; it < 6; it++ {
		grid, minX, maxX, minY, maxY, minZ, maxZ, minW, maxW = transform2(grid, minX, maxX, minY, maxY, minZ, maxZ, minW, maxW)
	}
	return countGrid(grid)
}

func main() {
	aoc.CheckEqualsMsg(exp1, calc("test.txt"), "test1")
	aoc.CheckEqualsMsg(exp2, calc2("test.txt"), "test2")
	r1 := calc("input.txt")
	r2 := calc2("input.txt")
	fmt.Printf("Result1: %d\n", r1)
	fmt.Printf("Result2: %d\n", r2)
}
