package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"math"
	"sort"
	"strings"
)

const exp11 = 35
const exp12 = 220
const exp21 = 8
const exp22 = 19208

func calc(filename string) int {
	joltages := aoc.ReadInputIntegers(filename)
	sort.Ints(joltages)
	diffs := make([]int, len(joltages))
	previousJoltage := 0
	j1 := 0
	j3 := 1
	for i, joltage := range joltages {
		diff := joltage - previousJoltage
		diffs[i] = diff
		switch diff {
		case 1:
			j1++
		case 3:
			j3++
		}
		previousJoltage = joltage
	}
	return j1 * j3
}

func calc2(filename string) int {
	joltages := aoc.ReadInputIntegers(filename)
	joltages = append(joltages, 0)
	sort.Ints(joltages)
	nbs := strings.Builder{}
	for i, j := range joltages {
		n := 0
		for _, jj := range joltages[i+1:] {
			if jj-j > 3 {
				break
			} else {
				n++
			}
		}
		nbs.WriteString(fmt.Sprintf("%d", n))
	}
	stuff := nbs.String()
	n7 := strings.Count(stuff, "332")
	n4 := strings.Count(stuff, "32") - n7
	n2 := strings.Count(stuff, "2") - n7 - n4
	return int(math.Pow(7, float64(n7)) * math.Pow(4, float64(n4)) * math.Pow(2, float64(n2)))
}

func main() {
	t11 := calc("test.txt")
	t12 := calc("test2.txt")
	t21 := calc2("test.txt")
	t22 := calc2("test2.txt")
	aoc.CheckEqualsMsg(exp11, t11, "test1_1")
	aoc.CheckEqualsMsg(exp12, t12, "test1_2")
	aoc.CheckEqualsMsg(exp21, t21, "test2_1")
	aoc.CheckEqualsMsg(exp22, t22, "test2_2")
	r1 := calc("input.txt")
	r2 := calc2("input.txt")
	fmt.Printf("result 1: %d\n", r1)
	fmt.Printf("result 2: %d\n", r2)
}
