package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
)

const exp1 = 25
const exp2 = 286

var cardinal = map[int][]int{
	0: {0, 1},
	1: {1, 0},
	2: {0, -1},
	3: {-1, 0},
}

type mov struct {
	dir string
	nb  int
}

func readInput(filename string) []*mov {
	instrs := aoc.ReadInputLines(filename)
	result := make([]*mov, len(instrs))
	for i, instr := range instrs {
		mDir := instr[0:1]
		mNb, _ := strconv.Atoi(instr[1:])
		result[i] = &mov{
			dir: mDir,
			nb:  mNb,
		}
	}
	return result
}

func calc(mvts []*mov) int {
	x, y := 0, 0
	cardId := 0
	for _, mvt := range mvts {
		switch mvt.dir {
		case "N":
			y -= mvt.nb
		case "S":
			y += mvt.nb
		case "E":
			x += mvt.nb
		case "W":
			x -= mvt.nb
		case "L":
			cardId -= mvt.nb / 90
			for cardId < 0 {
				cardId += 4
			}
		case "R":
			cardId += mvt.nb / 90
			for cardId > 3 {
				cardId -= 4
			}
		case "F":
			mvtYX := cardinal[cardId]
			y += mvt.nb * mvtYX[0]
			x += mvt.nb * mvtYX[1]
		}
	}
	return aoc.Abs(x) + aoc.Abs(y)
}

func calc2(mvts []*mov) int {
	x, y := 0, 0
	wx, wy := 10, -1
	for _, mvt := range mvts {
		switch mvt.dir {
		case "N":
			wy -= mvt.nb
		case "S":
			wy += mvt.nb
		case "E":
			wx += mvt.nb
		case "W":
			wx -= mvt.nb
		case "L":
			n := (mvt.nb / 90) % 4
			switch n {
			case 1:
				wx, wy = wy, -wx
			case 2:
				wx, wy = -wx, -wy
			case 3:
				wx, wy = -wy, wx
			}
		case "R":
			n := (mvt.nb / 90) % 4
			switch n {
			case 1:
				wx, wy = -wy, wx
			case 2:
				wx, wy = -wx, -wy
			case 3:
				wx, wy = wy, -wx
			}
		case "F":
			x += wx * mvt.nb
			y += wy * mvt.nb
		}
	}
	return aoc.Abs(x) + aoc.Abs(y)
}

func main() {
	testData := readInput("test.txt")
	t1 := calc(testData)
	t2 := calc2(testData)
	aoc.CheckEqualsMsg(exp1, t1, "test1")
	aoc.CheckEqualsMsg(exp2, t2, "test2")

	data := readInput("input.txt")
	r1 := calc(data)
	r2 := calc2(data)
	fmt.Printf("result 1: %d\n", r1)
	fmt.Printf("result 2: %d\n", r2)
}
