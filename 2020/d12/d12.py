MOV = {'E': (0, 1), 'S': (1, 0), 'W': (0, -1), 'N': (-1, 0)}


def read_input(filename):
    with open(filename, "r") as of:
        return map(str.strip, of.readlines())


def move(_pos, _dir, _nb):
    return _pos[0] + _nb * MOV[_dir][0], _pos[1] + _nb * MOV[_dir][1]


def read_instr(_pos, _rot, _in_str):
    mov_nb = int(_in_str[1:])
    mov_dir = _in_str[0]
    if mov_dir in ['R', 'L']:
        sign = {'R': 1, 'L': -1}[mov_dir]
        return _pos, (_rot + sign * mov_nb // 90) % 4
    elif mov_dir in MOV.keys():
        return move(_pos, mov_dir, mov_nb), _rot
    else:
        return move(_pos, list(MOV.keys())[_rot], mov_nb), _rot


if __name__ == '__main__':
    pos, rot = (0, 0), 0
    for in_str in read_input("input.txt"):
        pos, rot = read_instr(pos, rot, in_str)
    print(sum(map(abs, pos)))
