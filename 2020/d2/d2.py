import re

policy = re.compile("^([0-9]*)-([0-9]*) ([a-z]): ([a-z]*)")

test_expected = 2
test_expected_2 = 1


def read_file(filename):
    with open(filename, 'r') as of:
        return [s.strip() for s in of.readlines()]


def calc1(filename):
    data = read_file(filename)
    cpt = 0
    for d in data:
        grp = policy.match(d).groups()
        min_o = int(grp[0])
        max_o = int(grp[1])
        char = grp[2]
        pwd = grp[3]
        if pwd.count(char) in range(min_o, max_o + 1):
            cpt += 1
    return cpt


def calc2(filename):
    data = read_file(filename)
    cpt = 0
    for d in data:
        grp = policy.match(d).groups()
        min_o = int(grp[0]) - 1
        max_o = int(grp[1]) - 1
        char = grp[2]
        pwd = grp[3]
        if pwd[min_o] == char:
            if pwd[max_o] != char:
                cpt += 1
        elif pwd[max_o] == char:
            cpt += 1
    return cpt


if __name__ == '__main__':
    try:
        assert calc1('test.txt') == test_expected
        assert calc2('test.txt') == test_expected_2
    except Exception as e:
        print(f"test 1: expected {test_expected}, got {calc1('test.txt')}")
        print(f"test 2: expected {test_expected_2}, got {calc2('test.txt')}")
    print(f"result 1: {calc1('input.txt')}")
    print(f"result 2: {calc2('input.txt')}")
