package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

const exp1 = 71

type validity struct {
	minV1 int
	maxV1 int
	minV2 int
	maxV2 int
}

func (v validity) valid(value int) bool {
	return (value >= v.minV1 && value <= v.maxV1) || (value >= v.minV2 && value <= v.maxV2)
}

func readRules(rules []string) map[string]validity {
	result := make(map[string]validity)
	for _, line := range rules {
		keyValueSplit := strings.Split(line, ":")
		fieldName := keyValueSplit[0]
		fields := strings.Split(keyValueSplit[1], " ")
		firstRange := strings.Split(fields[1], "-")
		minV1, _ := strconv.Atoi(firstRange[0])
		maxV1, _ := strconv.Atoi(firstRange[1])
		secondRange := strings.Split(fields[3], "-")
		minV2, _ := strconv.Atoi(secondRange[0])
		maxV2, _ := strconv.Atoi(secondRange[1])
		fieldValidity := validity{
			minV1: minV1,
			maxV1: maxV1,
			minV2: minV2,
			maxV2: maxV2,
		}
		result[fieldName] = fieldValidity
	}
	return result
}

func readTicket(str string) []int {
	values := strings.Split(str, ",")
	result := make([]int, len(values))
	for i, vStr := range values {
		vInt, _ := strconv.Atoi(vStr)
		result[i] = vInt
	}
	return result
}

func readInput(filename string) (map[string]validity, []int, [][]int) {
	blocks := aoc.ReadByBlocks(filename)
	rules := readRules(blocks[0])
	ticket := readTicket(blocks[1][1])
	nearby := make([][]int, len(blocks[2])-1)
	for i := 1; i <= len(nearby); i++ {
		nearby[i-1] = readTicket(blocks[2][i])
	}
	return rules, ticket, nearby
}

func getFirstAnswerAndValidTickets(rules map[string]validity, nearby [][]int) (int, [][]int) {
	result := 0
	validTickets := make([][]int, 0)
	for _, ticket := range nearby {
		ticketIsValid := true
		for _, fieldValue := range ticket {
			invalidField := true
			for _, rule := range rules {
				if rule.valid(fieldValue) {
					invalidField = false
					break
				}
			}
			if invalidField {
				ticketIsValid = false
				result += fieldValue
			}
		}
		if ticketIsValid {
			validTickets = append(validTickets, ticket)
		}
	}
	return result, validTickets
}

func cleanup(finalMapping map[string]int, mapFieldsToRows map[string][]int) (map[string]int, map[string][]int) {
	for fieldName, rowsMatching := range mapFieldsToRows {
		if aoc.Count(rowsMatching, 0) == 1 {
			goodRow := aoc.IndexOf(rowsMatching, 0)
			delete(mapFieldsToRows, fieldName)
			for k := range mapFieldsToRows {
				mapFieldsToRows[k][goodRow] = -1
			}
			finalMapping[fieldName] = goodRow
		}
	}
	return finalMapping, mapFieldsToRows
}

func calc(filename string) (int, int) {
	rules, myTicket, nearby := readInput(filename)
	result, validTickets := getFirstAnswerAndValidTickets(rules, nearby)
	// key is name of field, value is array; index of array is row number, value of array is whether match between field
	// and row is still considered possible; match = 0, no match = -1
	mapFieldsToRows := make(map[string][]int)
	finalMapping := make(map[string]int)
	for field, rule := range rules {
		mapFieldsToRows[field] = make([]int, len(myTicket))
		for rowNumber := range myTicket {
			matchStillPossible := true
			for _, ticket := range validTickets {
				if !rule.valid(ticket[rowNumber]) {
					matchStillPossible = false
					break
				}
			}
			if !matchStillPossible {
				mapFieldsToRows[field][rowNumber] = -1
			}
		}
	}
	for len(finalMapping) != len(myTicket) {
		finalMapping, mapFieldsToRows = cleanup(finalMapping, mapFieldsToRows)
	}
	result2 := 1
	for fieldName, rowNb := range finalMapping {
		if strings.HasPrefix(fieldName, "departure ") {
			result2 *= myTicket[rowNb]
		}
	}
	return result, result2
}

func main() {
	t1, _ := calc("test.txt")
	aoc.CheckEqualsMsg(exp1, t1, "test1")
	r1, r2 := calc("input.txt")
	fmt.Printf("result 1: %d\n", r1)
	fmt.Printf("result 2: %d\n", r2)
}
