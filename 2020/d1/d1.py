test_expected = 514579
test_expected_2 = 241861950


def read_file(filename):
    with open(filename, 'r') as of:
        return [int(s.strip()) for s in of.readlines()]


def calc1(filename):
    data = read_file(filename)
    for i in data:
        if 2020 - i in data:
            return i * (2020 - i)
    return 0


def calc2(filename):
    data = read_file(filename)
    for i in range(len(data)):
        for j in range(i, len(data)):
            if data[i] + data[j] < 2020:
                dk = 2020 - data[i] - data[j]
                if dk in data[j:]:
                    return dk * data[i] * data[j]
    return 0


if __name__ == '__main__':
    try:
        assert calc1('test.txt') == test_expected
        assert calc2('test.txt') == test_expected_2
    except Exception as e:
        print(f"test 1: expected {test_expected}, got {calc1('test.txt')}")
        print(f"test 2: expected {test_expected_2}, got {calc2('test.txt')}")
    print(f"result 1: {calc1('input.txt')}")
    print(f"result 2: {calc2('input.txt')}")
