package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

const exp1 = 514579
const exp2 = 241861950

func calc1(filename string) int {
	data := aoc.ReadInputIntegers(filename)
	for i, v := range data {
		if aoc.Contains(data[i+1:], 2020-v) {
			return v * (2020 - v)
		}
	}
	return 0
}

func calc2(filename string) int {
	data := aoc.ReadInputIntegers(filename)
	for i, v := range data {
		for j, w := range data[i+1:] {
			if v+w < 2020 {
				if aoc.Contains(data[j+1:], 2020-v-w) {
					return v * w * (2020 - v - w)
				}
			}
		}
	}
	return 0
}

func main() {
	aoc.CheckEqualsMsg(exp1, calc1("test.txt"), "test1")
	aoc.CheckEqualsMsg(exp2, calc2("test.txt"), "test2")
	r1 := calc1("input.txt")
	r2 := calc2("input.txt")
	fmt.Printf("Result 1: %d\n", r1)
	fmt.Printf("Result 2: %d\n", r2)
}
