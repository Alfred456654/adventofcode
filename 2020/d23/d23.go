package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

const (
	exp1        = 67384529
	cupsPart1   = 9
	iterations1 = 100
	exp2        = 149245887792
	cupsPart2   = 1_000_000
	iterations2 = 10_000_000
)

type doubleLinkedList struct {
	next     *doubleLinkedList
	previous *doubleLinkedList
	value    int
}

func readInput(filename string) (map[int]*doubleLinkedList, int) {
	result := make(map[int]*doubleLinkedList)
	cupsStr := aoc.ReadInputLines(filename)[0]
	var currentCup, nextCup, previousCup, firstCup *doubleLinkedList
	currentCup = &doubleLinkedList{}
	for cupPos := 0; cupPos < len(cupsStr); cupPos++ {
		cupValue, _ := strconv.Atoi(cupsStr[cupPos : cupPos+1])
		currentCup.value = cupValue
		if previousCup != nil {
			currentCup.previous = previousCup
		}
		nextCup = &doubleLinkedList{}
		currentCup.next = nextCup
		result[cupValue] = currentCup
		if firstCup == nil {
			firstCup = currentCup
		}
		previousCup = currentCup
		currentCup = nextCup
	}
	if previousCup == nil || firstCup == nil {
		return nil, 0
	}
	previousCup.next = firstCup
	firstCup.previous = previousCup
	return result, firstCup.value
}

func moveCups(mapOfCups map[int]*doubleLinkedList, currentCup *doubleLinkedList, maxCupValue int) *doubleLinkedList {
	currentCupValue := currentCup.value
	destCupValue := decreaseDestCupValue(currentCupValue, maxCupValue)

	firstCupMoved := currentCup.next
	secondCupMoved := firstCupMoved.next
	lastCupMoved := secondCupMoved.next
	forbiddenValues := make([]int, 3)
	forbiddenValues[0] = firstCupMoved.value
	forbiddenValues[1] = secondCupMoved.value
	forbiddenValues[2] = lastCupMoved.value
	for aoc.Contains(forbiddenValues, destCupValue) {
		destCupValue = decreaseDestCupValue(destCupValue, maxCupValue)
	}

	destinationCup := mapOfCups[destCupValue]
	afterDestinationCup := destinationCup.next
	reattachThere := lastCupMoved.next

	currentCup.next = reattachThere
	reattachThere.previous = currentCup

	destinationCup.next = firstCupMoved
	firstCupMoved.previous = destinationCup

	afterDestinationCup.previous = lastCupMoved
	lastCupMoved.next = afterDestinationCup
	return currentCup.next
}

func decreaseDestCupValue(currentCupValue, maxCupValue int) int {
	destCupValue := currentCupValue - 1
	if destCupValue <= 0 {
		destCupValue = maxCupValue
	}
	return destCupValue
}

func readCupsFrom1(mapOfCups map[int]*doubleLinkedList) int {
	cupsSequence := strings.Builder{}
	curCup := mapOfCups[1].next
	for curCup.value != 1 {
		cupsSequence.WriteString(fmt.Sprintf("%d", curCup.value))
		curCup = curCup.next
	}
	result, _ := strconv.Atoi(cupsSequence.String())
	return result
}

func calc(filename string) int {
	mapOfCups, firstCupValue := readInput(filename)
	currentCup := mapOfCups[firstCupValue]
	for i := 0; i < iterations1; i++ {
		currentCup = moveCups(mapOfCups, currentCup, cupsPart1)
	}
	return readCupsFrom1(mapOfCups)
}

func addCups(mapOfCups map[int]*doubleLinkedList, firstCup *doubleLinkedList, maxCupNumber int) {
	lastCup := firstCup.previous
	previousCup := lastCup
	curCup := &doubleLinkedList{}
	var nextCup, firstNewCup *doubleLinkedList
	for i := cupsPart1 + 1; i <= maxCupNumber; i++ {
		curCup.value = i
		curCup.previous = previousCup
		mapOfCups[i] = curCup
		nextCup = &doubleLinkedList{}
		curCup.next = nextCup
		if firstNewCup == nil {
			firstNewCup = curCup
		}
		previousCup = curCup
		curCup = nextCup
	}
	lastCup.next = firstNewCup
	firstCup.previous = previousCup
	previousCup.next = firstCup
}

func calc2(filename string) int {
	mapOfCups, firstCupValue := readInput(filename)
	currentCup := mapOfCups[firstCupValue]
	addCups(mapOfCups, currentCup, cupsPart2)
	for i := 0; i < iterations2; i++ {
		currentCup = moveCups(mapOfCups, currentCup, cupsPart2)
	}
	v1 := mapOfCups[1].next.value
	v2 := mapOfCups[1].next.next.value
	return v1 * v2
}

func main() {
	aoc.CheckEqualsMsg(exp1, calc("test.txt"), "test1")
	aoc.CheckEqualsMsg(exp2, calc2("test.txt"), "test2")
	fmt.Printf("Result 1: %d\n", calc("input.txt"))
	fmt.Printf("Result 2: %d\n", calc2("input.txt"))
}
