import re

exp1 = 2
exp2 = 4
height_format = re.compile(r"^[0-9]+[a-z]{2}$")
hair_color = re.compile(r"^#[0-9a-f]{6}$")
pass_id = re.compile(r"^[0-9]{9}$")
eye_colors = "amb blu brn gry grn hzl oth".split()


def check_height(hgt):
    if height_format.match(hgt) is None:
        return False
    unit = hgt[-2:]
    if unit == "cm":
        return int(hgt[:-2]) in range(150, 194)
    elif unit == "in":
        return int(hgt[:-2]) in range(59, 77)
    else:
        return False


requirements = {
    "byr": lambda x: int(x) in range(1920, 2003),
    "iyr": lambda x: int(x) in range(2010, 2021),
    "eyr": lambda x: int(x) in range(2020, 2031),
    "hgt": lambda x: check_height(x),
    "hcl": lambda x: hair_color.match(x) is not None,
    "ecl": lambda x: x in eye_colors,
    "pid": lambda x: pass_id.match(x) is not None
}


def read_input(filename):
    with open(filename, 'r') as of:
        return [{k.split(":")[0]: k.split(":")[1] for k in p.split(" ")} for p in
                [p.replace("\n", " ") for p in of.read().split("\n\n")]]


def valid(passport):
    return all([f in passport for f in requirements.keys()])


def valid2(passport):
    if not valid(passport):
        return False
    checks = [criteria(passport[field]) for field, criteria in requirements.items()]
    return all(checks)


def calc(filename):
    passports = read_input(filename)
    return sum(map(valid, passports))


def calc2(filename):
    passports = read_input(filename)
    return sum(map(valid2, passports))


if __name__ == '__main__':
    assert calc("test.txt") == exp1
    assert calc2("test2.txt") == exp2
    print(f"answer 1: {calc('input.txt')}")
    print(f"answer 2: {calc2('input.txt')}")
