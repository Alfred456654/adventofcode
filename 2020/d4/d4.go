package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"os"
	"regexp"
	"strconv"
	"strings"
)

const exp1 = 2
const exp2 = 4

var requirements = make(map[string]func(string) bool)
var heightFormat = regexp.MustCompile("^[0-9]+[a-z]{2}$")
var hairColor = regexp.MustCompile("^#[0-9a-f]{6}$")
var passId = regexp.MustCompile("^[0-9]{9}$")
var eyeColors = strings.Split("amb blu brn gry grn hzl oth", " ")

func readInput(filename string) []map[string]string {
	bytes, _ := os.ReadFile(filename)
	passportStrsRaw := strings.Split(string(bytes), "\n\n")
	passportStrs := make([]string, len(passportStrsRaw))
	metadata := make([]map[string]string, len(passportStrs))
	for i, raw := range passportStrsRaw {
		p := strings.ReplaceAll(raw, "\n", " ")
		metadata[i] = make(map[string]string)
		for _, keyValueStr := range strings.Split(p, " ") {
			keyValue := strings.Split(keyValueStr, ":")
			metadata[i][keyValue[0]] = keyValue[1]
		}
	}
	return metadata
}

func makeRequirements() {
	requirements["byr"] = func(s string) bool {
		i, _ := strconv.Atoi(s)
		return i >= 1920 && i < 2003
	}
	requirements["iyr"] = func(s string) bool {
		i, _ := strconv.Atoi(s)
		return i >= 2010 && i < 2021
	}
	requirements["eyr"] = func(s string) bool {
		i, _ := strconv.Atoi(s)
		return i >= 2020 && i < 2031
	}
	requirements["hgt"] = func(s string) bool {
		if !heightFormat.MatchString(s) {
			return false
		}
		l := len(s)
		unit := s[l-2 : l]
		h, _ := strconv.Atoi(s[0 : l-2])
		switch {
		case unit == "cm":
			return h >= 150 && h < 194
		case unit == "in":
			return h >= 59 && h < 77
		default:
			return false
		}
	}
	requirements["hcl"] = hairColor.MatchString
	requirements["ecl"] = func(s string) bool {
		return aoc.Contains(eyeColors, s)
	}
	requirements["pid"] = passId.MatchString
}

func checkPassportFields(p map[string]string) int {
	for k := range requirements {
		if _, exists := p[k]; !exists {
			return 0
		}
	}
	return 1
}

func fullCheckPassport(p map[string]string) int {
	if checkPassportFields(p) == 1 {
		for k, r := range requirements {
			if !r(p[k]) {
				return 0
			}
		}
		return 1
	}
	return 0
}

func calc(filename string) int {
	passports := readInput(filename)
	valid := 0
	for _, p := range passports {
		valid += checkPassportFields(p)
	}
	return valid
}

func calc2(filename string) int {
	passports := readInput(filename)
	valid := 0
	for _, p := range passports {
		valid += fullCheckPassport(p)
	}
	return valid
}

func main() {
	makeRequirements()
	aoc.CheckEqualsMsg(exp1, calc("test.txt"), "test1")
	aoc.CheckEqualsMsg(exp2, calc2("test2.txt"), "test2")
	r1 := calc("input.txt")
	r2 := calc2("input.txt")
	fmt.Printf("Result1: %d\n", r1)
	fmt.Printf("Result2: %d\n", r2)
}
