package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

const (
	exp11       = 436
	exp12       = 1
	exp13       = 10
	exp14       = 27
	exp15       = 78
	exp16       = 438
	exp17       = 1836
	exp21       = 175594
	exp22       = 2578
	exp23       = 3544142
	exp24       = 261214
	exp25       = 6895259
	exp26       = 18
	exp27       = 362
	totalTurns  = 2020
	totalTurns2 = 30000000
)

var (
	test1 = []int{0, 3, 6}
	test2 = []int{1, 3, 2}
	test3 = []int{2, 1, 3}
	test4 = []int{1, 2, 3}
	test5 = []int{2, 3, 1}
	test6 = []int{3, 2, 1}
	test7 = []int{3, 1, 2}
	input = []int{8, 11, 0, 19, 1, 2}
)

func prepareData(data []int) (map[int]int, int) {
	result := make(map[int]int)
	last := data[len(data)-1]
	for i := 0; i < len(data)-1; i++ {
		result[data[i]] = i
	}
	return result, last
}

func getNext(mapData map[int]int, last, itNb int) int {
	previous, seenBefore := mapData[last]
	if !seenBefore {
		return 0
	}
	return itNb - previous
}

func calc(data []int, nbTurns int) int {
	mapData, last := prepareData(data)
	turn := len(mapData)
	for turn < nbTurns-1 {
		newLast := getNext(mapData, last, turn)
		mapData[last] = turn
		turn++
		last = newLast
	}
	return last
}

func main() {
	aoc.CheckEqualsMsg(exp11, calc(test1, totalTurns), "test1_1")
	aoc.CheckEqualsMsg(exp12, calc(test2, totalTurns), "test1_2")
	aoc.CheckEqualsMsg(exp13, calc(test3, totalTurns), "test1_3")
	aoc.CheckEqualsMsg(exp14, calc(test4, totalTurns), "test1_4")
	aoc.CheckEqualsMsg(exp15, calc(test5, totalTurns), "test1_5")
	aoc.CheckEqualsMsg(exp16, calc(test6, totalTurns), "test1_6")
	aoc.CheckEqualsMsg(exp17, calc(test7, totalTurns), "test1_7")
	aoc.CheckEqualsMsg(exp21, calc(test1, totalTurns2), "test2_1")
	aoc.CheckEqualsMsg(exp22, calc(test2, totalTurns2), "test2_2")
	aoc.CheckEqualsMsg(exp23, calc(test3, totalTurns2), "test2_3")
	aoc.CheckEqualsMsg(exp24, calc(test4, totalTurns2), "test2_4")
	aoc.CheckEqualsMsg(exp25, calc(test5, totalTurns2), "test2_5")
	aoc.CheckEqualsMsg(exp26, calc(test6, totalTurns2), "test2_6")
	aoc.CheckEqualsMsg(exp27, calc(test7, totalTurns2), "test2_7")
	r1 := calc(input, totalTurns)
	fmt.Printf("Result1: %d\n", r1)
	r2 := calc(input, totalTurns2)
	fmt.Printf("Result2: %d\n", r2)
}
