package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"slices"
	"strconv"
	"strings"
)

func readSeeds(blocks [][]string) []int {
	seedChunks := strings.Split(blocks[0][0], " ")[1:]
	seedNumbers := make([]int, len(seedChunks))
	for i, chunk := range seedChunks {
		seedNumbers[i], _ = strconv.Atoi(chunk)
	}
	return seedNumbers
}

func extractRuleFromLine(line string, reversed bool) []int {
	numbersStr := strings.Split(line, " ")
	numbersOnLine := make([]int, len(numbersStr))
	for numberIdOnLine, nbStr := range numbersStr {
		numbersOnLine[numberIdOnLine], _ = strconv.Atoi(nbStr)
	}
	if reversed {
		numbersOnLine[0], numbersOnLine[1] = numbersOnLine[1], numbersOnLine[0]
	}
	return numbersOnLine
}

func makeMappingFunction(rules [][]int) func(inputNumber int) int {
	return func(inputNumber int) int {
		for _, rule := range rules {
			if inputNumber >= rule[1] && inputNumber < rule[1]+rule[2] {
				return inputNumber - rule[1] + rule[0]
			}
		}
		return inputNumber
	}
}

func readMappings(blocks [][]string, reversed bool) []func(int) int {
	result := make([]func(int) int, len(blocks)-1)
	for blockNumber, block := range blocks[1:] {
		rules := make([][]int, len(block)-1)
		for lineNumberInBlock, line := range block[1:] {
			numbersOnLine := extractRuleFromLine(line, reversed)
			rules[lineNumberInBlock] = numbersOnLine
		}
		result[blockNumber] = makeMappingFunction(rules)
	}
	if reversed {
		slices.Reverse(result)
	}
	return result
}

func readInput(filename string) ([]int, []func(int) int) {
	blocks := aoc.ReadByBlocks(filename)
	seedNumbers := readSeeds(blocks)
	mappings := readMappings(blocks, false)
	return seedNumbers, mappings
}

func calc(filename string) int {
	seeds, mappings := readInput(filename)
	result := 0
	for _, seed := range seeds {
		for _, mapping := range mappings {
			seed = mapping(seed)
		}
		if seed < result || result == 0 {
			result = seed
		}
	}
	return result
}

func isInSeedRanges(curLoc int, seedRanges []int) bool {
	for i := 0; i < len(seedRanges); i += 2 {
		if curLoc >= seedRanges[i] && curLoc < seedRanges[i]+seedRanges[i+1] {
			return true
		}
	}
	return false
}

func calc2(filename string) int {
	blocks := aoc.ReadByBlocks(filename)
	seedRanges := readSeeds(blocks)
	mappings := readMappings(blocks, true)
	initialLoc := 0
	for {
		initialLoc++
		curLoc := initialLoc
		for _, mapping := range mappings {
			curLoc = mapping(curLoc)
		}
		if isInSeedRanges(curLoc, seedRanges) {
			break
		}
	}
	return initialLoc
}

func main() {
	aoc.CheckEquals(35, calc("test.txt"))
	aoc.CheckEquals(46, calc2("test.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
