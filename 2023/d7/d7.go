package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"golang.org/x/exp/maps"
	"golang.org/x/exp/slices"
	"strconv"
	"strings"
)

type handType int

const (
	none handType = iota
	highCard
	onePair
	twoPairs
	threeOfAKind
	fullHouse
	fourOfAKind
	fiveOfAKind
)

var (
	highCardVals     = map[int]int{1: 5}
	onePairVals      = map[int]int{2: 1, 1: 3}
	twoPairsVals     = map[int]int{2: 2, 1: 1}
	threeOfAKindVals = map[int]int{3: 1, 1: 2}
	fullHouseVals    = map[int]int{3: 1, 2: 1}
	fourOfAKindVals  = map[int]int{4: 1, 1: 1}
	fiveOfAKindVals  = map[int]int{5: 1}
	orderOfCards     = map[rune]int{'A': 12, 'K': 11, 'Q': 10, 'J': 9, 'T': 8, '9': 7, '8': 6, '7': 5, '6': 4, '5': 3, '4': 2, '3': 1, '2': 0}
	orderOfCards2    = map[rune]int{'A': 12, 'K': 11, 'Q': 10, 'J': -1, 'T': 8, '9': 7, '8': 6, '7': 5, '6': 4, '5': 3, '4': 2, '3': 1, '2': 0}
)

type cardHand struct {
	cardsStr string
	cards    map[rune]int
	hType    handType
	score    int
	part2    bool
}

func compareHands(c, other *cardHand) int {
	if c == nil || other == nil {
		panic("nil hand")
	}
	if c.hType > other.hType {
		return 1
	} else if c.hType < other.hType {
		return -1
	} else {
		order := orderOfCards
		if c.part2 {
			order = orderOfCards2
		}
		for i := 0; i < 5; i++ {
			if order[rune(c.cardsStr[i])] > order[rune(other.cardsStr[i])] {
				return 1
			} else if order[rune(c.cardsStr[i])] < order[rune(other.cardsStr[i])] {
				return -1
			}
		}
	}
	return 0
}

func newCardHand(handStr string, part2 bool) *cardHand {
	h := &cardHand{cards: make(map[rune]int), cardsStr: handStr, part2: part2}
	for _, c := range handStr {
		h.cards[c]++
	}
	h.guessHandType()
	return h
}

func (c *cardHand) guessHandType() {
	countCounts := c.tallyUp()
	if maps.Equal(countCounts, highCardVals) {
		c.hType = highCard
	}
	if maps.Equal(countCounts, onePairVals) {
		c.hType = onePair
	}
	if maps.Equal(countCounts, twoPairsVals) {
		c.hType = twoPairs
	}
	if maps.Equal(countCounts, threeOfAKindVals) {
		c.hType = threeOfAKind
	}
	if maps.Equal(countCounts, fullHouseVals) {
		c.hType = fullHouse
	}
	if maps.Equal(countCounts, fourOfAKindVals) {
		c.hType = fourOfAKind
	}
	if maps.Equal(countCounts, fiveOfAKindVals) {
		c.hType = fiveOfAKind
	}
	if c.hType == none {
		panic("unknown hand type")
	}
}

func (c *cardHand) tallyUp() map[int]int {
	countCounts := make(map[int]int)
	if c.part2 && c.cardsStr != "JJJJJ" {
		nbJ := 0
		for card, count := range c.cards {
			if card != 'J' {
				countCounts[count]++
			} else {
				nbJ = count
			}
		}
		if nbJ != 0 {
			biggestAmount := 0
			var mostFrequentCard rune
			for card, amount := range c.cards {
				if amount > biggestAmount && card != 'J' {
					biggestAmount = amount
					mostFrequentCard = card
				}
			}
			countCounts[biggestAmount]--
			if countCounts[biggestAmount] == 0 {
				delete(countCounts, biggestAmount)
			}
			countCounts[biggestAmount+nbJ]++
			delete(c.cards, 'J')
			c.cards[mostFrequentCard] += nbJ
		}
	} else {
		for _, count := range c.cards {
			countCounts[count]++
		}
	}
	return countCounts
}

func readInput(filename string, part2 bool) []*cardHand {
	lines := aoc.ReadInputLines(filename)
	hands := make([]*cardHand, len(lines))
	for i, line := range lines {
		chunks := strings.Split(line, " ")
		hands[i] = newCardHand(chunks[0], part2)
		hands[i].score, _ = strconv.Atoi(chunks[1])
	}
	return hands
}

func calc(filename string, part2 bool) int {
	hands := readInput(filename, part2)
	result := 0
	slices.SortFunc(hands, compareHands)
	for i, hand := range hands {
		result += (i + 1) * hand.score
	}
	return result
}

func main() {
	aoc.CheckEquals(6440, calc("test.txt", false))
	aoc.CheckEquals(5905, calc("test.txt", true))
	fmt.Println(calc("input.txt", false))
	fmt.Println(calc("input.txt", true))
}
