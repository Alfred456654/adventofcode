package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

func readInput(filename string) ([][]string, [][]string) {
	blocks := aoc.ReadByBlocks(filename)
	invertedBlocks := make([][]string, len(blocks))
	for i, block := range blocks {
		invertedBlocks[i] = invertBlock(block)
	}
	return blocks, invertedBlocks
}

func invertBlock(block []string) []string {
	invertedBlock := make([]string, len(block[0]))
	for _, line := range block {
		for j, char := range line {
			invertedBlock[j] += string(char)
		}
	}
	return invertedBlock
}

func analyzeBlock(block []string) int {
	for l1 := 0; l1 < len(block)-1; l1++ {
		line1 := block[l1]
		line2 := block[l1+1]
		if line1 == line2 {
			mirrored := true
			for i := 0; l1-i >= 0 && l1+1+i < len(block); i++ {
				if block[l1-i] != block[l1+1+i] {
					mirrored = false
					break
				}
			}
			if mirrored {
				return l1 + 1
			}
		}
	}
	return 0
}

func calc(filename string) int {
	blocks, invertedBlocks := readInput(filename)
	byLine := 0
	byColumn := 0
	for _, block := range blocks {
		byLine += analyzeBlock(block)
	}
	for _, block := range invertedBlocks {
		byColumn += analyzeBlock(block)
	}
	return byColumn + 100*byLine
}

func countDifferences(s1, s2 string) int {
	differences := 0
	for i := 0; i < len(s1); i++ {
		if s1[i] != s2[i] {
			differences++
		}
	}
	return differences
}

func analyzeBlock2(block []string) int {
	for l1 := 0; l1 < len(block)-1; l1++ {
		smudgeFound := false
		line1 := block[l1]
		line2 := block[l1+1]
		if countDifferences(line1, line2) <= 1 {
			mirrored := true
			for i := 0; l1-i >= 0 && l1+1+i < len(block); i++ {
				diff := countDifferences(block[l1-i], block[l1+1+i])
				if diff > 1 {
					mirrored = false
					break
				} else if diff == 1 {
					if smudgeFound {
						mirrored = false
						break
					} else {
						smudgeFound = true
					}
				}
			}
			if mirrored && smudgeFound {
				return l1 + 1
			}
		}
	}
	return 0
}

func calc2(filename string) int {
	blocks, invertedBlocks := readInput(filename)
	byLine := 0
	byColumn := 0
	for i := 0; i < len(blocks); i++ {
		resultLines := analyzeBlock2(blocks[i])
		if resultLines == 0 {
			resultColumns := analyzeBlock2(invertedBlocks[i])
			if resultColumns == 0 {
				panic("No result found")
			} else {
				byColumn += resultColumns
			}
		} else {
			byLine += resultLines
		}
	}
	return byColumn + 100*byLine
}

func main() {
	aoc.CheckEquals(405, calc("test.txt"))
	aoc.CheckEquals(400, calc2("test.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
