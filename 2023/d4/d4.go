package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

type scratchcard struct {
	winning   *aoc.Set[int]
	got       *aoc.Set[int]
	cardId    int
	nbMatches int
}

func readInputFile(filename string) []scratchcard {
	lines := aoc.ReadInputLines(filename)
	result := make([]scratchcard, len(lines))
	for l, line := range lines {
		chunks := strings.Split(line, ": ")
		chunks = strings.Split(chunks[1], "| ")
		winning := aoc.NewSet[int]()
		got := aoc.NewSet[int]()
		for _, chunk := range strings.Split(chunks[0], " ") {
			if chunk != "" {
				val, _ := strconv.Atoi(chunk)
				winning.Add(val)
			}
		}
		for _, chunk := range strings.Split(chunks[1], " ") {
			if chunk != "" {
				val, _ := strconv.Atoi(chunk)
				got.Add(val)
			}
		}
		result[l] = scratchcard{winning: winning, got: got, cardId: l + 1, nbMatches: winning.Intersection(got).Size()}
	}
	return result
}

func calc(filename string) int {
	cards := readInputFile(filename)
	result := 0
	for _, card := range cards {
		if card.nbMatches > 0 {
			cardScore := aoc.Pow(2, card.nbMatches-1)
			result += cardScore
		}
	}
	return result
}

func calc2(filename string) int {
	originalDeck := readInputFile(filename)
	winMap := make(map[int][]int)
	newDeck := make([]int, len(originalDeck))
	for i, card := range originalDeck {
		winMap[card.cardId] = make([]int, card.nbMatches)
		for j := 0; j < card.nbMatches; j++ {
			winMap[card.cardId][j] = card.cardId + j + 1
		}
		newDeck[i] = i + 1
	}
	result := len(newDeck)
	for len(newDeck) > 0 {
		newDeck = iterate(newDeck, winMap)
		result += len(newDeck)
	}
	return result
}

func iterate(currentDeck []int, winMap map[int][]int) []int {
	newDeck := make([]int, 0)
	for _, cardId := range currentDeck {
		newDeck = append(newDeck, winMap[cardId]...)
	}
	return newDeck
}

func main() {
	aoc.CheckEquals(13, calc("test.txt"))
	aoc.CheckEquals(30, calc2("test.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
