package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"regexp"
	"strconv"
	"strings"
)

type record struct {
	leftStr       string
	regexpToMatch *regexp.Regexp
	wantDots      int
}

func generatePermutations(length, nbDots, nbSharps, wantDots, wantSharps int, currentStr string) []string {
	result := make([]string, 0)
	if len(currentStr) == length && nbDots == wantDots && nbSharps == wantSharps {
		result = append(result, currentStr)
	}

	if nbDots < wantDots {
		result = append(result, generatePermutations(length, nbDots+1, nbSharps, wantDots, wantSharps, currentStr+".")...)
	}

	if nbSharps < wantSharps {
		result = append(result, generatePermutations(length, nbDots, nbSharps+1, wantDots, wantSharps, currentStr+"#")...)
	}
	return result
}

func readInput(filename string) ([]*record, map[int]map[int][]string) {
	lines := aoc.ReadInputLines(filename)
	result := make([]*record, len(lines))
	permutationsCache := make(map[int]map[int][]string)
	for l, line := range lines {
		leftAndRight := strings.Split(line, " ")
		leftStr := leftAndRight[0]
		rightChunks := strings.Split(leftAndRight[1], ",")
		rightDef := make([]int, len(rightChunks))

		totalSharps := 0
		for i, chunk := range rightChunks {
			rightDef[i], _ = strconv.Atoi(chunk)
			totalSharps += rightDef[i]
		}
		wantSharps := totalSharps - strings.Count(leftStr, "#")
		nbQuestionMarks := strings.Count(leftStr, "?")
		wantDots := nbQuestionMarks - wantSharps
		result[l] = &record{
			leftStr:       leftStr,
			regexpToMatch: makeRegexpToMatch(rightDef),
			wantDots:      wantDots,
		}

		_, exists := permutationsCache[nbQuestionMarks]
		if !exists {
			permutationsCache[nbQuestionMarks] = make(map[int][]string)
		}
		_, exists = permutationsCache[nbQuestionMarks][wantDots]
		if !exists {
			permutationsCache[nbQuestionMarks][wantDots] = generatePermutations(nbQuestionMarks, 0, 0, wantDots, wantSharps, "")
		}
	}
	return result, permutationsCache
}

func makeRegexpToMatch(def []int) *regexp.Regexp {
	result := strings.Builder{}
	result.WriteString(`^\.*`)
	for i, nbSharps := range def {
		result.WriteString(fmt.Sprintf(`#{%d}`, nbSharps))
		if i < len(def)-1 {
			result.WriteString(`\.+`)
		} else {
			result.WriteString(`\.*`)
		}
	}
	result.WriteString(`$`)
	return regexp.MustCompile(result.String())
}

func doReplacement(inStr string, permutation string) string {
	result := strings.Builder{}
	idx := 0
	for _, char := range inStr {
		if char == '?' {
			result.WriteByte(permutation[idx])
			idx++
		} else {
			result.WriteRune(char)
		}
	}
	return result.String()
}

func solve(records []*record, permCache map[int]map[int][]string) int {
	result := 0
	for _, rec := range records {
		partialResult := 0
		nbQM := strings.Count(rec.leftStr, "?")
		nbDots := rec.wantDots
		permutations := permCache[nbQM][nbDots]
		for _, permut := range permutations {
			correction := doReplacement(rec.leftStr, permut)
			if rec.regexpToMatch.MatchString(correction) {
				partialResult++
			}
		}
		result += partialResult
	}
	return result
}

func calc(filename string) int {
	records, permCache := readInput(filename)
	return solve(records, permCache)
}

func main() {
	aoc.CheckEquals(21, calc("test1.txt"))
	fmt.Println(calc("input.txt"))
}
