package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"regexp"
)

var (
	leftRight  = map[rune]int{'L': 0, 'R': 1}
	lineFormat = regexp.MustCompile(`(.{3}) = \((.{3}), (.{3})\)`)
)

func readInput(filename string) ([]int, map[string][]string, []string) {
	lines := aoc.ReadInputLines(filename)
	startNodes := make([]string, 0)
	directions := make([]int, len(lines[0]))
	for i, dir := range lines[0] {
		directions[i] = leftRight[dir]
	}
	proximity := make(map[string][]string)
	for _, line := range lines[1:] {
		chunks := lineFormat.FindAllStringSubmatch(line, -1)
		proximity[chunks[0][1]] = []string{chunks[0][2], chunks[0][3]}
		if chunks[0][1][2] == 'A' {
			startNodes = append(startNodes, chunks[0][1])
		}
	}
	return directions, proximity, startNodes
}

func calc(filename string) int {
	directions, proximity, _ := readInput(filename)
	currentLocation := "AAA"
	dirIndex := 0
	result := 0
	for currentLocation != "ZZZ" {
		currentLocation = proximity[currentLocation][directions[dirIndex]]
		dirIndex++
		if dirIndex > len(directions)-1 {
			dirIndex = 0
		}
		result++
	}
	return result
}

func calc2(filename string) int {
	directions, proximity, startNodes := readInput(filename)
	result := 1
	for _, start := range startNodes {
		dirIndex := 0
		distance := 0
		currentLocation := start
		for currentLocation[2] != 'Z' {
			currentLocation = proximity[currentLocation][directions[dirIndex]]
			dirIndex++
			if dirIndex > len(directions)-1 {
				dirIndex = 0
			}
			distance++
		}
		result = aoc.Ppcm(result, distance)
	}
	return result
}

func main() {
	aoc.CheckEquals(2, calc("test1A.txt"))
	aoc.CheckEquals(6, calc("test1B.txt"))
	aoc.CheckEquals(6, calc2("test2.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
