package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

func hashStr(inStr string) uint8 {
	var v uint8
	for _, r := range inStr {
		v += uint8(r)
		v *= 17
	}
	return v
}

func calc(filename string) int {
	var res int
	for _, word := range strings.Split(aoc.ReadInputLines(filename)[0], ",") {
		res += int(hashStr(word))
	}
	return res
}

type lensDef struct {
	name  string
	hash  uint8
	value int
}

func calc2(filename string) int {
	var res int
	boxes := make(map[uint8][]lensDef)
	instrs := strings.Split(aoc.ReadInputLines(filename)[0], ",")
	for _, instr := range instrs {
		if instr[len(instr)-1] == '-' {
			name := instr[:len(instr)-1]
			hash := hashStr(name)
			contents, ok := boxes[hash]
			if ok {
				for idx, l := range contents {
					if l.name == name {
						boxes[hash] = append(boxes[hash][:idx], boxes[hash][idx+1:]...)
					}
				}
			}
		} else {
			chunks := strings.Split(instr, "=")
			name := chunks[0]
			value, _ := strconv.Atoi(chunks[1])
			hash := hashStr(name)
			newLens := lensDef{
				name:  name,
				hash:  hash,
				value: value,
			}
			contents, ok := boxes[hash]
			if !ok {
				boxes[hash] = []lensDef{newLens}
			} else {
				replaced := false
				for idx, l := range contents {
					if l.name == name {
						boxes[hash][idx] = newLens
						replaced = true
						break
					}
				}
				if !replaced {
					boxes[hash] = append(boxes[hash], newLens)
				}
			}
		}
	}
	for boxNum, boxContents := range boxes {
		for idx, lens := range boxContents {
			res += (int(boxNum) + 1) * (idx + 1) * lens.value
		}
	}
	return res
}

func main() {
	aoc.CheckEquals(1320, calc("test.txt"))
	fmt.Println(calc("input.txt"))
	aoc.CheckEquals(145, calc2("test.txt"))
	fmt.Println(calc2("input.txt"))
}
