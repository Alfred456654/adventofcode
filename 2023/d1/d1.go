package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"regexp"
	"strconv"
)

var (
	numbers = map[string]string{"one": "1", "two": "2", "three": "3", "four": "4", "five": "5", "six": "6", "seven": "7", "eight": "8", "nine": "9"}
)

func transform(inStr string) int {
	replacements := make([]string, len(inStr))
	for nbStr := range numbers {
		indexes := regexp.MustCompile(nbStr).FindAllStringIndex(inStr, -1)
		for _, index := range indexes {
			replacements[index[0]] = nbStr
		}
	}
	var result string
	for i := len(inStr) - 1; i >= 0; i-- {
		if replacements[i] != "" {
			result = fmt.Sprintf("%s%s", numbers[replacements[i]], result)
		} else if inStr[i] >= '0' && inStr[i] <= '9' {
			result = fmt.Sprintf("%s%s", string(inStr[i]), result)
		}
	}
	nb, _ := strconv.Atoi(string(result[0]) + string(result[len(result)-1]))
	return nb
}

func calc(filename string) int {
	lines := aoc.ReadInputLines(filename)
	var first, last string
	var result int
	for _, line := range lines {
		for _, r := range line {
			if r >= '0' && r <= '9' {
				first = string(r)
				break
			}
		}
		for i := len(line) - 1; i >= 0; i-- {
			if line[i] >= '0' && line[i] <= '9' {
				last = string(line[i])
				break
			}
		}
		lineResult, _ := strconv.Atoi(first + last)
		result += lineResult
	}
	return result
}

func calc2(filename string) int {
	lines := aoc.ReadInputLines(filename)
	foundNumbers := make([]int, len(lines))
	for l, line := range lines {
		foundNumbers[l] = transform(line)
	}
	return aoc.Sum(foundNumbers)
}

func main() {
	aoc.CheckEquals(142, calc("test.txt"))
	aoc.CheckEquals(281, calc2("test2.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
