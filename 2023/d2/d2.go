package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"regexp"
	"strconv"
	"strings"
)

type color int

const (
	red color = iota
	green
	blue
)

var (
	gameStr            = regexp.MustCompile(`^Game (\d+): (.*)$`)
	redShown           = regexp.MustCompile(`(\d+) red`)
	greenShown         = regexp.MustCompile(`(\d+) green`)
	blueShown          = regexp.MustCompile(`(\d+) blue`)
	defaultConstraints = map[color]int{
		red:   12,
		green: 13,
		blue:  14,
	}
)

func readInput(filename string) map[int][]map[color]int {
	lines := aoc.ReadInputLines(filename)
	result := make(map[int][]map[color]int, len(lines))
	for _, line := range lines {
		matches := gameStr.FindStringSubmatch(line)
		if matches == nil {
			continue
		}
		gameNum, _ := strconv.Atoi(matches[1])
		diceShownList := strings.Split(matches[2], "; ")
		result[gameNum] = make([]map[color]int, len(diceShownList))
		for i, diceShown := range diceShownList {
			result[gameNum][i] = make(map[color]int)
			if redMatches := redShown.FindStringSubmatch(diceShown); redMatches != nil {
				result[gameNum][i][red], _ = strconv.Atoi(redMatches[1])
			}
			if greenMatches := greenShown.FindStringSubmatch(diceShown); greenMatches != nil {
				result[gameNum][i][green], _ = strconv.Atoi(greenMatches[1])
			}
			if blueMatches := blueShown.FindStringSubmatch(diceShown); blueMatches != nil {
				result[gameNum][i][blue], _ = strconv.Atoi(blueMatches[1])
			}
		}
	}
	return result
}

func calc(filename string) int {
	games := readInput(filename)
	var result int
	for gameId, gameDefs := range games {
		valid := true
		for _, gameDef := range gameDefs {
			for col, nb := range gameDef {
				if nb > defaultConstraints[col] {
					valid = false
				}
			}
		}
		if valid {
			result += gameId
		}
	}
	return result
}

func calc2(filename string) int {
	games := readInput(filename)
	gamePowers := make([]int, len(games))
	for i, gameDefs := range games {
		minNbOfDice := make(map[color]int)
		for _, gameDef := range gameDefs {
			for col, nb := range gameDef {
				if nb > minNbOfDice[col] {
					minNbOfDice[col] = nb
				}
			}
		}
		gamePower := 1
		for _, nb := range minNbOfDice {
			gamePower *= nb
		}
		gamePowers[i-1] = gamePower
	}
	return aoc.Sum(gamePowers)
}

func main() {
	aoc.CheckEquals(8, calc("test.txt"))
	aoc.CheckEquals(2286, calc2("test.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
