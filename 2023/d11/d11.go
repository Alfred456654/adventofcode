package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

func expand(galaxy *aoc.Grid) ([]int, []int) {
	emptyRows := make([]int, 0)
	emptyColumns := make([]int, 0)
	for y := 0; y < galaxy.Height; y++ {
		empty := true
		for x := 0; x < galaxy.Width; x++ {
			if galaxy.Cells[aoc.Tuple{X: x, Y: y}] == '#' {
				empty = false
				break
			}
		}
		if empty {
			emptyRows = append(emptyRows, y)
		}
	}
	for x := 0; x < galaxy.Width; x++ {
		empty := true
		for y := 0; y < galaxy.Height; y++ {
			if galaxy.Cells[aoc.Tuple{X: x, Y: y}] == '#' {
				empty = false
				break
			}
		}
		if empty {
			emptyColumns = append(emptyColumns, x)
		}
	}
	return emptyRows, emptyColumns
}

func findClosest(value int, values []int) int {
	for i, v := range values {
		if v > value {
			return i
		}
	}
	return len(values)
}

func calc(filename string, factor int) int {
	galaxy := aoc.NewGridFromFile(filename, false)
	emptyRows, emptyColumns := expand(galaxy)
	stars := galaxy.FindAll(func(x int, y int, r rune) bool {
		return r == '#'
	})
	result := 0
	for i, star1 := range stars {
		for j := i + 1; j < len(stars); j++ {
			star2 := stars[j]
			xIndex1 := findClosest(star1.X, emptyColumns)
			xIndex2 := findClosest(star2.X, emptyColumns)
			yIndex1 := findClosest(star1.Y, emptyRows)
			yIndex2 := findClosest(star2.Y, emptyRows)
			expansion := aoc.Abs(xIndex1-xIndex2) + aoc.Abs(yIndex1-yIndex2)
			initialDistance := aoc.Abs(star1.X-star2.X) + aoc.Abs(star1.Y-star2.Y)
			result += (factor-1)*expansion + initialDistance
		}
	}
	return result
}

func main() {
	aoc.CheckEquals(374, calc("test.txt", 2))
	fmt.Println(calc("input.txt", 2))
	aoc.CheckEquals(1030, calc("test.txt", 10))
	aoc.CheckEquals(8410, calc("test.txt", 100))
	fmt.Println(calc("input.txt", 1000000))
}
