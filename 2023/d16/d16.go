package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
)

var (
	up    = aoc.Tuple{X: 0, Y: -1}
	left  = aoc.Tuple{X: -1, Y: 0}
	down  = aoc.Tuple{X: 0, Y: 1}
	right = aoc.Tuple{X: 1, Y: 0}

	reflectSlash = map[aoc.Tuple]aoc.Tuple{
		up:    right,
		right: up,
		down:  left,
		left:  down,
	}

	reflectBackSlash = map[aoc.Tuple]aoc.Tuple{
		up:    left,
		left:  up,
		down:  right,
		right: down,
	}
)

type beamDef struct {
	loc         aoc.Tuple
	orientation aoc.Tuple
}

func propagate(beams []beamDef, energized *aoc.TupleSet, grid *aoc.Grid, eventHistory map[aoc.Tuple]map[aoc.Tuple]struct{}) []beamDef {
	if len(beams) == 0 {
		return beams
	}
	beam := beams[0]
	beams = beams[1:]
	events, visited := eventHistory[beam.loc]
	if visited {
		_, known := events[beam.orientation]
		if known {
			return beams
		}
	} else {
		eventHistory[beam.loc] = make(map[aoc.Tuple]struct{})
	}
	energized.Add(beam.loc)
	eventHistory[beam.loc][beam.orientation] = struct{}{}
	nextPos := beam.loc.Add(beam.orientation)
	cell, inside := grid.GetAt(nextPos)
	if !inside {
		return beams
	}
	return append(beams, updateOrientation(beamDef{loc: nextPos, orientation: beam.orientation}, cell)...)
}

func updateOrientation(beam beamDef, cell rune) []beamDef {
	curOr := beam.orientation
	switch {
	case cell == '/':
		return []beamDef{{loc: beam.loc, orientation: reflectSlash[curOr]}}
	case cell == '\\':
		return []beamDef{{loc: beam.loc, orientation: reflectBackSlash[curOr]}}
	case cell == '|' && (curOr == left || curOr == right):
		return []beamDef{{loc: beam.loc, orientation: down}, {loc: beam.loc, orientation: up}}
	case cell == '-' && (curOr == up || curOr == down):
		return []beamDef{{loc: beam.loc, orientation: left}, {loc: beam.loc, orientation: right}}
	default:
		return []beamDef{{loc: beam.loc, orientation: curOr}}
	}
}

func simulate(beam beamDef, grid *aoc.Grid) int {
	energized := aoc.NewTupleSet()
	eventHistory := make(map[aoc.Tuple]map[aoc.Tuple]struct{})
	firstTile, _ := grid.GetAt(beam.loc)
	beams := updateOrientation(beam, firstTile)
	for len(beams) > 0 {
		beams = propagate(beams, energized, grid, eventHistory)
	}
	return energized.Size()
}

func calc(filename string) int {
	grid := aoc.NewGridFromFile(filename, false)
	beam := beamDef{orientation: right}
	return simulate(beam, grid)
}

func calc2(filename string) int {
	grid := aoc.NewGridFromFile(filename, false)
	var res int
	for x := 0; x < grid.Width; x++ {
		beam := beamDef{loc: aoc.Tuple{X: x, Y: 0}, orientation: down}
		res = aoc.Max(res, simulate(beam, grid))
		beam = beamDef{loc: aoc.Tuple{X: x, Y: grid.Height - 1}, orientation: up}
		res = aoc.Max(res, simulate(beam, grid))
	}
	for y := 0; y < grid.Height; y++ {
		beam := beamDef{loc: aoc.Tuple{X: 0, Y: y}, orientation: right}
		res = aoc.Max(res, simulate(beam, grid))
		beam = beamDef{loc: aoc.Tuple{X: grid.Width - 1, Y: y}, orientation: left}
		res = aoc.Max(res, simulate(beam, grid))
	}
	return res
}

func main() {
	aoc.CheckEquals(46, calc("test.txt"))
	fmt.Println(calc("input.txt"))
	aoc.CheckEquals(51, calc2("test.txt"))
	fmt.Println(calc2("input.txt"))
}
