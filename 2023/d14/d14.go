package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strings"
)

const (
	nbIt       = 1_000_000_000
	fourPoints = "NWSE"
)

var (
	neighbours = map[rune]func(aoc.Tuple) aoc.Tuple{
		'N': func(t aoc.Tuple) aoc.Tuple {
			return aoc.Tuple{X: t.X, Y: t.Y - 1}
		},
		'S': func(t aoc.Tuple) aoc.Tuple {
			return aoc.Tuple{X: t.X, Y: t.Y + 1}
		},
		'E': func(t aoc.Tuple) aoc.Tuple {
			return aoc.Tuple{X: t.X + 1, Y: t.Y}
		},
		'W': func(t aoc.Tuple) aoc.Tuple {
			return aoc.Tuple{X: t.X - 1, Y: t.Y}
		},
	}

	edgeCondition = map[rune]func(aoc.Tuple, *aoc.Grid) bool{
		'N': func(t aoc.Tuple, p *aoc.Grid) bool {
			return t.Y > 0
		},
		'S': func(t aoc.Tuple, p *aoc.Grid) bool {
			return t.Y < p.Height-1
		},
		'W': func(t aoc.Tuple, p *aoc.Grid) bool {
			return t.X > 0
		},
		'E': func(t aoc.Tuple, p *aoc.Grid) bool {
			return t.X < p.Width-1
		},
	}
)

func hash(plate *aoc.Grid) string {
	hashStr := strings.Builder{}
	for i := 0; i < plate.Height; i++ {
		for j := 0; j < plate.Width; j++ {
			hashStr.WriteRune(plate.Cells[aoc.Tuple{X: j, Y: i}])
		}
	}
	return hashStr.String()
}

func slide(plate *aoc.Grid, cardinal rune) {
	finished := false
	for !finished {
		roundRocks := plate.FindAll(func(x int, y int, r rune) bool {
			return r == 'O' &&
				edgeCondition[cardinal](aoc.Tuple{X: x, Y: y}, plate) &&
				plate.Cells[neighbours[cardinal](aoc.Tuple{X: x, Y: y})] == '.'
		})
		finished = len(roundRocks) == 0
		for _, rock := range roundRocks {
			plate.Cells[rock] = '.'
			plate.Cells[neighbours[cardinal](rock)] = 'O'
		}
	}
}

func calc(filename string) int {
	plate := aoc.NewGridFromFile(filename, false)
	slide(plate, 'N')
	result := computeNorthLoad(plate)
	return result
}

func computeNorthLoad(plate *aoc.Grid) int {
	roundRocks := plate.FindAll(func(x int, y int, r rune) bool {
		return r == 'O'
	})
	result := 0
	for _, rock := range roundRocks {
		result += plate.Height - rock.Y
	}
	return result
}

func calc2(filename string) int {
	plate := aoc.NewGridFromFile(filename, false)
	hashes := make(map[string]int)
	northLoads := make([]int, 0)
	for i := 0; i < nbIt; i++ {
		for _, cardinal := range fourPoints {
			slide(plate, cardinal)
		}
		hashVal := hash(plate)
		if j, ok := hashes[hashVal]; ok {
			return northLoads[j+(nbIt-j)%(i-j)-1]
		}
		hashes[hashVal] = i
		northLoads = append(northLoads, computeNorthLoad(plate))
	}
	return -1
}

func main() {
	aoc.CheckEquals(136, calc("test.txt"))
	aoc.CheckEquals(64, calc2("test.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
