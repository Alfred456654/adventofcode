package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"golang.org/x/exp/maps"
	"regexp"
	"strings"
)

type neighbourPlacement int

const (
	above neighbourPlacement = iota
	left
	below
	right
)

var (
	connectsUp = map[rune]struct{}{
		'|': {},
		'J': {},
		'L': {},
		'S': {},
	}

	connectsDown = map[rune]struct{}{
		'|': {},
		'7': {},
		'F': {},
		'S': {},
	}

	connectsLeft = map[rune]struct{}{
		'-': {},
		'7': {},
		'J': {},
		'S': {},
	}

	connectsRight = map[rune]struct{}{
		'-': {},
		'L': {},
		'F': {},
		'S': {},
	}

	borders = []*regexp.Regexp{
		regexp.MustCompile(`\|`),
		regexp.MustCompile(`L-*7`),
		regexp.MustCompile(`F-*J`),
	}

	pipeSymbols = regexp.MustCompile(`[|L7JF-]`)
)

func findNavigableNeighbours(maze *aoc.Grid, visited map[aoc.Tuple]rune, coords aoc.Tuple) map[aoc.Tuple]rune {
	return maze.GetNeighbours(coords.X, coords.Y, func(nx, ny int, neighb rune) bool {
		if _, contains := visited[aoc.Tuple{X: nx, Y: ny}]; contains {
			return false
		}
		dx := nx - coords.X
		dy := ny - coords.Y
		if dx*dy != 0 {
			return false
		}
		here := maze.Cells[coords]
		relativePlacement := getRelativePlacement(dx, dy)
		return areConnected(relativePlacement, here, neighb)
	})
}

func areConnected(relativePlacement neighbourPlacement, here rune, neighb rune) bool {
	switch relativePlacement {
	case above:
		_, ok := connectsUp[here]
		_, ok2 := connectsDown[neighb]
		return ok && ok2
	case left:
		_, ok := connectsLeft[here]
		_, ok2 := connectsRight[neighb]
		return ok && ok2
	case below:
		_, ok := connectsDown[here]
		_, ok2 := connectsUp[neighb]
		return ok && ok2
	case right:
		_, ok := connectsRight[here]
		_, ok2 := connectsLeft[neighb]
		return ok && ok2
	default:
		panic("unreachable")
	}
}

func getRelativePlacement(dx int, dy int) neighbourPlacement {
	var relativePlacement neighbourPlacement
	if dx == 0 {
		if dy > 0 {
			relativePlacement = below
		} else {
			relativePlacement = above
		}
	} else {
		if dx > 0 {
			relativePlacement = right
		} else {
			relativePlacement = left
		}
	}
	return relativePlacement
}

func iterate(visited, toVisit map[aoc.Tuple]rune, maze *aoc.Grid) (map[aoc.Tuple]rune, map[aoc.Tuple]rune) {
	var newVisits = make(map[aoc.Tuple]rune)
	for coords := range toVisit {
		neighbours := findNavigableNeighbours(maze, visited, coords)
		maps.Copy(newVisits, neighbours)
		visited[coords] = maze.Cells[coords]
	}
	toVisit = newVisits
	return visited, toVisit
}

func calc(filename string) (int, int) {
	maze := aoc.NewGridFromFile(filename, false)
	//display(maze)
	origin := maze.FindAll(func(x, y int, r rune) bool {
		return r == 'S'
	})[0]
	visited, result := computeDistanceToFurthestPoint(maze, origin)
	forgetUselessPipes(maze, visited)
	replaceOriginWithPipe(maze, origin)
	result2 := countTilesInside(maze)
	return result, result2
}

func forgetUselessPipes(maze *aoc.Grid, visited map[aoc.Tuple]rune) {
	for coords := range maze.Cells {
		if _, contains := visited[coords]; !contains {
			maze.Cells[coords] = '.'
		}
	}
}

func computeDistanceToFurthestPoint(maze *aoc.Grid, origin aoc.Tuple) (map[aoc.Tuple]rune, int) {
	visited := make(map[aoc.Tuple]rune)
	toVisit := map[aoc.Tuple]rune{origin: 'S'}
	result := -1
	for len(toVisit) > 0 {
		result++
		visited, toVisit = iterate(visited, toVisit, maze)
	}
	return visited, result
}

func countTilesInside(maze *aoc.Grid) int {
	result2 := 0
	for y := 0; y < maze.Height; y++ {
		lineBuilder := strings.Builder{}
		for x := 0; x < maze.Width; x++ {
			coords := aoc.Tuple{X: x, Y: y}
			tile := maze.Cells[coords]
			lineBuilder.WriteRune(tile)
		}
		line := lineBuilder.String()
		for _, borderRE := range borders {
			line = borderRE.ReplaceAllString(line, "1")
		}
		line = pipeSymbols.ReplaceAllString(line, "")
		parity := false
		for _, r := range line {
			if r == '1' {
				parity = !parity
			}
			if r == '.' && parity {
				result2++
			}
		}
	}
	return result2
}

func replaceOriginWithPipe(maze *aoc.Grid, origin aoc.Tuple) {
	connected := lookAtNeighboursAroundOrigin(maze, origin)
	if connected[aoc.Tuple{X: 0, Y: -1}] {
		if connected[aoc.Tuple{X: 1, Y: 0}] {
			maze.Cells[origin] = 'L'
		} else if connected[aoc.Tuple{X: -1, Y: 0}] {
			maze.Cells[origin] = 'J'
		} else if connected[aoc.Tuple{X: 0, Y: 1}] {
			maze.Cells[origin] = '|'
		}
	} else if connected[aoc.Tuple{X: 0, Y: 1}] {
		if connected[aoc.Tuple{X: 1, Y: 0}] {
			maze.Cells[origin] = 'F'
		} else if connected[aoc.Tuple{X: -1, Y: 0}] {
			maze.Cells[origin] = '7'
		}
	} else {
		if connected[aoc.Tuple{X: 1, Y: 0}] && connected[aoc.Tuple{X: -1, Y: 0}] {
			maze.Cells[origin] = '-'
		} else {
			panic("unreachable")
		}
	}
}

func lookAtNeighboursAroundOrigin(maze *aoc.Grid, origin aoc.Tuple) map[aoc.Tuple]bool {
	aroundOrigin := maze.GetNeighbours(origin.X, origin.Y, func(nx, ny int, neighb rune) bool {
		return neighb != '.'
	})
	connected := make(map[aoc.Tuple]bool)
	for neighbCoords, neighb := range aroundOrigin {
		dx := neighbCoords.X - origin.X
		dy := neighbCoords.Y - origin.Y
		if dx*dy != 0 {
			continue
		}
		relativePlacement := getRelativePlacement(dx, dy)
		connected[aoc.Tuple{X: dx, Y: dy}] = areConnected(relativePlacement, 'S', neighb)
	}
	return connected
}

func main() {
	t1, _ := calc("test1.txt")
	aoc.CheckEqualsMsg(4, t1, "1")
	t2, _ := calc("test2.txt")
	aoc.CheckEqualsMsg(8, t2, "2")
	_, t3 := calc("test3.txt")
	aoc.CheckEqualsMsg(4, t3, "3")
	_, t4 := calc("test4.txt")
	aoc.CheckEqualsMsg(8, t4, "4")
	_, t5 := calc("test5.txt")
	aoc.CheckEqualsMsg(10, t5, "5")
	fmt.Println(calc("input.txt"))
}
