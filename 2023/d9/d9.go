package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"slices"
	"strconv"
	"strings"
)

func readInput(filename string) [][]int64 {
	lines := aoc.ReadInputLines(filename)
	result := make([][]int64, len(lines))
	for i, line := range lines {
		chunks := strings.Split(line, " ")
		result[i] = make([]int64, len(chunks))
		for j, chunk := range chunks {
			result[i][j], _ = strconv.ParseInt(chunk, 10, 64)
		}
	}
	return result
}

func derive(values []int64) []int64 {
	result := make([]int64, len(values)-1)
	for i := 0; i < len(values)-1; i++ {
		result[i] = values[i+1] - values[i]
	}
	return result
}

func guessNext(values []int64) int64 {
	if aoc.Sum(values) == 0 && slices.Max(values) == 0 && slices.Min(values) == 0 {
		return 0
	}
	result := values[len(values)-1] + guessNext(derive(values))
	return result
}

func guessPrevious(values []int64) int64 {
	if aoc.Sum(values) == 0 && slices.Max(values) == 0 && slices.Min(values) == 0 {
		return 0
	}
	result := values[0] - guessPrevious(derive(values))
	return result
}

func calc(filename string) (int64, int64) {
	sequences := readInput(filename)
	var sumOfNext, sumOfPrevious int64
	for _, seq := range sequences {
		sumOfNext += guessNext(seq)
		sumOfPrevious += guessPrevious(seq)
	}
	return sumOfNext, sumOfPrevious
}

func main() {
	part1, part2 := calc("test.txt")
	aoc.CheckEquals(114, part1)
	aoc.CheckEquals(2, part2)
	fmt.Println(calc("input.txt"))
}
