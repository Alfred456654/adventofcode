package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"regexp"
	"strconv"
	"strings"
)

var (
	blank = regexp.MustCompile(`\s+`)
)

func holdButtonToDistance(holdDuration, raceDuration int) int {
	return (raceDuration - holdDuration) * holdDuration
}

func readInput(filename string) ([]int, []int) {
	lines := aoc.ReadInputLines(filename)

	timesStr := blank.ReplaceAllString(lines[0], " ")
	timeChunks := strings.Split(timesStr, " ")

	distancesStr := blank.ReplaceAllString(lines[1], " ")
	distanceChunks := strings.Split(distancesStr, " ")

	if len(distanceChunks) != len(timeChunks) {
		panic("invalid input")
	}

	times := make([]int, len(timeChunks)-1)
	distances := make([]int, len(distanceChunks)-1)

	for i, chunk := range timeChunks[1:] {
		times[i], _ = strconv.Atoi(chunk)
		distances[i], _ = strconv.Atoi(distanceChunks[i+1])
	}
	return times, distances
}

func readInput2(filename string) ([]int, []int) {
	lines := aoc.ReadInputLines(filename)

	timeStr := strings.Replace(strings.Split(lines[0], ":")[1], " ", "", -1)
	distanceStr := strings.Replace(strings.Split(lines[1], ":")[1], " ", "", -1)

	time, _ := strconv.Atoi(timeStr)
	distance, _ := strconv.Atoi(distanceStr)
	return []int{time}, []int{distance}
}

func simulateRaces(times, distances []int) int {
	results := make([]int, len(times))
	for i, time := range times {
		for j := 0; j < time; j++ {
			distance := holdButtonToDistance(j, time)
			if distance > distances[i] {
				results[i]++
			}
		}
	}
	return aoc.Prod(results)
}

func calc(filename string) int {
	times, distances := readInput(filename)
	return simulateRaces(times, distances)
}

func calc2(filename string) int {
	times, distances := readInput2(filename)
	return simulateRaces(times, distances)
}

func main() {
	aoc.CheckEquals(288, calc("test.txt"))
	aoc.CheckEquals(71503, calc2("test.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
