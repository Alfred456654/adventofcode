package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"regexp"
	"strconv"
)

var (
	numberRE = regexp.MustCompile(`\d+`)
	symbolRE = regexp.MustCompile(`[^0-9.]`)
)

type numberLocation struct {
	lineNumber int
	column     int
	length     int
	value      int
}

func (nl numberLocation) close(x, y int) bool {
	return x+1 >= nl.column && x <= nl.column+nl.length && aoc.Abs(y-nl.lineNumber) <= 1
}

func readPuzzleInput(filename string) ([]numberLocation, *aoc.Grid) {
	lines := aoc.ReadInputLines(filename)
	numLocs := make([]numberLocation, 0)
	for i, line := range lines {
		for _, match := range numberRE.FindAllStringIndex(line, -1) {
			atoi, err := strconv.Atoi(line[match[0]:match[1]])
			if err != nil {
				panic(err)
			}
			numLocs = append(numLocs, numberLocation{
				lineNumber: i,
				column:     match[0],
				length:     match[1] - match[0],
				value:      atoi,
			})
		}
	}
	grid := aoc.NewGridFromFile(filename, false)
	return numLocs, grid
}

func calc(filename string) int {
	numLocs, grid := readPuzzleInput(filename)
	correctNumbers := make([]int, 0)
	for _, numLoc := range numLocs {
		n := grid.CountNeighboursAroundZone(numLoc.column, numLoc.column+numLoc.length-1, numLoc.lineNumber, numLoc.lineNumber, func(x, y int, r rune) bool { return symbolRE.MatchString(string(r)) })
		if n > 0 {
			correctNumbers = append(correctNumbers, numLoc.value)
		}
	}
	return aoc.Sum(correctNumbers)
}

func calc2(filename string) int {
	numLocs, grid := readPuzzleInput(filename)
	gears := grid.FindAll(func(x, y int, r rune) bool { return r == '*' })
	var result int
	for _, gear := range gears {
		gearPower := 1
		nbEngineParts := 0
		for _, numLoc := range numLocs {
			if numLoc.close(gear.X, gear.Y) {
				nbEngineParts++
				gearPower *= numLoc.value
			}
		}
		if nbEngineParts == 2 {
			result += gearPower
		}
	}
	return result
}

func main() {
	aoc.CheckEquals(4361, calc("test.txt"))
	aoc.CheckEquals(467835, calc2("test.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
