package main

import (
    "fmt"
    "gitlab.com/Alfred456654/aoc-utils"
    "strconv"
    "strings"
)

func calc(filename string) int {
    instrs := aoc.ReadInputLines(filename)
    posX := 0
    depth := 0
    for _, instr := range instrs {
        chunks := strings.Split(instr, " ")
        nb, err := strconv.Atoi(chunks[1])
        if err != nil {
            panic(err)
        }
        switch chunks[0] {
        case "forward":
            posX += nb
        case "down":
            depth += nb
        case "up":
            depth -= nb
        }
    }
    return posX * depth
}

func calc2(filename string) int {
    instrs := aoc.ReadInputLines(filename)
    posX := 0
    depth := 0
    aim := 0
    for _, instr := range instrs {
        chunks := strings.Split(instr, " ")
        nb, err := strconv.Atoi(chunks[1])
        if err != nil {
            panic(err)
        }
        switch chunks[0] {
        case "forward":
            posX += nb
            depth += aim * nb
        case "down":
            aim += nb
        case "up":
            aim -= nb
        }
    }
    return posX * depth
}

func main() {
    aoc.CheckEquals(150, calc("test.txt"))
    aoc.CheckEquals(900, calc2("test.txt"))
    fmt.Println(calc("input.txt"))
    fmt.Println(calc2("input.txt"))
}
