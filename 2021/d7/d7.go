package main

import (
    "fmt"
    "gitlab.com/Alfred456654/aoc-utils"
)

func totalFuel(positions []int, point int, part2 bool) int {
    res := 0
    for _, p := range positions {
        dist := aoc.Abs(p - point)
        if part2 {
            res += dist * (dist + 1) / 2
        } else {
            res += dist
        }
    }
    return res
}

func calc(filename string, part2 bool) int {
    pos := aoc.ReadSimpleList(filename)
    bestPos := int(float64(aoc.Sum(pos)) / float64(len(pos)))
    minFuel := totalFuel(pos, bestPos, part2)
    nextFuel := totalFuel(pos, bestPos+1, part2)
    goUp := nextFuel < minFuel
    for {
        if goUp {
            bestPos++
        } else {
            bestPos--
        }
        nextFuel = totalFuel(pos, bestPos, part2)
        if nextFuel <= minFuel {
            minFuel = nextFuel
        } else {
            break
        }
    }
    return minFuel
}

func main() {
    aoc.CheckEquals(37, calc("test.txt", false))
    aoc.CheckEquals(168, calc("test.txt", true))
    fmt.Println(calc("input.txt", false))
    fmt.Println(calc("input.txt", true))
}
