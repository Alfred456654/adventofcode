package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

func computeCommonBits(lines []string, mostCommon bool) []string {
	transpose := make([][]string, len(lines[0]))
	for i := range lines[0] {
		transpose[i] = make([]string, len(lines))
	}
	for i, l := range lines {
		for j, c := range l {
			transpose[j][i] = string(c)
		}
	}
	cb := make([]string, len(lines[0]))
	for i, bts := range transpose {
		c0 := aoc.Count(bts, "0")
		if (c0 > len(bts)/2.) != mostCommon {
			cb[i] = "1"
		} else {
			cb[i] = "0"
		}
	}
	return cb
}

func calc(filename string) (int64, int64) {
	lines := aoc.ReadInputLines(filename)
	mcb := computeCommonBits(lines, true)
	lcb := computeCommonBits(lines, false)
	gamma, _ := strconv.ParseInt(strings.Join(mcb, ""), 2, 64)
	epsilon, _ := strconv.ParseInt(strings.Join(lcb, ""), 2, 64)
	return gamma, epsilon
}

func calc2(filename string) (int64, int64) {

	inputLines := aoc.ReadInputLines(filename)
	candidatesO := make([]string, len(inputLines))
	candidatesC := make([]string, len(inputLines))
	copy(candidatesO, inputLines)
	copy(candidatesC, inputLines)

	o := identifyCandidate(candidatesO, true)
	c := identifyCandidate(candidatesC, false)
	return o, c
}

func identifyCandidate(candidates []string, keepMostCommon bool) int64 {
	col := 0
	for len(candidates) > 1 {
		expected := computeCommonBits(candidates, keepMostCommon)
		for idx := len(candidates) - 1; idx >= 0; idx-- {
			line := candidates[idx]
			if expected[col] != string(line[col]) {
				candidates = aoc.DelFirstVal(line, candidates)
				continue
			}
		}
		col++
	}
	result, _ := strconv.ParseInt(candidates[0], 2, 64)
	return result
}

func main() {
	g, e := calc("test.txt")
	if g != 22 || e != 9 {
		panic(fmt.Errorf("expected 22, 9, got %d, %d", g, e))
	}
	g, e = calc("input.txt")
	fmt.Println(g * e)

	o, c := calc2("test.txt")
	if o != 23 || c != 10 {
		panic(fmt.Errorf("expected 23, 10, got %d, %d", o, c))
	}
	o, c = calc2("input.txt")
	fmt.Println(o * c)
}
