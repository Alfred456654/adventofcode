package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"strconv"
	"strings"
)

func readNumbers(line string) []int {
	line = strings.ReplaceAll(line, "  ", " ")
	chunks := strings.Split(line, " ")
	result := make([]int, len(chunks))
	for i, c := range chunks {
		i64, _ := strconv.ParseInt(c, 10, 64)
		result[i] = int(i64)
	}
	return result
}

func readInput(filename string) ([]int, [][][]int, [][][]int) {
	blocks := aoc.ReadByBlocks(filename)
	randomOrderStr := blocks[0][0]
	randomOrderStrs := strings.Split(randomOrderStr, ",")
	randomOrder := make([]int, len(randomOrderStrs))
	for i, s := range randomOrderStrs {
		i64, _ := strconv.ParseInt(s, 10, 64)
		randomOrder[i] = int(i64)
	}
	allGrids := make([][][]int, len(blocks)-1)
	allCards := make([][][]int, len(blocks)-1)
	for gridIdx, gridLines := range blocks {
		if gridIdx > 0 {
			allGrids[gridIdx-1] = make([][]int, len(gridLines))
			allCards[gridIdx-1] = make([][]int, len(gridLines))
			for rowInGrid, oneLine := range gridLines {
				numbers := readNumbers(oneLine)
				allGrids[gridIdx-1][rowInGrid] = make([]int, len(numbers))
				allCards[gridIdx-1][rowInGrid] = make([]int, len(numbers))
				copy(allGrids[gridIdx-1][rowInGrid], numbers)
			}
		}
	}
	return randomOrder, allGrids, allCards
}

func findValue(grid [][]int, value int) (int, int, bool) {
	for row, nbrs := range grid {
		for col, nbr := range nbrs {
			if nbr == value {
				return row, col, true
			}
		}
	}
	return 0, 0, false
}

func checkCard(card [][]int) bool {
	for _, rowNbrs := range card {
		if aoc.ListMin(rowNbrs) == 1 {
			return true
		}
	}
	for colIdx := 0; colIdx < len(card[0]); colIdx++ {
		rowIdx := 0
		for card[rowIdx][colIdx] == 1 && rowIdx < len(card)-1 {
			rowIdx++
		}
		if card[rowIdx][colIdx] == 1 && rowIdx == len(card)-1 {
			return true
		}
	}
	return false
}

func computeRemaining(grid, card [][]int) int {
	var result int
	for rowIdx, nbrs := range grid {
		for colIdx, nbr := range nbrs {
			if card[rowIdx][colIdx] == 0 {
				result += nbr
			}
		}
	}
	return result
}

func calc(filename string) int {
	randomOrder, allGrids, allCards := readInput(filename)
	for _, nbr := range randomOrder {
		for gridNb, grid := range allGrids {
			row, col, found := findValue(grid, nbr)
			if found {
				allCards[gridNb][row][col] = 1
				if checkCard(allCards[gridNb]) {
					remain := computeRemaining(grid, allCards[gridNb])
					return nbr * remain
				}
			}
		}
	}
	return 0
}

func calc2(filename string) int {
	randomOrder, allGrids, allCards := readInput(filename)
	for _, nbr := range randomOrder {
		for gridNb := len(allGrids) - 1; gridNb >= 0; gridNb-- {
			grid := allGrids[gridNb]
			row, col, found := findValue(grid, nbr)
			if found {
				card := allCards[gridNb]
				card[row][col] = 1
				if checkCard(card) {
					allGrids = append(allGrids[:gridNb], allGrids[gridNb+1:]...)
					allCards = append(allCards[:gridNb], allCards[gridNb+1:]...)
					if len(allGrids) == 0 {
						remain := computeRemaining(grid, card)
						return nbr * remain
					}
				}
			}
		}
	}
	return 0
}

func main() {
	aoc.CheckEquals(4512, calc("test.txt"))
	aoc.CheckEquals(1924, calc2("test.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
