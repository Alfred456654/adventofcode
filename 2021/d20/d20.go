package main

import (
    "fmt"
    "gitlab.com/Alfred456654/aoc-utils"
)

const dimIncrement = 2

func asciiToBytes(inStr string) []byte {
    result := make([]byte, len(inStr))
    for i, c := range inStr {
        result[i] = byte((46 - c) / 11)
    }
    return result
}

func readInput(filename string) ([]byte, map[aoc.Tuple]byte, int, int, int, int) {
    blocks := aoc.ReadByBlocks(filename)
    algo := asciiToBytes(blocks[0][0])
    inputImage := make(map[aoc.Tuple]byte)
    var xMax int
    yMax := len(blocks[1])
    for row, lineStr := range blocks[1] {
        toBytes := asciiToBytes(lineStr)
        xMax = len(toBytes)
        for col := range toBytes {
            inputImage[aoc.Tuple{X: col, Y: row}] = toBytes[col]
        }
    }
    return algo, inputImage, 0, xMax, 0, yMax
}

func transformPixel(algo []byte, inputImage map[aoc.Tuple]byte, coords aoc.Tuple) byte {
    binNumber := 0
    for y := coords.Y - 1; y <= coords.Y+1; y++ {
        for x := coords.X - 1; x <= coords.X+1; x++ {
            binNumber <<= 1
            binNumber += int(inputImage[aoc.Tuple{X: x, Y: y}])
        }
    }
    return algo[binNumber]
}

func applyAlgo(algo []byte, inputImage map[aoc.Tuple]byte, xMin, xMax, yMin, yMax int) map[aoc.Tuple]byte {
    outputImage := make(map[aoc.Tuple]byte)
    for y := yMin; y <= yMax; y++ {
        for x := xMin; x <= xMax; x++ {
            coords := aoc.Tuple{X: x, Y: y}
            pixel := transformPixel(algo, inputImage, coords)
            outputImage[coords] = pixel
        }
    }
    return outputImage
}

func enhanceTwice(algo []byte, inputImage map[aoc.Tuple]byte, xMin, xMax, yMin, yMax int) map[aoc.Tuple]byte {
    return applyAlgo(
        algo,
        applyAlgo(algo, inputImage, xMin-2*dimIncrement, xMax+2*dimIncrement, yMin-2*dimIncrement, yMax+2*dimIncrement),
        xMin-dimIncrement,
        xMax+dimIncrement,
        yMin-dimIncrement,
        yMax+dimIncrement,
    )
}

func countPixelsOn(image map[aoc.Tuple]byte) int {
    result := 0
    for _, val := range image {
        result += int(val)
    }
    return result
}

func calc(filename string) (int, int) {
    algo, inputImage, xMin, xMax, yMin, yMax := readInput(filename)
    currentImage := enhanceTwice(algo, inputImage, xMin, xMax, yMin, yMax)
    part1 := countPixelsOn(currentImage)
    for i := 0; i < 24; i++ {
        xMin -= dimIncrement
        xMax += dimIncrement
        yMin -= dimIncrement
        yMax += dimIncrement
        currentImage = enhanceTwice(algo, currentImage, xMin, xMax, yMin, yMax)
    }
    return part1, countPixelsOn(currentImage)
}

func main() {
    t1, t2 := calc("test.txt")
    aoc.CheckEquals(35, t1)
    aoc.CheckEquals(3351, t2)
    fmt.Println(calc("input.txt"))
}
