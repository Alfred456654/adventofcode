package main

import (
    "fmt"
    "gitlab.com/Alfred456654/aoc-utils"
)

var (
    fourDirs = []aoc.FastTuple{
        aoc.NewFastTuple(-1, 0, 1000),
        aoc.NewFastTuple(1, 0, 1000),
        aoc.NewFastTuple(0, -1, 1000),
        aoc.NewFastTuple(0, 1, 1000),
    }
    destination aoc.FastTuple
)

func readInput(filename string) (map[aoc.FastTuple]int, int) {
    riskMap := make(map[aoc.FastTuple]int)
    rectangle := aoc.ReadIntRectangle(filename)
    for rowNb, numbers := range rectangle {
        for colNb, risk := range numbers {
            riskMap[aoc.NewFastTuple(colNb, rowNb, 1000)] = risk
        }
    }
    dim := len(rectangle)
    destination = aoc.NewFastTuple(dim, dim, 1000)
    return riskMap, dim
}

func readInput2(filename string) (map[aoc.FastTuple]int, int) {
    riskMap := make(map[aoc.FastTuple]int)
    rectangle := aoc.ReadIntRectangle(filename)
    smallGridDim := len(rectangle)
    bigGrid := make([][]int, 5*smallGridDim)
    for rowNb := range bigGrid {
        bigGrid[rowNb] = make([]int, 5*smallGridDim)
        for colNb := 0; colNb < 5*smallGridDim; colNb++ {
            newValue := rectangle[(rowNb % smallGridDim)][(colNb%smallGridDim)] + rowNb/smallGridDim + colNb/smallGridDim
            if newValue > 9 {
                newValue -= 9
            }
            bigGrid[rowNb][colNb] = newValue
        }
    }
    for rowNb, numbers := range bigGrid {
        for colNb, risk := range numbers {
            riskMap[aoc.NewFastTuple(colNb, rowNb, 1000)] = risk
        }
    }
    return riskMap, 5 * len(rectangle)
}

func getNeighbours(riskMap map[aoc.FastTuple]int, pos aoc.FastTuple) []aoc.FastTuple {
    result := make([]aoc.FastTuple, 0)
    for _, d := range fourDirs {
        n := pos.Add(d)
        _, found := riskMap[n]
        if found {
            result = append(result, n)
        }
    }
    return result
}

func minByValue(scores map[aoc.FastTuple]int, toVisit map[aoc.FastTuple]struct{}) aoc.FastTuple {
    var minPos aoc.FastTuple
    minDist := -1
    for pos := range toVisit {
        d, exists := scores[pos]
        if exists && (d < minDist || minDist < 0) {
            minDist = d
            minPos = pos
        }
    }
    return minPos
}

func calc(filename string, part2 bool) int {
    var riskMap map[aoc.FastTuple]int
    var dim int
    if !part2 {
        riskMap, dim = readInput(filename)
    } else {
        riskMap, dim = readInput2(filename)
    }
    destination = aoc.NewFastTuple(dim-1, dim-1, 1000)
    toVisit := map[aoc.FastTuple]struct{}{aoc.NewFastTuple(0, 0, 1000): {}}
    distances := map[aoc.FastTuple]int{aoc.NewFastTuple(0, 0, 1000): 0}
    precedence := make(map[aoc.FastTuple]aoc.FastTuple)
    for len(toVisit) > 0 {
        pos := minByValue(distances, toVisit)
        if pos.Equals(destination) {
            return distances[pos]
        }
        delete(toVisit, pos)
        for _, n := range getNeighbours(riskMap, pos) {
            _, distExists := distances[n]
            if !distExists {
                distances[n] = distances[pos] + 10
                toVisit[n] = struct{}{}
            }
            alt := distances[pos] + riskMap[n]
            if alt < distances[n] {
                distances[n] = alt
                precedence[n] = pos
                toVisit[n] = struct{}{}
            }
        }
    }
    return distances[destination]
}

func main() {
    aoc.CheckEquals(40, calc("test.txt", false))
    aoc.CheckEquals(315, calc("test.txt", true))
    fmt.Println(calc("input.txt", false))
    fmt.Println(calc("input.txt", true))
}
