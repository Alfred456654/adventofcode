package main

import (
    "fmt"
    "gitlab.com/Alfred456654/aoc-utils"
)

func calc(filename string, sampling int) int {
    input := aoc.ReadInputIntegers(filename)
    previous := -1
    nbInc := -1
    for i := 0; i < len(input)-sampling + 1; i++ {
        v := 0
        for j := 0; j < sampling; j++ {
            v += input[i+j]
        }
        if v > previous {
            nbInc++
        }
        previous = v
    }
    return nbInc
}

func main() {
    aoc.CheckEquals(7, calc("test.txt", 1))
    aoc.CheckEquals(5, calc("test.txt", 3))
    fmt.Println(calc("input.txt", 1))
    fmt.Println(calc("input.txt", 3))
}
