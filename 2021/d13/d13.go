package main

import (
    "fmt"
    "gitlab.com/Alfred456654/aoc-utils"
    "strconv"
    "strings"
)

type foldDirection int

const (
    horizontal foldDirection = iota
    vertical
)

type fold struct {
    direction foldDirection
    index     int
}

var dimensions map[string]int

func printDots(dots *aoc.TupleSet) {
    for y := dimensions["ymin"]; y <= dimensions["ymax"]; y++ {
        for x := dimensions["xmin"]; x <= dimensions["xmax"]; x++ {
            if dots.Contains(aoc.Tuple{X: x, Y: y}) {
                fmt.Printf("#")
            } else {
                fmt.Printf(".")
            }
        }
        fmt.Println()
    }
    fmt.Println()
}

func readDots(lines []string) (dots *aoc.TupleSet) {
    dots = aoc.NewTupleSet()
    for _, l := range lines {
        coords := strings.Split(l, ",")
        x, _ := strconv.Atoi(coords[0])
        y, _ := strconv.Atoi(coords[1])
        dots.Add(aoc.Tuple{X: x, Y: y})
    }
    return
}

func readFolds(instructions []string) (result []fold) {
    result = make([]fold, len(instructions))
    for i, instr := range instructions {
        params := strings.Split(strings.Split(instr, " ")[2], "=")
        foldIdx, _ := strconv.Atoi(params[1])
        foldDir := horizontal
        if params[0] == "x" {
            foldDir = vertical
            if dimensions["xmax"] == 0 {
                dimensions["xmax"] = 2 * foldIdx
            }
        } else if dimensions["ymax"] == 0 {
            dimensions["ymax"] = 2 * foldIdx
        }
        result[i] = fold{
            direction: foldDir,
            index:     foldIdx,
        }
    }
    return
}

func readInput(filename string) (dots *aoc.TupleSet, folds []fold) {
    blocks := aoc.ReadByBlocks(filename)
    dots = readDots(blocks[0])
    folds = readFolds(blocks[1])
    return
}

func calc(filename string) int {
    dimensions = map[string]int{
        "xmin": 0,
        "xmax": 0,
        "ymin": 0,
        "ymax": 0,
    }
    dots, folds := readInput(filename)
    foundFirst := false
    var part1 int
    for _, foldParams := range folds {
        switch foldParams.direction {
        case horizontal: // y = n
            for dotCoords := range dots.Iterate() {
                if dotCoords.Y > foldParams.index {
                    dots.Add(aoc.Tuple{X: dotCoords.X, Y: dimensions["ymax"] - dotCoords.Y})
                    dots.Remove(dotCoords)
                }
                if dotCoords.Y == foldParams.index {
                    dots.Remove(dotCoords)
                }
            }
            dimensions["ymax"] = (dimensions["ymax"] - 1) / 2
        case vertical: // x = n
            for dotCoords := range dots.Iterate() {
                if dotCoords.X > foldParams.index {
                    dots.Add(aoc.Tuple{X: dimensions["xmax"] - dotCoords.X, Y: dotCoords.Y})
                    dots.Remove(dotCoords)
                }
                if dotCoords.X == foldParams.index {
                    dots.Remove(dotCoords)
                }
            }
            dimensions["xmax"] = (dimensions["xmax"] - 1) / 2
        }
        if !foundFirst {
            foundFirst = true
            part1 = dots.Size()
        }
    }
    printDots(dots)
    return part1
}

func main() {
    aoc.CheckEquals(17, calc("test.txt"))
    fmt.Println(calc("input.txt"))
}
