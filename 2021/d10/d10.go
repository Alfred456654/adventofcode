package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"sort"
)

var (
	scores1 = map[string]int{
		")": 3,
		"]": 57,
		"}": 1197,
		">": 25137,
	}
	scores2 = map[string]int{
		")": 1,
		"]": 2,
		"}": 3,
		">": 4,
	}
	matching = map[string]string{
		"(": ")",
		"[": "]",
		"{": "}",
		"<": ">",
	}
	openings = []string{"(", "[", "{", "<"}
)

func scoreRemain(endsExpected []string) int {
	result := 0
	l := len(endsExpected)
	for i := l - 1; i >= 0; i-- {
		result *= 5
		chr := endsExpected[i]
		result += scores2[chr]
	}
	return result
}

func scoreLine(line string) (int, int) {
	endsExpected := make([]string, 0)
	for _, c := range line {
		chr := string(c)
		if aoc.Contains(openings, chr) {
			endsExpected = append(endsExpected, matching[chr])
		} else {
			expectedEnd := endsExpected[len(endsExpected)-1]
			if chr != expectedEnd {
				return scores1[chr], 0
			}
			endsExpected = endsExpected[:len(endsExpected)-1]
		}
	}
	return 0, scoreRemain(endsExpected)
}

func calc(filename string) (int, int) {
	lines := aoc.ReadInputLines(filename)
	result1 := 0
	results2 := make([]int, 0)
	for _, l := range lines {
		sc1, sc2 := scoreLine(l)
		result1 += sc1
		if sc2 != 0 {
			results2 = append(results2, sc2)
		}
	}
	sort.Ints(results2)
	return result1, results2[(len(results2) / 2)]
}

func main() {
	t1, t2 := calc("test.txt")
	aoc.CheckEquals(26397, t1)
	aoc.CheckEquals(288957, t2)
	fmt.Println(calc("input.txt"))
}
