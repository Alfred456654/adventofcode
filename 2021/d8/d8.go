package main

import (
	"fmt"
	"gitlab.com/Alfred456654/aoc-utils"
	"math"
	"sort"
	"strings"
)

var segmLetters []string
var digitsToSegments = map[int]string{
	2: "acdeg",
	3: "acdfg",
	5: "abdfg",
	6: "abdefg",
	9: "abcdfg",
	0: "abcefg",
}

func readInput(filename string) ([][]string, [][]string) {
	patternsAndDisplays := aoc.ReadInputLines(filename)
	patterns := make([][]string, len(patternsAndDisplays))
	displays := make([][]string, len(patternsAndDisplays))
	for idx, pad := range patternsAndDisplays {
		chunks := strings.Split(pad, " | ")
		patterns[idx] = strings.Split(chunks[0], " ")
		displays[idx] = strings.Split(chunks[1], " ")
	}
	return patterns, displays
}

func calc(filename string) int {
	matchingLengths := []int{2, 3, 4, 7}
	_, displays := readInput(filename)
	res := 0
	for _, disp := range displays {
		for _, d := range disp {
			if aoc.Contains(matchingLengths, len(d)) {
				res++
			}
		}
	}
	return res
}

func stringDiff(a, b string) string {
	if len(a) > len(b) {
		a, b = b, a
	}
	for _, c := range b {
		chr := string(c)
		if !strings.Contains(a, chr) {
			return chr
		}
	}
	return ""
}

func sortString(a string) string {
	ascii := make([]int, len(a))
	for i, c := range a {
		ascii[i] = int(c)
	}
	sort.Ints(ascii)
	chrs := make([]rune, len(ascii))
	for i, c := range ascii {
		chrs[i] = rune(c)
	}
	return string(chrs)
}

func identify1478(patterns []string) (map[string]int, []string) {
	totalAppearances := make(map[string]int) // number of times each segment is lit up when writing each number from 0-9
	digits := make([]string, 10)
	for _, p := range patterns {
		switch len(p) {
		case 2:
			digits[1] = sortString(p)
		case 3:
			digits[7] = sortString(p)
		case 4:
			digits[4] = sortString(p)
		case 7:
			digits[8] = sortString(p)
		}
		for _, segmLetter := range segmLetters {
			totalAppearances[segmLetter] += strings.Count(p, segmLetter)
		}
	}
	return totalAppearances, digits
}

func transformString(a string, segmentsMapping map[string]string) string {
	var result string
	for _, c := range a {
		result += segmentsMapping[string(c)]
	}
	return sortString(result)
}

func isSubset(a, b string) bool {
	chrsA := strings.Split(a, "")
	setA := aoc.NewSet[string]()
	setA.AddAll(chrsA)
	chrsB := strings.Split(b, "")
	setB := aoc.NewSet[string]()
	setB.AddAll(chrsB)
	inter := setB.Intersection(setA)
	return inter.Size() == setA.Size()
}

func identifyDisplaySegments(patterns []string, digits []string, totalAppearances map[string]int) map[string]string {
	segmentsMapping := make(map[string]string)

	// identify segment a: the only difference between 1 and 7
	segmentsMapping["a"] = stringDiff(digits[1], digits[7])
	segmLetters = aoc.DelFirstVal(segmentsMapping["a"], segmLetters)

	// identify segments c, b, e, f
	for letter, nbAppearances := range totalAppearances {
		switch nbAppearances {
		case 4: // the only one lit up 4 times when writing 0-9 is bottom-left
			segmentsMapping["e"] = letter
			segmLetters = aoc.DelFirstVal(segmentsMapping["e"], segmLetters)
		case 6: // the only one lit up 6 times when writing 0-9 is top-left
			segmentsMapping["b"] = letter
			segmLetters = aoc.DelFirstVal(segmentsMapping["b"], segmLetters)
		case 8: // the only ones lit up 8 times when writing 0-9 are top and top-right, we already know the top one
			if letter != segmentsMapping["a"] {
				segmentsMapping["c"] = letter
				segmLetters = aoc.DelFirstVal(segmentsMapping["c"], segmLetters)
			}
		case 9: // the only one lit up 9 times when writing 0-9 is bottom-right
			segmentsMapping["f"] = letter
			segmLetters = aoc.DelFirstVal(segmentsMapping["f"], segmLetters)
		}
	}

	// identify segment g: the only segment from 0 that we don't know is that one
	for _, p := range patterns {
		knownPortionOf0 := transformString("abcef", segmentsMapping)
		if len(p) == 6 {
			sortedPattern := sortString(p)
			if isSubset(knownPortionOf0, sortedPattern) {
				segmentsMapping["g"] = stringDiff(knownPortionOf0, sortedPattern)
				segmLetters = aoc.DelFirstVal(segmentsMapping["g"], segmLetters)
			}
		}
	}

	// last one is d
	segmentsMapping["d"] = segmLetters[0]
	segmLetters = aoc.DelFirstVal(segmentsMapping["d"], segmLetters)
	return segmentsMapping
}

func identify235690(patterns, digits []string, segmentsMapping map[string]string) []string {
	for digit, segments := range digitsToSegments {
		modifiedSegm := transformString(segments, segmentsMapping)
		for _, p := range patterns {
			if len(p) == len(modifiedSegm) {
				sortedPattern := sortString(p)
				if modifiedSegm == sortedPattern {
					digits[digit] = sortedPattern
				}
			}
		}
	}
	return digits
}

func readDisplay(display []string, digits []string) int {
	result := 0
	for order, pattern := range display {
		orderedNb := sortString(pattern)
		for digit, digitSegments := range digits {
			if orderedNb == digitSegments {
				result += int(math.Pow(10, float64(len(display)-1-order))) * digit
			}
		}
	}
	return result
}

func solveDisplay(patterns, display []string) int {
	segmLetters = []string{"a", "b", "c", "d", "e", "f", "g"}
	totalAppearances, digits := identify1478(patterns)
	segmentsMapping := identifyDisplaySegments(patterns, digits, totalAppearances)
	digits = identify235690(patterns, digits, segmentsMapping)
	return readDisplay(display, digits)
}

func calc2(filename string) int {
	patterns, displays := readInput(filename)
	result := 0
	for idx := 0; idx < len(patterns); idx++ {
		result += solveDisplay(patterns[idx], displays[idx])
	}
	return result
}

func main() {
	aoc.CheckEquals(26, calc("test.txt"))
	aoc.CheckEquals(5353, calc2("minitest.txt"))
	aoc.CheckEquals(61229, calc2("test.txt"))
	fmt.Println(calc("input.txt"))
	fmt.Println(calc2("input.txt"))
}
