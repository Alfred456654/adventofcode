package main

import (
    "fmt"
    "gitlab.com/Alfred456654/aoc-utils"
    "strconv"
    "strings"
)

var tests = map[string]int{
    "D2FE28":                         6,
    "38006F45291200":                 9,
    "8A004A801A8002F478":             16,
    "620080001611562C8802118E34":     12,
    "C0015000016115A2E0802F182340":   23,
    "A0016C880162017C3686B18A3D4780": 31,
}

var tests2 = map[string]int{
    "C200B40A82":                 3,
    "04005AC33890":               54,
    "880086C3E88112":             7,
    "CE00C43D881120":             9,
    "D8005AC2A8F0":               1,
    "F600BC2D8F":                 0,
    "9C005AC2F8F0":               0,
    "9C0141080250320F1802104A08": 1,
}

func hexStrToBinArray(hexStr string) []byte {
    binStrBuild := strings.Builder{}
    for _, h := range hexStr {
        nbr, _ := strconv.ParseInt(string(h), 16, 64)
        binStrBuild.WriteString(fmt.Sprintf("%04b", nbr))
    }
    binStr := binStrBuild.String()
    binArray := make([]byte, len(binStr))
    for i, s := range binStr {
        binArray[i] = byte(s - 48)
    }
    return binArray
}

func bytesToInt(byteArray []byte) int {
    var result int
    for _, b := range byteArray {
        result <<= 1
        result += int(b)
    }
    return result
}

func readSimpleBinary(binArray []byte) ([]byte, int) {
    labelIdx := 0
    result := 0
    for binArray[labelIdx] == 1 {
        result <<= 4
        result += bytesToInt(binArray[labelIdx+1 : labelIdx+5])
        labelIdx += 5
    }
    result <<= 4
    result += bytesToInt(binArray[labelIdx+1 : labelIdx+5])
    return binArray[labelIdx+5:], result
}

func boolToInt(val bool) int {
    if val {
        return 1
    }
    return 0
}

func applyOperation(typeId int, values []int) int {
    switch typeId {
    case 0:
        return aoc.Sum(values)
    case 1:
        res := 1
        for _, v := range values {
            res *= v
        }
        return res
    case 2:
        return aoc.ListMin(values)
    case 3:
        return aoc.ListMax(values)
    case 5:
        return boolToInt(values[0] > values[1])
    case 6:
        return boolToInt(values[0] < values[1])
    case 7:
        return boolToInt(values[0] == values[1])
    default:
        panic(fmt.Sprintf("unknown typeId: %d", typeId))
    }
}

func parseOperator(typeId int, binArray []byte) ([]byte, int, int) {
    lengthTypeId := binArray[0]
    var versionSum, version, newValue int
    valuesRead := make([]int, 0)
    switch lengthTypeId {
    case 0:
        lengthBytes := bytesToInt(binArray[1:16])
        binArray = binArray[16:]
        expectedLengthAtEnd := len(binArray) - lengthBytes
        for len(binArray) > expectedLengthAtEnd {
            binArray, version, newValue = parsePacket(binArray)
            versionSum += version
            valuesRead = append(valuesRead, newValue)
        }
    case 1:
        nbPackets := bytesToInt(binArray[1:12])
        binArray = binArray[12:]
        for n := 0; n < nbPackets; n++ {
            binArray, version, newValue = parsePacket(binArray)
            versionSum += version
            valuesRead = append(valuesRead, newValue)
        }
    }
    valueRead := applyOperation(typeId, valuesRead)
    return binArray, versionSum, valueRead
}

func parsePacket(binArray []byte) ([]byte, int, int) {
    var versionSum, valueRead int
    versionSum += bytesToInt(binArray[:3])
    typeId := bytesToInt(binArray[3:6])
    binArray = binArray[6:]
    if typeId == 4 {
        binArray, valueRead = readSimpleBinary(binArray)
    } else {
        var version int
        binArray, version, valueRead = parseOperator(typeId, binArray)
        versionSum += version
    }
    return binArray, versionSum, valueRead
}

func calc(hexStr string) (int, int) {
    binArray := hexStrToBinArray(hexStr)
    _, sum, value := parsePacket(binArray)
    return sum, value
}

func main() {
    for hexStr, sum := range tests {
        t1, _ := calc(hexStr)
        aoc.CheckEqualsMsg(sum, t1, hexStr)
    }
    for hexStr, val := range tests2 {
        _, t2 := calc(hexStr)
        aoc.CheckEqualsMsg(val, t2, hexStr)
    }
    myInput := aoc.ReadInputLines("input.txt")[0]
    fmt.Println(calc(myInput))
}
