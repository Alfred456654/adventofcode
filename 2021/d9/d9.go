package main

import (
    "fmt"
    "gitlab.com/Alfred456654/aoc-utils"
    "sort"
)

var (
    up    = aoc.Tuple{X: 0, Y: -1}
    down  = aoc.Tuple{X: 0, Y: 1}
    left  = aoc.Tuple{X: -1, Y: 0}
    right = aoc.Tuple{X: 1, Y: 0}
)
var fourPoints = []aoc.Tuple{up, down, left, right}
var visited []aoc.Tuple

func readInput(filename string) (map[aoc.Tuple]int, int, int) {
    result := make(map[aoc.Tuple]int)
    lines := aoc.ReadInputLines(filename)
    for y, l := range lines {
        for x, c := range l {
            result[aoc.Tuple{X: x, Y: y}] = int(c) - 48
        }
    }
    return result, len(lines[0]), len(lines)
}

func scoreLocalMinimum(floor map[aoc.Tuple]int, x int, y int) int {
    here := aoc.Tuple{X: x, Y: y}
    height := floor[here]
    for _, d := range fourPoints {
        nearbyHeight, found := floor[here.Add(d)]
        if found && nearbyHeight <= height {
            return 0
        }
    }
    return height + 1
}

func calc(filename string) int {
    result := 0
    floor, xMax, yMax := readInput(filename)
    for x := 0; x < xMax; x++ {
        for y := 0; y < yMax; y++ {
            result += scoreLocalMinimum(floor, x, y)
        }
    }
    return result
}

func findHigherNeighbours(floor map[aoc.Tuple]int, here aoc.Tuple) []aoc.Tuple {
    height := floor[here]
    higherNeighbours := make([]aoc.Tuple, 0)
    for _, d := range fourPoints {
        neighbour := here.Add(d)
        h, found := floor[neighbour]
        if found && h > height && h < 9 && !aoc.ContainsTuple(visited, neighbour) {
            visited = append(visited, neighbour)
            higherNeighbours = append(higherNeighbours, neighbour)
        }
    }
    return higherNeighbours
}

func iterateBasinScoring(floor map[aoc.Tuple]int, toVisit []aoc.Tuple) int {
    result := 1
    for _, n := range toVisit {
        result += iterateBasinScoring(floor, findHigherNeighbours(floor, n))
    }
    return result
}

func scoreBasin(floor map[aoc.Tuple]int, coordsMin aoc.Tuple) int {
    visited = []aoc.Tuple{coordsMin}
    initialCandidates := findHigherNeighbours(floor, coordsMin)
    result := iterateBasinScoring(floor, initialCandidates)
    return result
}

func calc2(filename string) int {
    minima := make([]aoc.Tuple, 0)
    floor, xMax, yMax := readInput(filename)
    for x := 0; x < xMax; x++ {
        for y := 0; y < yMax; y++ {
            score := scoreLocalMinimum(floor, x, y)
            if score > 0 {
                minima = append(minima, aoc.Tuple{X: x, Y: y})
            }
        }
    }
    l := len(minima)
    basinSizes := make([]int, l)
    for idx, coordsMin := range minima {
        basinSizes[idx] = scoreBasin(floor, coordsMin)
    }
    sort.Ints(basinSizes)
    return basinSizes[l-1] * basinSizes[l-2] * basinSizes[l-3]
}

func main() {
    aoc.CheckEquals(15, calc("test.txt"))
    aoc.CheckEquals(1134, calc2("test.txt"))
    fmt.Println(calc("input.txt"))
    fmt.Println(calc2("input.txt"))
}
