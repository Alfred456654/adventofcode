package main

import (
    "fmt"
    "gitlab.com/Alfred456654/aoc-utils"
)

var eightCoordsAround = []aoc.Tuple{
    {X: -1, Y: -1},
    {X: 0, Y: -1},
    {X: 1, Y: -1},
    {X: -1, Y: 0},
    {X: 1, Y: 0},
    {X: -1, Y: 1},
    {X: 0, Y: 1},
    {X: 1, Y: 1},
}

func readInput(filename string) *map[aoc.Tuple]int {
    squidsArray := aoc.ReadIntRectangle(filename)
    squidsMap := make(map[aoc.Tuple]int)
    for row, squids := range squidsArray {
        for col, squid := range squids {
            squidsMap[aoc.Tuple{X: col, Y: row}] = squid
        }
    }
    return &squidsMap
}

func squidsAround(squidsMap *map[aoc.Tuple]int, coord aoc.Tuple, squidsFlashed []aoc.Tuple, squidsToFlash []aoc.Tuple) []aoc.Tuple {
    result := make([]aoc.Tuple, 0)
    for _, nearCoord := range eightCoordsAround {
        neighbourCoords := coord.Add(nearCoord)
        _, exists := (*squidsMap)[neighbourCoords]
        if exists && !aoc.ContainsTuple(squidsFlashed, neighbourCoords) && !aoc.ContainsTuple(squidsToFlash, neighbourCoords) {
            result = append(result, neighbourCoords)
        }
    }
    return result
}

func increasePower(squidsMap *map[aoc.Tuple]int) []aoc.Tuple {
    squidsToFlash := make([]aoc.Tuple, 0)
    for coords := range *squidsMap {
        (*squidsMap)[coords]++
        if (*squidsMap)[coords] > 9 {
            squidsToFlash = append(squidsToFlash, coords)
        }
    }
    return squidsToFlash
}

func timeStep(squidsMap *map[aoc.Tuple]int) int {
    squidsToFlash := increasePower(squidsMap)
    squidsFlashed := make([]aoc.Tuple, 0)
    flashes := 0
    for len(squidsToFlash) > 0 {
        flashes++
        flashingSquid := squidsToFlash[0]
        squidsToFlash = squidsToFlash[1:]
        squidsFlashed = append(squidsFlashed, flashingSquid)
        for _, neighbour := range squidsAround(squidsMap, flashingSquid, squidsFlashed, squidsToFlash) {
            (*squidsMap)[neighbour]++
            if (*squidsMap)[neighbour] > 9 {
                squidsToFlash = append(squidsToFlash, neighbour)
            }
        }
    }
    for _, coords := range squidsFlashed {
        (*squidsMap)[coords] = 0
    }
    return flashes
}

func calc(filename string) (int, int) {
    squidsMap := readInput(filename)
    var it, totalFlashes int
    for {
        it++
        flashes := timeStep(squidsMap)
        if flashes == 100 {
            return totalFlashes, it
        }
        if it <= 100 {
            totalFlashes += flashes
        }
    }
}

func main() {
    t1, t2 := calc("test.txt")
    aoc.CheckEquals(1656, t1)
    aoc.CheckEquals(195, t2)
    fmt.Println(calc("input.txt"))
}
