package main

import (
    "fmt"
    "gitlab.com/Alfred456654/aoc-utils"
)

func readInput(filename string) []int {
    result := make([]int, 9)
    fish := aoc.ReadSimpleList(filename)
    for _, f := range fish {
        result[f]++
    }
    return result
}

func calc(filename string, days int) int {
    fishAge := readInput(filename)
    var d int
    for d = 0; d < days-7; d += 7 {
        f0 := fishAge[7]
        f1 := fishAge[8]
        fishAge[7] = 0
        fishAge[8] = 0
        for i := 6; i >= 0; i-- {
            fishAge[i+2] += fishAge[i]
        }
        fishAge[0] += f0
        fishAge[1] += f1
    }
    for ; d < days; d++ {
        newBorns := fishAge[0]
        for age := 1; age < 9; age++ {
            fishAge[age-1] = fishAge[age]
        }
        fishAge[6] += newBorns
        fishAge[8] = newBorns
    }
    return aoc.Sum(fishAge)
}

func main() {
    aoc.CheckEquals(26, calc("test.txt", 18))
    aoc.CheckEquals(5934, calc("test.txt", 80))
    aoc.CheckEquals(26984457539, calc("test.txt", 256))
    fmt.Println(calc("input.txt", 80))
    fmt.Println(calc("input.txt", 256))
}
