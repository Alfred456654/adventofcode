package main

import (
    "fmt"
    "gitlab.com/Alfred456654/aoc-utils"
    "strconv"
    "strings"
)

type coord struct {
    x int
    y int
}

type grid struct {
    cells map[coord]int
}

func newGrid() grid {
    result := make(map[coord]int)
    return grid{cells: result}
}

func (g *grid) add(x, y int) {
    g.cells[coord{x, y}]++
}

func (g *grid) countAtLeast(val int) int {
    var result int
    for _, v := range g.cells {
        if v >= val {
            result++
        }
    }
    return result
}

func parseLine(line string) (int, int, int, int) {
    vals := strings.Split(strings.ReplaceAll(line, " -> ", ","), ",")
    result := make([]int, 4)
    for i := 0; i < 4; i++ {
        i64, _ := strconv.ParseInt(vals[i], 10, 64)
        result[i] = int(i64)
    }
    return result[0], result[1], result[2], result[3]
}

func readInput(filename string) (grid, grid) {
    vents := newGrid()
    vents2 := newGrid()
    lines := aoc.ReadInputLines(filename)
    for _, lineStr := range lines {
        x1, y1, x2, y2 := parseLine(lineStr)
        switch {
        case x1 != x2 && y1 == y2:
            for x := aoc.Min(x1, x2); x <= aoc.Max(x1, x2); x++ {
                vents.add(x, y1)
                vents2.add(x, y1)
            }
        case x1 == x2 && y1 != y2:
            for y := aoc.Min(y1, y2); y <= aoc.Max(y1, y2); y++ {
                vents.add(x1, y)
                vents2.add(x1, y)
            }
        case aoc.Abs(x1-x2) == aoc.Abs(y1-y2):
            tx := (x2 - x1) / aoc.Abs(x2-x1)
            ty := (y2 - y1) / aoc.Abs(y2-y1)
            for t := 0; t <= aoc.Abs(x1-x2); t++ {
                vents2.add(x1+t*tx, y1+t*ty)
            }
        }
    }
    return vents, vents2
}

func calc(filename string) (int, int) {
    g, g2 := readInput(filename)
    return g.countAtLeast(2), g2.countAtLeast(2)
}

func main() {
    t1, t2 := calc("test.txt")
    aoc.CheckEquals(5, t1)
    aoc.CheckEquals(12, t2)
    res, res2 := calc("input.txt")
    fmt.Println(res, res2)
}
