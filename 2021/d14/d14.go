package main

import (
    "fmt"
    "gitlab.com/Alfred456654/aoc-utils"
    "strings"
)

func polymerToPairs(polymer string) map[string]int {
    pairs := make(map[string]int)
    for idx := 0; idx < len(polymer)-1; idx++ {
        pair := polymer[idx : idx+2]
        pairs[pair]++
    }
    return pairs
}

func readInput(filename string) (map[string]int, map[string][]string, uint8, uint8) {
    blocks := aoc.ReadByBlocks(filename)
    polymerStr := blocks[0][0]
    firstElement := polymerStr[0]
    lastElement := polymerStr[len(polymerStr)-1]
    polymer := polymerToPairs(polymerStr)
    rules := make(map[string][]string)
    for _, line := range blocks[1] {
        chunks := strings.Split(line, " ")
        firstPair := chunks[0]
        s0 := strings.Join([]string{string(firstPair[0]), chunks[2]}, "")
        s1 := strings.Join([]string{chunks[2], string(firstPair[1])}, "")
        rules[firstPair] = []string{s0, s1}
    }
    return polymer, rules, firstElement, lastElement
}

func step(polymer map[string]int, rules map[string][]string) map[string]int {
    newPolymer := make(map[string]int)
    for pair, nb := range polymer {
        for _, p := range rules[pair] {
            newPolymer[p] += nb
        }
    }
    return newPolymer
}

func leastAndMostFrequent(polymer map[string]int, first, last uint8) int {
    freq := make(map[uint8]int)
    freq[first] = 1
    freq[last] = 1
    for pair, nb := range polymer {
        p0 := pair[0]
        p1 := pair[1]
        freq[p0] += nb
        freq[p1] += nb
    }
    s := 0
    for _, n := range polymer {
        s += n
    }
    min := s
    max := 0
    for _, n := range freq {
        min = aoc.Min(n, min)
        max = aoc.Max(n, max)
    }
    return (max - min) / 2
}

func calc(filename string) (int, int) {
    polymer, rules, first, last := readInput(filename)
    for st := 0; st < 10; st++ {
        polymer = step(polymer, rules)
    }
    part1 := leastAndMostFrequent(polymer, first, last)
    for st := 0; st < 30; st++ {
        polymer = step(polymer, rules)
    }
    part2 := leastAndMostFrequent(polymer, first, last)
    return part1, part2
}

func main() {
    t1, t2 := calc("test.txt")
    aoc.CheckEquals(1588, t1)
    aoc.CheckEquals(2188189693529, t2)
    fmt.Println(calc("input.txt"))
}
