package main

import (
    "fmt"
    "gitlab.com/Alfred456654/aoc-utils"
    "math"
    "regexp"
    "strconv"
    "strings"
)

var rangePattern = regexp.MustCompile("[xy]=([0-9-]*)..([0-9-]*)")

func parseRange(rangeStr string) (int, int) {
    groups := rangePattern.FindStringSubmatch(rangeStr)
    valMin64, _ := strconv.ParseInt(groups[1], 10, 64)
    valMax64, _ := strconv.ParseInt(groups[2], 10, 64)
    return int(valMin64), int(valMax64)
}

func readInput(filename string) (int, int, int, int) {
    line := aoc.ReadInputLines(filename)[0]
    chunks := strings.Split(line, " ")
    xMin, xMax := parseRange(chunks[2])
    yMin, yMax := parseRange(chunks[3])
    return xMin, xMax, yMin, yMax
}

func calc(filename string) int {
    _, _, yMin, _ := readInput(filename)
    return (yMin * (yMin + 1)) / 2
}

func almostTwiceSqrtThing(nb int) int {
    // kind of invert nb = n(n-1)/2
    n := int(math.Sqrt(float64(nb)))
    for n*(n-1) < 2*nb {
        n++
    }
    return n-1
}

func simulate(vx int, vy int, xMin int, xMax int, yMin int, yMax int) int {
    posX, posY := 0, 0
    for posY+vy >= yMin {
        posX += vx
        posY += vy
        vx = aoc.Max(vx-1, 0)
        vy--
        if xMin <= posX && posX <= xMax && yMin <= posY && posY <= yMax {
            return 1
        }
    }
    return 0
}

func calc2(filename string) int {
    xMin, xMax, yMin, yMax := readInput(filename)
    vXmax := xMax
    vXmin := almostTwiceSqrtThing(xMin)
    result := 0
    for vx := vXmin; vx <= vXmax; vx++ {
        for vy := yMin; vy <= 1-yMin; vy++ {
            result += simulate(vx, vy, xMin, xMax, yMin, yMax)
        }
    }
    return result
}

func main() {
    aoc.CheckEquals(45, calc("test.txt"))
    aoc.CheckEquals(112, calc2("test.txt"))
    fmt.Println(calc("input.txt"))
    fmt.Println(calc2("input.txt"))
}
