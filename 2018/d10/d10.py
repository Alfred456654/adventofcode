#!/usr/bin/python

import re

RX = re.compile(r'^position=<(.*),(.*)> velocity=<(.*),(.*)>$')

size = lambda grid: (
        min([k[0] for g in grid for k in g.keys()]),
        max([k[0] for g in grid for k in g.keys()]),
        min([k[1] for g in grid for k in g.keys()]),
        max([k[1] for g in grid for k in g.keys()])
        )


def area(grid):
    s = size(grid)
    return (s[1]-s[0]) * (s[3]-s[2])


def read_input(filename):
    with open(filename, 'r') as of:
        return [{(int(t[0]), int(t[1])): (int(t[2]), int(t[3]))} for t in [RX.match(s).groups() for s in of.readlines()]]


def draw(grid):
    x_min, x_max, y_min, y_max = size(grid)
    print()
    for y in range(y_min, y_max + 1):
        print(''.join(['#' if (x, y) in [list(g.keys())[0] for g in grid] else ' ' for x in range(x_min, x_max + 1)]))
    print()


def move(grid):
    return [{(k[0]+v[0], k[1]+v[1]): v} for g in grid for k, v in g.items()]


def calc(filename):
    seconds = 0
    old_grid = read_input(filename)
    grid = old_grid
    grid_area = area(grid)
    old_grid_area = grid_area + 1
    while old_grid_area > grid_area:
        seconds += 1
        old_grid = grid
        grid = move(grid)
        old_grid_area = grid_area
        grid_area = area(grid)
    draw(old_grid)
    return seconds - 1


if __name__ == '__main__':
    print(f"{calc('test')} seconds")
    print(f"{calc('input')} seconds")
