#!/usr/bin/python

import numpy as np
import multiprocessing as mp

serial = 3613
allpowerlevels = np.array([[int(((x + 10) * y + serial) * (x + 10) / 100) % 10 - 5 for y in range(1, 301)] for x in range(1, 301)])


def square(x, y, s):
    return np.sum([allpowerlevels[x:x+s, y:y+s]])


def calc(s):
    grid = np.ndarray((301 - s, 301 - s), dtype=int)
    for x in range(301 - s):
        grid[x] = [square(x, y, s) for y in range(301 - s)]
    result = np.unravel_index(grid.argmax(), grid.shape)
    return f'coords: {result[0] + 1},{result[1] + 1}', np.max(grid)


def calc2():
    results = {}
    with mp.Pool(mp.cpu_count()) as p:
        results = p.map(calc, range(1, 301))
    high = max(results, key = lambda t: t[1])
    return results.index(high) + 1, high


if __name__ == '__main__':
    print(calc(3))
    print(calc2())
