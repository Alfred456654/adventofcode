#!/usr/bin/python

import sys

sys.setrecursionlimit(10000)


def read_input(filename):
    with open(filename, 'r') as of:
        numbers = list(map(int, of.readline().strip().split()))
        return numbers


def read_node(numbers, level):
    n_children = numbers[0]
    n_md = numbers[1]
    if n_children == 0:
        s = sum(numbers[2:2+n_md])
        return 2+n_md, s, s
    else:
        offset = 0
        total_md = 0
        node_val = []
        for c in range(n_children):
            p_offset, p_md, p_val = read_node(numbers[2+offset:], level + 1)
            offset += p_offset
            total_md += p_md
            node_val.append(p_val)
        md_refs = numbers[2+offset:2+offset+n_md]
        node_value = sum([node_val[i - 1] for i in md_refs if i <= len(node_val)])
        return 2+offset+n_md, total_md + sum(md_refs), node_value


def calc(filename):
    numbers = read_input(filename)
    return read_node(numbers, 0)[1:]


if __name__ == '__main__':
    assert calc('test') == (138, 66)
    print(calc('input'))
