#!/usr/bin/python

def read_input(filename):
    with open(filename, 'r') as of:
        return sorted([tuple(map(int, l.strip().split(','))) for l in of.readlines()])


def close(s, s2):
    return sum([abs(s[i] - s2[i]) for i in range(len(s))]) <= 3


def calc(filename):
    stars = read_input(filename)
    constellations = [[stars.pop(0)]]
    while len(stars) > 0:
        s = stars.pop(0)
        assigned = None
        for const in constellations[::-1]:
            j = len(const) - 1
            still_there = True
            while j >= 0 and still_there:
                s2 = const[j]
                if close(s, s2):
                    if assigned is None:
                        const.append(s)
                        assigned = const
                    elif assigned != const:
                        assigned.extend(constellations.pop(constellations.index(const)))
                        still_there = False
                j -= 1
        if assigned is None:
            constellations.append([s])
    return len(constellations)

if __name__ == '__main__':
    expected_results = [2, 4, 3, 8]
    for t in range(len(expected_results)):
        assert calc(f'test{t}') == expected_results[t]
    print(calc('input'))
