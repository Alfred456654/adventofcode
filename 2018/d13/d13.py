#!/usr/bin/python

import numpy as np
from copy import deepcopy
from collections import Counter

cart_symbols = ['<', 'v', '^', '>']
hallway = {'>': '-', '<': '-', '^': '|', 'v': '|'}
heading = {'>': (0, 1), '^': (-1, 0), 'v': (1, 0), '<': (0, -1)}
angles = {'\\': {'<': '^', '^': '<', '>': 'v', 'v': '>'},
        '/': {'<': 'v', '>': '^', 'v': '<', '^': '>'}}
turns = {'<': {'l': 'v', 's': '<', 'r': '^'},
        '>': {'l': '^', 's': '>', 'r': 'v'},
        'v': {'l': '>', 's': 'v', 'r': '<'},
        '^': {'l': '<', 's': '^', 'r': '>'}}
next_move = ['l', 's', 'r']


def print_maze(maze, carts):
    m = deepcopy(maze)
    n_lines = len(m)
    print(' ' * 103 + '1' * 50)
    print(' ' * 13 + ''.join([10 * str(i) for i in range(1, 10)]) + ''.join([10 * str(i) for i in range(5)]))
    print('   ' + ''.join([str(i) for i in range(10)] * 15))
    for c in carts:
        m[c[0][0]][c[0][1]] = c[1]
    for i in range(n_lines):
        print('{:3d}{}'.format(i, ''.join(m[i])))
    print()


def check_collisions(carts):
    coords = [c[0] for c in carts]
    if len(set(coords)) == len(coords):
        return [], carts
    c = Counter(coords)
    collisions = [k for k, v in c.items() if v > 1]
    new_carts = []
    for cart in carts:
        if cart[0] not in collisions:
            new_carts.append(cart)
    return collisions, new_carts


def move(maze, carts):
    colls = []
    carts = sorted(carts, key=lambda c: (c[0][0], c[0][1]))
    for c in carts:
        c[0] = tuple(sum(x) for x in zip(*[c[0], heading[c[1]]]))
        new_pos = maze[c[0][0]][c[0][1]]
        if new_pos in ['/', '\\']:
            c[1] = angles[new_pos][c[1]]
        elif new_pos == '+':
            c[1] = turns[c[1]][c[2]]
            c[2] = next_move[(next_move.index(c[2]) + 1) % 3]
        new_colls, carts = check_collisions(carts)
        colls.extend(new_colls)
    return maze, carts, list(set(colls))


def read_input(filename):
    with open(filename, 'r') as of:
        lines = list(map(lambda s: [t for t in s.replace('\n', '')], of.readlines()))
        carts = []
        for n in range(len(lines)):
            l = lines[n]
            for k in [t for t in range(len(l)) if l[t] in cart_symbols]:
                carts.append([(n, k), l[k], 'l'])
                l[k] = hallway[l[k]]
        return lines, carts


def calc(filename):
    maze, carts = read_input(filename)
    colls = []
    while len(colls) == 0:
        maze, carts, colls = move(maze, carts)
    assert len(colls) == 1
    coll = colls[0]
    return coll[1], coll[0]


def calc2(filename):
    maze, carts = read_input(filename)
    while len(carts) > 1:
        maze, carts, colls = move(maze, carts)
    return carts[0][0][1], carts[0][0][0]


if __name__ == '__main__':
    assert calc('test') == (7, 3)
    print(calc('input'))
    assert calc2('test2') == (6, 4)
    print(calc2('input'))

