| Whom | Where | What |
|:------------:|:-----------------------------------------------------------------:|:-------:|
| Alfred456654 | [GitLab/Alfred456654](https://www.gitlab.com/Alfred456654/aoc2018) | Python 3 |
| Izosel | [GitLab/Izosel](https://gitlab.com/Izosel/advent2018) | Groovy |
| Charles | [GitHub/karu9](https://github.com/karu9/adventcalendar4) | Python 3 |
| Pierre | [GitLab/pelyot](https://gitlab.com/pelyot/adventofcode) | Rust |
| Kantium | [GitHub/kantium](https://github.com/kantium) | ??? |
| SebDotV | [GitLab/sebdotv](https://gitlab.com/sebdotv/aoc18) | Scala |
| Ben | [Repl.it/Gryzorz](https://repl.it/@Gryzorz) | Lua |
| RawBean | [GitLab/rawbean007](https://gitlab.com/rawbean007/aoc-2018/tree/master) | Awk |
| Françoise | [GitLab/FranJdV](https://gitlab.com/FranJdV/aoc2018) | Python 3 |
| Sylvain | [Repl.it/syllabus](https://repl.it/@syllabus/AoC2018) | Python 3 |